@extends('admin/main')
@section('content')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Tour</h1>
                     </div><!-- /.col -->

          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Tour</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->
 <!-- Main content -->
   <section class="content">
      <div class="container-fluid">
        <!-- Info boxes -->
            <div class="card">

              <!-- /.card-header -->
              <div class="card-body p-0">
                <div class="table-responsive">
                    <table class="table m-0" id="example1">
                    <thead>
                    <tr>
                      <th>ID</th>
                      <th>Cat ID</th>
                      <th>Name</th>
                      <th>Duration </th>
                      <th>Pricechild </th>
                      <th>Priceadult </th>
                      <th>Pricechildren </th>
                      <th>Title </th>
                      {{-- <th >Content </th> --}}
                      <th>Status </th>
                       <th>Piority </th>
                       <th>img </th>

                        <th>TagID </th>
                      <th>
                        <a href="{{ route('admin.tour.create') }}" class="btn btn-xs btn-primary">Thêm</a>
                      </th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($cats as $model)
                      <tr>
                        <td>{{ $loop->index+1 }}</td>
                        <td>{{ $model->category->CatName }}</td>
                          <td>{{ $model->Name }}</td>
                          <td>{{ $model->Duration  }}</td>
                          <th>{{ $model->Pricechild  }}</th>
                          <th>{{ $model->Priceadult  }}</th>
                          <th>{{ $model->Pricechildren  }}</th>
                          <th>{{ $model->Title  }}</th>
                          {{-- <th >{!! $model->Content  !!}</th> --}}
                          <td>{{ ($model->Status ==1)?"Hiện":"Ẩn"}}</td>
                          <td>{{ $model->Piority }}</td>
                          <td><img id="blah" src="{{ url('upload')}}/{{ $model->Image }}" alt="" style="width:100px " /></td>
                              <td>{{ $model->tag->Name }}</td>
                            <td>
                                <a href="{{ route('admin.tour.edit',[$model->id]) }}" class="btn btn-xs btn-primary ">Sửa</a>
                              <form action="{{ route('admin.tour.destroy',$model->id) }}" method="POST" accept-charset="utf-8">

                              </form>

                            </td>
                      </tr>
                      @endforeach
                    </tbody>
                  </table>
                </div>
                <!-- /.table-responsive -->
              </div>
              <!-- /.card-body -->
             <div class="card-footer clearfix">
                <div class="btn btn-sm  float-right">
                        {{ $cats->links() }}
                    </div>
              </div>
              <!-- /.card-footer -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
@endsection
@section('data_table')
<script>
  $(function () {
    $("#example1").DataTable();
  });
</script>
@endsection
