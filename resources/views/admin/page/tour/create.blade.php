@extends('admin/main')
@section('content')
 <!-- Main content -->
 <div class="content-wrapper">
  <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">list</h1>
                     </div><!-- /.col -->

          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Tour</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
  <section>
        <div class="col-md-12">
            <!-- general form elements -->
            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Thêm mới</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form role="form" method="POST" action="{{ route('admin.tour.store') }}"  enctype="multipart/form-data">
                @csrf

                <div class="card-body">
                      <!-- select -->
                  <div class="form-group">
                    <label>Tên danh mục </label>
                    <select class="form-control  @error('CatID')border border-danger @enderror" name="CatID">
                      <option value="" >chọn tên danh mục</option>
                        @foreach($category as $val)
                        <option value="{{ $val->id }}">{{ $val->CatName }}</option>
                        @endforeach
                    </select>
                    @if($errors->has('CatID'))
                    <div class="text-danger">
                        <span >{{$errors->first('CatID')}}</span>
                    </div>
                    @endif
                  </div>

                   <div class="form-group">
                    <label for="exampleInputEmail1">Tên Tour </label>
                    <input type="text" class="form-control @error('Name')border border-danger @enderror" id="Name" name="Name" placeholder="Nhập tên Tour">
                    @if($errors->has('Name'))
                    <div class="text-danger">
                        <span >{{$errors->first('Name')}}</span>
                    </div>
                    @endif
                  </div>
                   <div class="form-group">
                    <label for="exampleInputEmail1">Giá Tour dành cho người lớn  </label>
                    <input type="text" class="form-control @error('Priceadult')border border-danger @enderror" id="Priceadult " name="Priceadult" placeholder="Nhập giá ">
                    @if($errors->has('Priceadult'))
                    <div class="text-danger">
                        <span >{{$errors->first('Priceadult')}}</span>
                    </div>
                    @endif
                  </div>
                  <div class="form-group">
                    <label for="exampleInputEmail1">Giá Tour dành cho trẻ e, từ 1-5 tuổi  </label>
                    <input type="text" class="form-control @error('Pricechild')border border-danger @enderror" id="Pricechild " name="Pricechild" placeholder="Nhập giá ">
                    @if($errors->has('Pricechild'))
                    <div class="text-danger">
                        <span >{{$errors->first('Pricechild')}}</span>
                    </div>
                    @endif
                  </div>
                  <div class="form-group">
                    <label for="exampleInputEmail1">Giá Tour dành cho trẻ em từ 6-10 tuổi </label>
                    <input type="text" class="form-control @error('Pricechildren')border border-danger @enderror" id="Pricechildren " name="Pricechildren" placeholder="Nhập giá ">
                    @if($errors->has('Pricechildren'))
                    <div class="text-danger">
                        <span >{{$errors->first('Pricechildren')}}</span>
                    </div>
                    @endif
                  </div>
                  <div class="form-group">
                    <label for="exampleInputEmail1"> Lựa chọn cấp độ  </label>
                    <input type="text" class="form-control @error('Piority')border border-danger @enderror" id="Piority " name="Piority" placeholder="nhập cấp độ ">
                    @if($errors->has('Piority'))
                    <div class="text-danger">
                        <span >{{$errors->first('Piority')}}</span>
                    </div>
                    @endif
                  </div>
                  <div class="form-group">
                    <label for="exampleInputEmail1">Nhập số ngày Tour </label>
                    <input type="text" class="form-control @error('Duration')border border-danger @enderror" id="CatName" name="Duration" placeholder="Nhập số ngày">
                    @if($errors->has('Duration'))
                    <div class="text-danger">
                        <span >{{$errors->first('Duration')}}</span>
                    </div>
                    @endif
                  </div>
                  <div class="form-group">
                    <label for="exampleInputEmail1">Số người tối đa </label>
                    <input type="text" class="form-control @error('MaxGuest')border border-danger @enderror" id="MaxGuest" name="MaxGuest" placeholder="Nhập số người tối đa">
                    @if($errors->has('MaxGuest'))
                    <div class="text-danger">
                        <span >{{$errors->first('MaxGuest')}}</span>
                    </div>
                    @endif
                  </div>
                  <div class="form-group">
                    <label for="exampleInputEmail1">Tiêu đề </label>
                    <input type="text" class="form-control @error('Title')border border-danger @enderror" id="Title" name="Title" placeholder="Nhập tieu đề">
                    @if($errors->has('Title'))
                    <div class="text-danger">
                        <span >{{$errors->first('Title')}}</span>
                    </div>
                    @endif
                  </div>
                  <div class="card-body pad p-0 ">
                      <label for="exampleInputEmail1"> Giới thiệu </label>
                      <div class=" @error('Content')border border-danger  @enderror">
                        <textarea id="desc"  placeholder="Place some text here"
                                 name="Content"></textarea>
                      </div>
                      @if($errors->has('Content'))
                    <div class="text-danger">
                        <span >{{$errors->first('Content')}}</span>
                    </div>
                    @endif
                    </div>
                  <div class="form-group">
                    <label for="exampleInputPassword1">Từ khóa Tìm kiếm</label>
                       <select class="form-control @error('TagID')border border-danger @enderror" name="TagID">
                      <option value="">chọn tên danh mục</option>
                        @foreach($tag as $val)
                        <option value="{{ $val->id }}">{{ $val->Name }}</option>
                        @endforeach
                    </select>
                    @if($errors->has('TagID'))
                    <div class="text-danger">
                        <span >{{$errors->first('TagID')}}</span>
                    </div>
                    @endif
                  </div>
                  <label for="exampleInputFile"> Ảnh tiêu đề </label>
                    <div class="form-group">
                        <div class="input-group">
                            {{-- <input type="text" class="form-group @error('Image')border border-danger @enderror" id="Image" name="Image" placeholder="link ảnh"> --}}
                            <span class="input-group-btn">
                                <a  data-toggle="modal" href="#model-image" class="btn btn-danger" >Chọn ảnh</a>
                            </span>
                            <input type="text" name="Image" id="Image" class="d-none" value="" >
                            @if($errors->has('Image'))
                            <div class="text-danger">
                                <span >{{$errors->first('Image')}}</span>
                            </div>
                            @endif
                        </div>
                        <img src="{{ url('filemanager') }}" id="img"  class="p-2 w-25" alt="">
                    </div>
                    <div id="img"></div>
                    <label for="exampleInputFile"> Trạng thái </label>
                    <div class="form-group">
                        <div class="custom-control custom-radio">
                          <input class="custom-control-input" type="radio" id="customRadio1" name="Status" value="1"checked>
                          <label for="customRadio1" class="custom-control-label">Hiện</label>
                        </div>
                        <div class="custom-control custom-radio">
                          <input class="custom-control-input" type="radio" id="customRadio2" name="Status"  value="0">
                          <label for="customRadio2" class="custom-control-label">Ẩn</label>
                        </div>
                    </div>
                  <div class="col-sm-6">
                      <!-- radio -->
                      <div class="form-group">

                    <label for="exampleInputFile">Những bức ảnh khác</label>
                    <div class="form-group">
                      <div class="input-group">
                            {{-- <input type="text" class="form-group @error('Image')border border-danger @enderror" id="Image" name="Image" placeholder="link ảnh"> --}}
                            <span class="input-group-btn">
                                <a  data-toggle="modal" href="#model-image2" class="btn btn-danger" >Chọn ảnh</a>
                            </span>
                            <input type="text" name="Images" id="Images" class="" value="" >
                            @if($errors->has('Images'))
                            <div class="text-danger">
                                <span >{{$errors->first('Images')}}</span>
                            </div>
                            @endif
                        <img src="{{ url('filemanager') }}" id="ImagesSHOW"  class="p-2 w-25" alt="">
                        <div id="show"> </div>
                        </div>
                      </div>
                        <label for="exampleInputFile">Anh Banner</label>
                        <div class="form-group">
                             <div class="input-group">
                            {{-- <input type="text" class="form-group @error('Image')border border-danger @enderror" id="Image" name="Image" placeholder="link ảnh"> --}}
                                <span class="input-group-btn">
                                    <a  data-toggle="modal" href="#model-baner" class="btn btn-danger" >Chọn ảnh</a>
                                </span>
                                <input type="text" name="baner" id="baner" class="d-none" value="" >
                            </div>
                             <img src="{{ url('filemanager') }}" id="imagebaner"  class="p-2 w-25" alt="">

                        </div>
                        <div class="form-group">
                             <label>Dịch vụ</label>
                            @foreach($service as $val)
                               <input type="checkbox" name="role[]" value="{{$val->id}}">
                               {{$val->Name}}

                            @endforeach
                        </div>

              <!-- /.card-body -->

                <div class="card-footer">
                  <button type="submit" class="btn btn-primary">Submit</button>
                </div>
              </form>
            </div>
            <!-- /.card -->

              </form>
            </div>
            <!-- /.card -->

          </div>
    </section>
</div>

<!-- Modal 1 -->
<div class="modal fade" id="model-image" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true" >
  <div class="modal-dialog" style=" width: 1000px;max-width: 1000px;">
    <div class="modal-content"style="width: 1000px;height : 800px;">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Modal title</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body ">
       <iframe src="{{ url('filemanager') }}/dialog.php?field_id=Image" class="w-100 h-100" ></iframe>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary|secondary|success|danger|warning|info|light|dark" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary|secondary|success|danger|warning|info|light|dark">Save changes</button>
      </div>
    </div>
  </div>
</div>
  <!-- Modal 3 -->
<div class="modal fade" id="model-baner" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitless" aria-hidden="true" >
  <div class="modal-dialog" style=" width: 1000px;max-width: 1000px;">
    <div class="modal-content"style="width: 1000px;height : 800px;">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitless">Modal title</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body ">
       <iframe src="{{ url('filemanager') }}/dialog.php?field_id=baner" class="w-100 h-100" ></iframe>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary|secondary|success|danger|warning|info|light|dark" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary|secondary|success|danger|warning|info|light|dark">Save changes</button>
      </div>
    </div>
  </div>
</div>
<!-- Modal 2 -->
<div class="modal fade" id="model-image2" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitles" aria-hidden="true" >
  <div class="modal-dialog" style=" width: 1000px;max-width: 1000px;">
    <div class="modal-content"style="width: 1000px;height : 800px;">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitles">Modal title</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body ">
       <iframe src="{{ url('filemanager') }}/dialog.php?field_id=Images" class="w-100 h-100" ></iframe>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary|secondary|success|danger|warning|info|light|dark" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary|secondary|success|danger|warning|info|light|dark">Save changes</button>
      </div>
    </div>
  </div>

<script src="../../plugins/summernote/summernote-bs4.min.js"></script>

@section('script')
  <script >
  $('.date').datepicker({
  multidate: true,
  format: 'dd-mm-yyyy'
});

  $('#model-image').on('hide.bs.modal',function(){
    // lấy value của input có id=images
    var image = $('#Image').val();
    $('#img').attr('src',image);
     });

  $('#model-baner').on('hide.bs.modal',function(){
    // lấy value của input có id=images
    var baner = $('#baner').val();
    $('#imagebaner').attr('src',baner);
     });

$('#model-image2').on('hide.bs.modal',function(){
    // lấy value của input có id=images
    var Images = $('input#Images').val();
    // $('img#ImagesSHOW').attr('src',Images);
    var imgList = $.parseJSON(Images);
    var _html = '';
    for(var i=0;i<imgList.length;i++){
      _html +='<div class="col-md-3 thumbneil">';
      _html +='<img src="'+imgList[i]+'"class="img-responsive" style="max-height: 100px">';
      _html +='</div>';
    }
    $('#show').html(_html);
  });
</script>
<script>
  $(function () {
    // Summernote
    $('.textarea').summernote()
  })
</script>

<script>
   function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#blah')
                        .attr('src', e.target.result);
                };

                reader.readAsDataURL(input.files[0]);
            }
        }
</script>
@endsection
@endsection
