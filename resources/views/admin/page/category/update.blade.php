@extends('admin/main')
@section('content')
 <!-- Main content -->
 <div class="content-wrapper">
  <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">list</h1>
                     </div><!-- /.col -->

          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">list</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <section>
        <div class="col-md-12">
            <!-- general form elements -->
            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">cập nhật</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form role="form" method="POST" action="{{ route('admin.category.update',['category'=>$category->id]) }}"  enctype="multipart/form-data">
                @csrf  @method('PUT')
                <div class="card-body">
                  <div class="form-group">
                    <label for="exampleInputEmail1">Tên danh mục </label>
                    <input type="text" class="form-control" id="CatName" name="CatName" placeholder="Enter name" value="{{ $category->CatName}}">
                  </div>

                  <div class="form-group">
                    <label for="exampleInputPassword1">Keyword Sell</label>
                    <input type="text" class="form-control" id="Keyword_Sell"  name="Keyword_Sell" placeholder="Password" value="{{ $category->Keyword_Sell}}">
                  </div>
                <div class="form-group">
                  <label for="">Trạng thái</label>
                    <div class="radio">
                      <label>
                        <input type="radio" name="Status" id="" value="0" {{($category->Status ==0)?'checked':""}}>
                        Ẩn
                      </label>
                      <label>
                        <input type="radio" name="Status" id="" value="1" {{($category->Status ==1)?'checked':""}}>
                        Hiện
                      </label>
                    </div>
                </div>

                </div>
                                  <!-- /.card-body -->

                <div class="card-footer">
                  <button type="submit" class="btn btn-primary">Submit</button>
                </div>
              </form>
            </div>
            <!-- /.card -->

              </form>
            </div>
            <!-- /.card -->

          </div>
    </section>
</div>

@endsection
