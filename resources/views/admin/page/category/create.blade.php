@extends('admin/main')
@section('content')
 <!-- Main content -->
 <div class="content-wrapper">
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">list</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">list</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <section>
        <div class="col-md-12">
            <!-- general form elements -->
            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Thêm mới</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form role="form" method="POST" action="{{ Route('admin.category.store') }}"  enctype="multipart/form-data">
                @csrf
                <div class="card-body">
                  <div class="form-group">
                    <label for="exampleInputEmail1">Tên danh mục </label>
                    <input type="text" class="form-control @error('CatName')border border-danger @enderror" id="CatName" name="CatName" placeholder="Enter name">
                    @if($errors->has('CatName'))
                    <div class="text-danger">
                        <span >{{$errors->first('CatName')}}</span>
                    </div>
                    @endif
                  </div>

                  <div class="form-group">
                    <label for="exampleInputPassword1">Keyword Sell</label>
                    <input type="text" class="form-control @error('Keyword_Sell')border border-danger @enderror" id="Keyword_Sell"  name="Keyword_Sell" placeholder="Password">
                     @if($errors->has('CatName'))
                    <div class="text-danger">
                        <span >{{$errors->first('Keyword_Sell')}}</span>
                    </div>
                    @endif
                  </div>
                  <div class="form-group">
                      <label for="exampleInputFile"> Trạng thái </label>
                    <div class="form-group">
                        <div class="custom-control custom-radio">
                          <input class="custom-control-input" type="radio" id="customRadio1" name="Status" value="1"checked>
                          <label for="customRadio1" class="custom-control-label">Hiện</label>
                        </div>
                        <div class="custom-control custom-radio">
                          <input class="custom-control-input" type="radio" id="customRadio2" name="Status"  value="0">
                          <label for="customRadio2" class="custom-control-label">Ẩn</label>
                        </div>
                    </div>
                  </div>

                </div>
                <!-- /.card-body -->

                <div class="card-footer">
                  <button type="submit" class="btn btn-primary">Submit</button>
                </div>
              </form>
            </div>
            <!-- /.card -->

              </form>
            </div>
            <!-- /.card -->

          </div>
    </section>
</div>


@endsection
