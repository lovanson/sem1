@extends('admin.main')
@section('content')
<div class="content-wrapper" style="padding:20px">
    <div class="content-header">
      <div class="container">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Add Gallery</h1>
        </div><!-- /.col -->
        <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Add Gallery</li>
          </ol>
      </div><!-- /.col -->
  </div><!-- /.row -->
</div><!-- /.container-fluid -->
</div>
<section>
    <div class="container">
        <div class="row">
            <div class="col-md-10">
                <!-- general form elements -->
                <div class="card card-primary">
                    <div class="card-header">
                        <h3 class="card-title">Thêm mới</h3>
                    </div>
                </div>
                <form role="form" method="POST" action="{{route('admin.gallery.store')}}" enctype="multipart/form-data">
                    @csrf
                    <div class="form-group">
                        <label for="exampleInputPassword1">Title</label>
                        <input type="text" class="form-control"  name="title" placeholder="Title">
                    </div>
                    <div class="form-group">
                      <div class="input-group">
                        <span class="input-group-btn">
                            <a  data-toggle="modal" href="#model-image" class="btn btn-danger" >Chọn ảnh</a>
                        </span>
                        <input type="text" name="img" id="Image" class="" value="" >
                        @if($errors->has('Image'))
                        <div class="text-danger">
                            <span >{{$errors->first('Image')}}</span>
                        </div>
                        @endif
                    </div>
                    <div class="form-group">
                        <span>ảnh khác</span>
                        <div class="input-group">
                            <span class="input-group-btn">
                                <a  data-toggle="modal" href="#model-baner-top" class="btn btn-danger" >Ảnh khác</a>
                            </span>
                            <input type="text" name="images" id="baner_top" class="" value="" >
                            @if($errors->has('images'))
                            <div class="text-danger">
                                <span >{{$errors->first('images')}}</span>
                            </div>
                            @endif
                        </div>
                        <img src="{{ url('filemanager') }}" id="img_baner"  class="p-2 w-25" alt="">
                    </div>
                </div>
                <button type="submit" class="btn btn-danger">Add</button>
            </form>
        </div>
    </div>
</div>
</section>
</div>
<!-- Modal 1 -->
<div class="modal fade" id="model-image" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true" >
    <div class="modal-dialog" style=" width: 1000px;max-width: 1000px;">
        <div class="modal-content"style="width: 1000px;height : 800px;">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLongTitle">Modal title</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
          </button>
      </div>
      <div class="modal-body ">
         <iframe src="{{ url('filemanager') }}/dialog.php?field_id=Image" class="w-100 h-100" ></iframe>
     </div>
     <div class="modal-footer">
        <button type="button" class="btn btn-primary|secondary|success|danger|warning|info|light|dark" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary|secondary|success|danger|warning|info|light|dark">Save changes</button>
    </div>
</div>
</div>
</div>
<!-- Modal 3 -->
<div class="modal fade" id="model-baner-top" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitless" aria-hidden="true" >
  <div class="modal-dialog" style=" width: 1000px;max-width: 1000px;">
    <div class="modal-content"style="width: 1000px;height : 800px;">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitless">Modal title</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
      </button>
  </div>
  <div class="modal-body ">
   <iframe src="{{ url('filemanager') }}/dialog.php?field_id=baner_top" class="w-100 h-100" ></iframe>
</div>
<div class="modal-footer">
    <button type="button" class="btn btn-primary|secondary|success|danger|warning|info|light|dark" data-dismiss="modal">Close</button>
    <button type="button" class="btn btn-primary|secondary|success|danger|warning|info|light|dark">Save changes</button>
</div>
</div>
</div>
</div>
@endsection
@section('script')
<script>
   $('#model-image').on('hide.bs.modal',function(){
    // lấy value của input có id=images
    var image = $('#Image').val();
    $('#img').attr('src',image);
});$('#model-image').on('hide.bs.modal',function(){
    // lấy value của input có id=images
    var baner_top = $('#baner_top').val();
    $('#img_baner').attr('src',baner_top);
});
// $('#model-image2').on('hide.bs.modal',function(){
//     // lấy value của input có id=images
//     var img = $('input#img').val();
//     // $('img#ImagesSHOW').attr('src',Images);
//     var imgList = $.parseJSON(img);
//     var _html = '';
//     for(var i=0;i<imgList.length;i++){
//       _html +='<div class="col-md-3 thumbneil">';
//       _html +='<img src="'+imgList[i]+'"class="img-responsive" style="max-height: 100px">';
//       _html +='</div>';
//     }
//     $('#show').html(_html);
//   });
</script>
@endsection
