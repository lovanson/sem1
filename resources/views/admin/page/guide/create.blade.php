@extends('admin/main')
@section('content')
<!-- Main content -->
<div class="content-wrapper">
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">list</h1>
        </div><!-- /.col -->
        <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">list</li>
          </ol>
      </div><!-- /.col -->
  </div><!-- /.row -->
</div><!-- /.container-fluid -->
</div>
<section>
    <div class="col-md-12">
        <!-- general form elements -->
        <div class="card card-primary">
          <div class="card-header">
            <h3 class="card-title">Thêm mới</h3>
        </div>
        <!-- /.card-header -->
        <!-- form start -->
        <form role="form" method="POST" action="{{ Route('admin.guide.store') }}"  enctype="multipart/form-data">
            @csrf
            <div class="card-body">
              <div class="form-group">
                <label for="exampleInputEmail1">Tên danh mục </label>
                <input type="text" class="form-control @error('Name')border border-danger @enderror " id="Name" name="Name" placeholder="Enter name">
                @if($errors->has('Name'))
                <div class="text-danger">
                    <span >{{$errors->first('Name')}}</span>
                </div>
                @endif
            </div>
            <div class="form-group">
                <label for="exampleInputEmail1">DoB </label>
                <input type="date" class="form-control @error('DoB')border border-danger @enderror " id="DoB" name="DoB" placeholder="Enter name">
                @if($errors->has('DoB'))
                <div class="text-danger">
                    <span >{{$errors->first('DoB')}}</span>
                </div>
                @endif
            </div>
            <div class="form-group">
                        <div class="input-group">
                            {{-- <input type="text" class="form-group @error('Image')border border-danger @enderror" id="Image" name="Image" placeholder="link ảnh"> --}}
                            <span class="input-group-btn">
                                <a  data-toggle="modal" href="#model-image" class="btn btn-danger" >Chọn ảnh</a>
                            </span>
                            <input type="text" name="avata" id="avata" class="" value="" >
                            @if($errors->has('Image'))
                            <div class="text-danger">
                                <span >{{$errors->first('avata')}}</span>
                            </div>
                            @endif
                        </div>
                        <img src="{{ url('filemanager') }}" id="img"  class="p-2 w-25" alt="">
                    </div>
            <div class="form-group">
                <label for="exampleInputEmail1">Email </label>
                <input type="text" class="form-control @error('Email')border border-danger @enderror " id="Email" name="Email" placeholder="Enter name">
                @if($errors->has('Email'))
                <div class="text-danger">
                    <span >{{$errors->first('Email')}}</span>
                </div>
                @endif
            </div>
            <div class="form-group">
                <label for="exampleInputEmail1">Phone </label>
                <input type="text" class="form-control @error('Phone')border border-danger @enderror " id="Phone" name="Phone" placeholder="Enter name">
                @if($errors->has('Phone'))
                <div class="text-danger">
                    <span >{{$errors->first('Phone')}}</span>
                </div>
                @endif
            </div>
            <div class="card-body pad p-0 ">
              <label for="exampleInputEmail1"> Contact </label>
              <div class=" @error('Contactmethod')border border-danger @enderror ">
                 <textarea id="desc"  placeholder="Place some text here"
                                 name="Contactmethod"></textarea>
            </div>
             @if($errors->has('Contactmethod'))
                <div class="text-danger">
                    <span >{{$errors->first('Contactmethod')}}</span>
                </div>
                @endif

        </div>

        <div class="form-group">
            <div class="custom-control custom-radio">
              <input class="custom-control-input" type="radio" id="customRadio1" name="Status" value="1"checked>
              <label for="customRadio1" class="custom-control-label">Hiện</label>
          </div>
          <div class="custom-control custom-radio">
              <input class="custom-control-input" type="radio" id="customRadio2" name="Status"  value="0">
              <label for="customRadio2" class="custom-control-label">Ẩn</label>
          </div>
      </div>

  </div>
  <!-- /.card-body -->

  <div class="card-footer">
      <button type="submit" class="btn btn-primary">Submit</button>
  </div>
</form>
</div>
<!-- /.card -->

</form>
</div>
<!-- /.card -->

</div>
</section>

</div>
<!-- Modal 1 -->
<div class="modal fade" id="model-image" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true" >
  <div class="modal-dialog " style=" width: 1000px;max-width: 1000px;">
    <div class="modal-content"style="width: 1000px;height : 800px;">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Modal title</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body ">
       <iframe src="{{ url('filemanager') }}/dialog.php?field_id=avata" class="w-100 h-100" ></iframe>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary|secondary|success|danger|warning|info|light|dark" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary|secondary|success|danger|warning|info|light|dark">Save changes</button>
      </div>
    </div>
  </div>
</div>
<script src="../../plugins/summernote/summernote-bs4.min.js"></script>
@section('script')
<script>
   tinymce.init({
    selector: '#desc';
   }) ;
</script>
<script type="text/javascript">
    $(document).ready(function () {
      bsCustomFileInput.init();
  });
     $('#model-image').on('hide.bs.modal',function(){
    // lấy value của input có id=images
    var avata = $('#avata').val();
    $('#img').attr('src',avata);
     });
      function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#blah')
                        .attr('src', e.target.result);
                };

                reader.readAsDataURL(input.files[0]);
            }
        }
 </script>
@endsection



    @endsection
