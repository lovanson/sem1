@extends('admin/main')
@section('content')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">list</h1>
                     </div><!-- /.col -->

          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">list</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->
 <!-- Main content -->
   <section class="content">
      <div class="container-fluid">
        <!-- Info boxes -->
            <div class="card">

              <!-- /.card-header -->
              <div class="card-body">
                <div class="table-responsive">
                  <table class="table m-0" id="example1">
                    <thead>
                    <tr>
                      <th>ID</th>
                      <th>Name</th>
                      <th>DoB</th>
                      <th>Email</th>
                      <th>Phone</th>
                      <th>Contactmethod</th>
                      <th>Status</th>
                      <th>Avata</th>

                      <th>
                        <a href="{{ route('admin.guide.create') }}" class="btn btn-xs btn-primary">Thêm</a>
                      </th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($cats as $model)
                      <tr>
                        <td>{{ $model->id }}</td>
                          <td>{{ $model->Name }}</td>
                          <td>{{ $model->DoB }}</td>
                          <td>{{ $model->Email }}</td>
                          <td>{{ $model->Phone }}</td>
                          <td>{!! $model->Contactmethod !!}</td>
                           <td>{{ ($model->Status ==1)?"Hiện":"Ẩn"}}</td>
                           <td><img src="{{ url('upload')}}/{{($model->avata)}}" alt="" class="w-50 "></td>
                            <td>
                                <a href="{{ route('admin.guide.edit',$model->id) }}" class="btn btn-xs btn-primary ">Sửa</a>
                              <form action="{{ route('admin.guide.destroy',$model->id) }}" method="POST" accept-charset="utf-8">
                                @csrf @method('DELETE')
                                <button type="submit" class="btn btn-xs btn-danger  " onclick="return confirm('Bạn có chắc muốn xóa không') " >Xóa</button>
                              </form>

                            </td>
                      </tr>
                      @endforeach
                    </tbody>
                  </table>
                </div>
                <!-- /.table-responsive -->
              </div>
              <!-- /.card-body -->
              <div class="card-footer clearfix">
                <div class="btn btn-sm  float-right">
                        {{ $cats->links() }}
                    </div>
              </div>
              <!-- /.card-footer -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
@endsection
@section('data_table')
<script>
  $(function () {
    $("#example1").DataTable();
  });
</script>
@endsection
