@extends('admin/main')
@section('content')
 <!-- Main content -->
 <div class="content-wrapper">
  <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">list</h1>
                     </div><!-- /.col -->

          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Tour</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>

 <section>
        <div class="col-md-12">
            <!-- general form elements -->
            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Thêm mới</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form role="form" method="POST" action="{{ route('admin.payment.update',$paymentMethods->id) }}"  enctype="multipart/form-data">
                @csrf @method('PUT')

                <div class="card-body">
                      <!-- select -->


                   <div class="form-group">
                    <label for="exampleInputEmail1">Phương thức thanh toán </label>
                    <input type="text" class="form-control @error('Payment_type')border border-danger @enderror" id="Payment_type" name="Payment_type" placeholder="Nhập tên Tour" value="{{ $paymentMethods->Payment_type }}">
                        @if($errors->has('Payment_type'))
                        <div class="text-danger">
                            <span >{{$errors->first('Payment_type')}}</span>
                        </div>
                        @endif
                  </div>
                     <div class="form-group">
                  <label for="">Trạng thái</label>
                    <div class="radio">
                      <label>
                        <input type="radio" name="Status" id="" value="0" {{($paymentMethods->Status ==0)?'checked':""}}>
                        Ẩn
                      </label>
                      <label>
                        <input type="radio" name="Status" id="" value="1" {{($paymentMethods->Status ==1)?'checked':""}}>
                        Hiện
                      </label>
                    </div>
                </div>
                               <!-- /.card-body -->

                <div class="card-footer">
                  <button type="submit" class="btn btn-primary">Submit</button>
                </div>
              </form>
            </div>
            <!-- /.card -->

              </form>
            </div>
            <!-- /.card -->

          </div>
    </section>
</div>
<script src="../../plugins/summernote/summernote-bs4.min.js"></script>
<script>
  $(function () {
    // Summernote
    $('.textarea').summernote()
  })
</script>
<script type="text/javascript">
$(document).ready(function () {
  bsCustomFileInput.init();
});
</script>
<script>
   function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#blah')
                        .attr('src', e.target.result);
                };

                reader.readAsDataURL(input.files[0]);
            }
        }
</script>
@endsection
