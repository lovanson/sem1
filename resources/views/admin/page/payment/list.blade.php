@extends('admin/main')
@section('content')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Tour</h1>
        </div><!-- /.col -->

        <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Tour</li>
          </ol>
      </div><!-- /.col -->
  </div><!-- /.row -->
</div><!-- /.container-fluid -->
</div>
<!-- /.content-header -->
<!-- Main content -->
<section class="content">
  <div class="container-fluid">
    <!-- Info boxes -->
    <div class="card">
      <div class="card-header border-transparent">
        <h3 class="card-title">Tour</h3>

    </div>
    <form class="form-inline ml-3"  action="{{ route('admin.search_payment') }}" method="GET">
      <div class="input-group input-group-sm">
        <input class="form-control form-control-navbar" name="search" type="search" placeholder="Search" aria-label="Search">
        <div class="input-group-append">
          <button class="btn btn-primary" type="submit">
            <i class="fas fa-search"></i>
        </button>
    </div>
</div>
</form>

<!-- /.card-header -->
<div class="card-body">
    <div class="table-responsive">
      <table class="table m-0" id="example2">
        <thead>
            <tr>
              <th>ID</th>
              <th>Cat ID</th>
              <th>Payment_type</th>
              <th>Status </th>
              <th>
                <a href="{{ route('admin.payment.create') }}" class="btn btn-xs btn-primary">Thêm</a>
            </th>
        </tr>
    </thead>
    <tbody>
        @foreach($cats as $model)
        <tr>
            <td>{{ $model->id }}</td>
            <td>{{ $model->Payment_type }}</td>
              <td>{{ ($model->Status ==1)?"Hiện":"Ẩn"}}</td>
            <td>
                <a href="{{ route('admin.payment.edit',$model->id) }}" class="btn btn-xs btn-primary ">Sửa</a>
                <form action="{{ route('admin.payment.destroy',$model->id) }}" method="POST" accept-charset="utf-8">
                    @csrf @method('DELETE')
                    <button type="submit" class="btn btn-xs btn-danger  " onclick="return confirm('Bạn có chắc muốn xóa không') " >Xóa</button>
                </form>

            </td>
        </tr>
        @endforeach
    </tbody>
</table>
</div>
<!-- /.table-responsive -->
</div>
<!-- /.card-body -->
<!-- /.card -->
</div>
<!-- /.col -->
@endsection
@section('data_table')
<script>
    $('#example2').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false,
      "responsive": true,
    });
</script>
@endsection
