@extends('admin/main')
@section('content')
 <!-- Main content -->
 <div class="content-wrapper">
  <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Add</h1>
        </div><!-- /.col -->
        <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="#">Home</a></li>
            <li class="breadcrumb-item active">Role Assignment</li>
        </ol>
        </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <section>
        <div class="col-md-12">
            <!-- general form elements -->
            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Thêm mới</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form role="form" method="POST" action="{{ route('admin.customer_service.store') }}"  enctype="multipart/form-data">
                @csrf

                <div class="card-body">
                      <!-- select -->

                  <div class="form-group">
                    <label for="exampleInputEmail1">Email:</label>
                     <input type="text" class="form-control d-none" id="contact_id" name="contact_id"  value="{{$contact->id}}"  >
                    <input type="hidden" class="form-control d-none"  name="email"  value="{{$contact->email}}"  >
                    <h6>{{ $contact->email }}</h6>
                  </div>

                  <div class="form-group">
                    <label for="exampleInputEmail1">Contact </label>
                    <input type="text" class="form-control" id="content" name="content" placeholder="content">
                  </div>
                <!-- /.card-body -->
                <div class="card-footer">
                  <button type="submit" class="btn btn-primary">Add</button>
                </div>
              </form>
            </div>
            <!-- /.card -->
              </form>
            </div>
            <!-- /.card -->

          </div>
    </section>
</div>
@endsection
