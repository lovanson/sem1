@if($order)
        <div class="">
           {{-- <b> Người đặt tour : {{ $order->customer->name }}</b> --}}
        </div>
        <div class="payment d-flex">
            <b>Phương thức thanh toán:</b>
            {{-- <label>{{ $order->payment->Payment_type }}</label> --}}
        </div>
        <div class="w-100 text-center d-flex">
            <h4><label>Tên Tour:</label>
            {{ $order->tours->Name }}</h4>
        </div>
        <div class="w-100 d-flex ">
            <div class="w-50">
                Ngày bắt đầu:
                <label> {{ $order->Start }}</label>
            </div>
            <div class="w-50">
                Ngày kết thúc:
                <label>{{ $order->End }}</label>
            </div>
        </div>
        <div class="d-flex" >
            <ul>
                <li>Số trẻ em từ 1- 5 tuổi:</li>
                <li>Số trẻ em từ 6-10 tuổi: </li>
                <li>Số trẻ vị thành niên và người lớn:</li>
            </ul>
            <ul>
                <ol>{{ $order->guestchild }}</ol>
                <ol>{{ $order->guestchildren }}</ol>
                <ol>{{ $order->guestadult }}</ol>
            </ul>
        </div>
        <div class="d-flex ">
            <p class="m-2">Ngày tạo:</p>
            <label class="m-2">{{ $order->created_at }}</label>
        </div>
@endif
