@if($order)

         <div class="card-body p-0">
                <div class="table-responsive">
                  <table class="table m-0">
                    <thead>
                    <tr>
                      <th>ID</th>
                      <th>UserID</th>
                      <th>PaymentID</th>
                      <th>Status</th>
                      <th>Total amount</th>

                    </tr>
                    </thead>
                    <tbody>
                    @foreach($order as $model)
                      <tr>
                        <td>{{ $model->id }}</td>
                          <td>{{ $model->UserID }}</td>
                          <td>{{ $model->PaymentID }}</td>
                            <td>{{ ($model->Status ==1)?"Hiện":"Ẩn"}}</td>
                            <td>{{ $model->Totalamount}}</td>
                            <td>

                              <form action="{{ route('order.destroy',$model->id) }}" method="POST" accept-charset="utf-8">
                                @csrf @method('DELETE')

                                <button type="submit" class="btn btn-xs btn-danger  " onclick="return confirm('Bạn có chắc muốn xóa không') " >Xóa</button>
                              </form>

                            </td>
                      </tr>
                      @endforeach
                    </tbody>
                  </table>
                </div>
                <!-- /.table-responsive --><div class="card-footer clearfix">
                <div class="btn btn-sm  float-right">
                        {{ $cats->links() }}
                    </div>
              </div>
              </div>
@endif
