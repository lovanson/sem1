@extends('admin/main')
@section('content')
 <!-- Main content -->
 <div class="content-wrapper">
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">list</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">list</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <section>
        <div class="col-md-12">
            <!-- general form elements -->
            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Thêm mới</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form role="form" method="POST" action="{{ route('admin.service.update',['service'=>$service->id]) }}"  enctype="multipart/form-data">
                @csrf  @method('PUT')
                <div class="card-body">
                  <div class="form-group">
                    <label for="exampleInputEmail1">Tên danh mục </label>
                    <input type="text" class="form-control" id="Name" name="Name" placeholder="Enter name " value="{{ $service->Name }}">
                  </div>
                  <div class="form-group">
                    <label for="exampleInputEmail1">Gía</label>
                    <input type="text" class="form-control" id="Price" name="Price" placeholder="Enter name" value="{{ $service->Price }}">
                  </div>
                 <div class="card-body pad p-0 ">
                      <label for="exampleInputEmail1"> Giới thiệu </label>
                      <div class="">
                        <textarea class="textarea" id="content" placeholder="Place some text here"
                                  style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;" name="Description">{{$service->Description}}</textarea>
                      </div>
                    </div>

                    <div class="form-group">
                  <label for="">Trạng thái</label>
                    <div class="radio">
                      <label>
                        <input type="radio" name="Status" id="" value="0" {{($service->Status ==0)?'checked':""}}>
                        Ẩn
                      </label>
                      <label>
                        <input type="radio" name="Status" id="" value="1" {{($service->Status ==1)?'checked':""}}>
                        Hiện
                      </label>
                    </div>
                </div>
                </div>
                <!-- /.card-body -->

                <div class="card-footer">
                  <button type="submit" class="btn btn-primary">Submit</button>
                </div>
              </form>
            </div>
            <!-- /.card -->

              </form>
            </div>
            <!-- /.card -->

          </div>
    </section>

</div>
<script src="../../plugins/summernote/summernote-bs4.min.js"></script>
<script type="text/javascript">
$(document).ready(function () {
  bsCustomFileInput.init();
});
</script>
@section('script')
<script>
   tinymce.init({
    selector: '#content'
   }) ;
</script>

@endsection
@endsection
