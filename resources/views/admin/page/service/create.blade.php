@extends('admin/main')
@section('content')
 <!-- Main content -->
 <div class="content-wrapper">
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">list</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Dịch vụ</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <section>
        <div class="col-md-12">
            <!-- general form elements -->
            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Thêm mới</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form role="form" method="POST" action="{{ Route('admin.service.store') }}"  enctype="multipart/form-data">
                @csrf
                <div class="card-body">
                  <div class="form-group ">
                    <label for="exampleInputEmail1">Tên dịch vụ </label>
                    <input type="text" class="form-control  @error('Name')border border-danger @enderror "id="Name" name="Name" placeholder="Enter name" >
                    @if($errors->has('Name'))
                    <div class="text-danger">
                        <span >{{$errors->first('Name')}}</span>
                    </div>
                    @endif
                  </div>
                  <div class="form-group">
                    <label for="exampleInputEmail1">Giá dịch vụ </label>
                    <input type="text" class="form-control @error('Name')border border-danger @enderror" id="Price" name="Price" placeholder="Enter name">
                    @if($errors->has('Price'))
                    <div class="text-danger">
                        <span >{{$errors->first('Price')}}</span>
                    </div>
                    @endif
                  </div>
                 <div class="card-body pad p-0 ">
                      <label for="exampleInputEmail1"> Giới thiệu về dịch vụ </label>
                      <div class="text-danger @error('Description')border border-danger  rounded @enderror">
                        <textarea class=" m-0" id="desc" name="Description"  placeholder="Place some text here"
                                  style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;" >

                         </textarea>
                      </div>
                         @if($errors->has('Description'))
                        <div class="text-danger">
                            <span >{{$errors->first('Description')}}</span>
                        </div>
                        @endif
                    </div>

                   <div class="form-group">
                        <div class="custom-control custom-radio">
                          <input class="custom-control-input" type="radio" id="customRadio1" name="Status" value="1"checked>
                          <label for="customRadio1" class="custom-control-label">Hiện</label>
                        </div>
                        <div class="custom-control custom-radio">
                          <input class="custom-control-input" type="radio" id="customRadio2" name="Status"  value="0">
                          <label for="customRadio2" class="custom-control-label">Ẩn</label>
                        </div>
                    </div>

                </div>
                <!-- /.card-body -->

                <div class="card-footer">
                  <button type="submit" class="btn btn-primary">Submit</button>
                </div>
              </form>
            </div>
            <!-- /.card -->

              </form>
            </div>
            <!-- /.card -->

          </div>
    </section>

</div>
<script src="../../plugins/summernote/summernote-bs4.min.js"></script>
<script type="text/javascript">

</script>
@section('script')

<script>
    $(document).ready(function () {
  bsCustomFileInput.init();
  $("#test").click(function(){
    alert("ok0");
    console.log($(".note-editable.card-block p").html());
  });
});
   tinymce.init({
    selector: '#content'
   }) ;
</script>


@endsection
