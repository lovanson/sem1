@extends('admin.main')
@section('content')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">list</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">list User</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->
 <!-- Main content -->
   <section class="content">
      <div class="container-fluid">
        <!-- Info boxes -->
        <div class="card">
          <a href="{{route('admin.customer.create')}}" class="btn btn-primary " style="font-size:25px;"><i class="fa fa-plus" aria-hidden="true" style="float:right"></i></a>
        </div>
        <ul class="nav nav-tabs" id="myTab" role="tablist">
          <li class="nav-item">
            <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">Admin</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">Approved</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" id="profile-tab" data-toggle="tab" href="#user" role="tab" aria-controls="user" aria-selected="false">User</a>
          </li>
        </ul>
        <div class="tab-content" id="myTabContent">
          <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
            <div class="card">
              <!-- /.card-header -->
              <div class="row">
                <div class="col-md-12">
                  <div class="card-body table-responsive" style="height: 300px;">
                    <table class="table table-head-fixed text-nowrap" style="text-align:center" id="example1">
                      <thead>
                        <tr>
                          <th>ID</th>
                          <th>Name</th>
                          <th>Image</th>
                          <th>Email</th>
                          <th>Date Of Birth</th>
                          <th>Address</th>
                          <th>Phone Number</th>
                          <th>Sex</th>
                          <th>Action</th>
                        </tr>
                      </thead>
                      <tbody>
                        @foreach($customer as $model)
                        @if($model->status==1)
                        <tr>
                          <td>{{$model->id}}</td>
                          <td>{{$model->name}}</td>
                          @if($model->image == Null)
                            <td>
                                <div id="imagePreview" >
                                      <img class="w-100 " style="border-radius: 70% ;width:80px" src="https://i1.wp.com/placehold.it/160x160/38729e/ffffff/&text={{ Str::limit(Auth::user()->name, 1, $end='')  }}" alt="">
                                </div>
                            </td>
                            @else
                            <td>
                                <img src="{{ url('public/upload/customer') }}/{{$model->image }}" style="border-radius: 60%; width:80px">
                            </td>
                            @endif
                          <td>{{$model->email}}</td>
                          <td>{{$model->date_of_birth}}</td>
                          <td>{{$model->address}}</td>
                          <td>{{$model->phone_number}}</td>
                          <td>{{($model->sex)==1?'Boy':'Girl'}}</td>
                          <td class="">
                              <a href="{{ route('admin.customer.edit',['customer'=>$model->id]) }}" type="submit" class="btn form-control btn-primary mb-2"><i class="fas fa-edit"></i></a>
                              <form action="{{ route('admin.customer.destroy',['customer'=>$model->id]) }}" method="POST" accept-charset="utf-8">
                              @csrf @method('DELETE')
                                <button type="submit" class="btn form-control btn-danger mb-2" onclick="return confirm('Bạn có chắc muốn xóa không')" ><i class="fas fa-trash" aria-hidden="true"></i></button>
                              </form>
                              @if(Auth::user()->id!=$model->id)
                              <a href="{{ route('admin.changestatus',['id'=>$model->id]) }}" class="btn btn-warning form-control"><i class="fa fa-eye-slash" aria-hidden="true"></i>
                              @endif
                          </td>
                        </tr>
                        @endif
                        @endforeach
                      </tbody>
                    </table>
                  </div>
                  <!-- /.card-body -->
                </div>
                <!-- /.card -->
              </div>
            </div>
          </div>
          <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">
            <div class="card">
            <!-- /.card-header -->
              <div class="row">
                <div class="col-md-12">
                  <div class="card-body table-responsive" style="height: 300px;">
                    <table class="table table-head-fixed text-nowrap" style="text-align:center" id="example3">
                      <thead>
                        <tr>
                          <th>ID</th>
                          <th>Name</th>
                          <th>Image</th>
                          <th>Email</th>
                          <th>Date Of Birth</th>
                          <th>Address</th>
                          <th>Phone Number</th>
                          <th>Sex</th>
                          <th>Level</th>
                          <th>Action</th>
                        </tr>
                      </thead>
                      <tbody>
                        @foreach($customer as $model)
                        @if($model->status==2 && $model->level==1)
                        <tr>
                          <td>{{$model->id}}</td>
                          <td>{{$model->name}}</td>
                         @if($model->image == Null)
                            <td>
                                <div id="imagePreview" >
                                      <img class="w-100 " style="border-radius: 70% ;width:80px" src="https://i1.wp.com/placehold.it/160x160/38729e/ffffff/&text={{ Str::limit(Auth::user()->name, 1, $end='')  }}" alt="">
                                </div>
                            </td>
                            @else
                            <td>
                                <img src="{{ url('public/upload/customer') }}/{{$model->image }}" style="border-radius: 60%; width:80px">
                            </td>
                            @endif
                          <td>{{$model->email}}</td>
                          <td>{{$model->date_of_birth}}</td>
                          <td>{{$model->address}}</td>
                          <td>{{$model->phone_number}}</td>
                          <td>{{($model->sex)==1?'Boy':'Girl'}}</td>
                          <td>{{($model->level)==1?'Admin Boss':(($model->level)==2?'Admin':'User')}}</td>
                          <td class="">

                              <a href="{{ route('admin.customer.edit',['customer'=>$model->id]) }}" type="submit" class="btn form-control btn-primary mb-2"><i class="fas fa-edit"></i></a>
                              <form action="{{ route('admin.customer.destroy',['customer'=>$model->id]) }}" method="POST" accept-charset="utf-8">
                              @csrf @method('DELETE')
                                <button type="submit" class="btn form-control btn-danger mb-2 " onclick="return confirm('Bạn có chắc muốn xóa không')" ><i class="fas fa-trash" aria-hidden="true"></i></button>
                              </form>
                              @if(Auth::user()->id!=$model->id)
                              <a href="{{ route('admin.changestatus',['id'=>$model->id]) }}" class="btn btn-warning form-control"><i class="fa fa-eye" aria-hidden="true"></i></a>
                              @endif
                          </td>
                        </tr>
                        @endif
                        @endforeach
                      </tbody>
                    </table>
                  </div>
                  <!-- /.card-body -->
                </div>
                <!-- /.card -->
              </div>
            </div>
          </div>
          <div class="tab-pane fade" id="user" role="tabpanel" aria-labelledby="profile-tab">
            <div class="card">
              <!-- /.card-header -->
              <div class="row">
                <div class="col-md-12">
                  <div class="card-body table-responsive" >
                    <table class="table table-head-fixed text-nowrap" style="text-align:center" id="example4">
                      <thead>
                        <tr>
                          <th>ID</th>
                          <th>Name</th>
                          <th>Image</th>
                          <th>Email</th>
                          <th>Date Of Birth</th>
                          <th>Address</th>
                          <th>Phone Number</th>
                          <th>Sex</th>
                          <th>Action</th>
                        </tr>
                      </thead>
                      <tbody>
                        @foreach($customer as $model)
                        @if($model->status==2 && $model->level==2)
                        <tr>
                          <td>{{$model->id}}</td>
                          <td>{{$model->name}}</td>
                         @if($model->image == Null)
                            <td>
                                <div id="imagePreview" >
                                      <img class="w-100 " style="border-radius: 70% ;width:80px" src="https://i1.wp.com/placehold.it/160x160/38729e/ffffff/&text={{ Str::limit(Auth::user()->name, 1, $end='')  }}" alt="">
                                </div>
                            </td>
                            @else
                            <td>
                                <img src="{{ url('public/upload/customer') }}/{{$model->image }}" style="border-radius: 60%; width:80px">
                            </td>
                            @endif
                          <td>{{$model->email}}</td>
                          <td>{{$model->date_of_birth}}</td>
                          <td>{{$model->address}}</td>
                          <td>{{$model->phone_number}}</td>
                          <td>{{($model->sex)==1?'Boy':'Girl'}}</td>
                          <td class="">
                              <a href="{{ route('admin.customer.edit',['customer'=>$model->id]) }}" type="submit" class="btn form-control btn-primary mb-2"><i class="fas fa-edit"></i></a>
                              <form action="{{ route('admin.customer.destroy',['customer'=>$model->id]) }}" method="POST" accept-charset="utf-8">
                              @csrf @method('DELETE')
                                <button type="submit" class="btn form-control btn-danger mb-2" onclick="return confirm('Bạn có chắc muốn xóa không')" ><i class="fas fa-trash" aria-hidden="true"></i></button>
                              </form>
                              @if(Auth::user()->id!=$model->id)
                              <a href="{{ route('admin.changestatus',['id'=>$model->id]) }}" class="btn btn-warning form-control"><i class="fa fa-eye" aria-hidden="true"></i></a>
                              @endif
                          </td>
                        </tr>
                        @endif
                        @endforeach
                      </tbody>
                    </table>
                  </div>
                  <!-- /.card-body -->
                </div>
                <!-- /.card -->
              </div>
            </div>
          </div>
        </div>

      </div>
<!-- /.row -->
   </section>
@endsection
@section('data_table')
<script>
  $(function () {
    $("#example3").DataTable();
    $("#example4").DataTable();
    $("#example1").DataTable();
    $('#example2').DataTable();
  });
</script>
@endsection
