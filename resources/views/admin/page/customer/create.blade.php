@extends('admin.main')
@section('content')
<div class="content-wrapper" style="padding:20px">
    <div class="content-header">
      <div class="container">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Add User</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Add User</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <section>
    <div class="container">
        <div class="row">
            <div class="col-md-10">
                <!-- general form elements -->
                <div class="card card-primary">
                    <div class="card-header">
                        <h3 class="card-title">Thêm mới</h3>
                    </div>
                </div>
                <form role="form" method="POST" action="{{route('admin.customer.store')}}" enctype="multipart/form-data">
                @csrf
                    <div class="form-group ">
                        <label for="usr">Name</label><br>
                        @if($errors->has('name'))<label style="color:red" for="usr">{{$errors->first('name')}}</label>@endif
                    <input type="text" value="{{old('name')}}" class="form-control @error('name') is-invalid @enderror" id="usr" name="name" placeholder="Name">
                    </div>
                    <div class="form-group">
                        <label for="exampleInputPassword1">Password</label><br>
                        @if($errors->has('password'))<label style="color:red" for="usr">{{$errors->first('password')}}</label>@endif
                        <input type="password" value="{{old('password')}}" class="form-control @error('password') is-invalid @enderror" id="exampleInputPassword1" name="password" placeholder="Password">
                    </div>
                    <div class="form-group">
                      <label for="exampleInputPassword1">Confirm password</label><br>
                      @if($errors->has('confirm_password'))<label style="color:red" for="usr">{{$errors->first('confirm_password')}}</label>@endif
                      <input type="password" value="{{old('confirm_password')}}" class="form-control @error('confirm_password') is-invalid @enderror" id="exampleInputPassword1" name="confirm_password" placeholder="Confirm password">
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Email address</label><br>
                        @if($errors->has('email'))<label style="color:red" for="usr">{{$errors->first('email')}}</label>@endif
                        <input type="text" value="{{old('email')}}" class="form-control @error('email') is-invalid @enderror" id="exampleInputEmail1" name="email" aria-describedby="emailHelp" placeholder="Enter email">
                    </div>
                    <div class="form-group">
                        <label for="usr">Address</label><br>
                        @if($errors->has('address'))<label style="color:red" for="usr">{{$errors->first('address')}}</label>@endif
                        <input type="text" value="{{old('address')}}" class="form-control @error('address') is-invalid @enderror" id="usr" name="address" placeholder="Address">
                    </div>
                    <div class="form-group">
                      <label for="exampleInputFile">File Image</label><br>
                      @if($errors->has('upload'))<label style="color:red" for="usr">{{$errors->first('upload')}}</label>@endif
                      <div class="input-group">
                          <div class="custom-file">
                              <input type="file" value="{{old('upload')}}" class="custom-file-input @error('upload') is-invalid @enderror" id="exampleInputFile" name="upload">
                              <label class="custom-file-label" for="exampleInputFile">Choose file</label>
                          </div>
                      </div>
                    </div>
                    <div class="form-group">
                        <label for="usr">Date Of Birth</label><br>
                        @if($errors->has('date_of_birth'))<label style="color:red" for="usr">{{$errors->first('date_of_birth')}}</label>@endif
                        <input type="date" value="{{old('date_of_birth')}}" class="form-control @error('date_of_birth') is-invalid @enderror" id="usr" name="date_of_birth">
                    </div>
                    <div class="form-group">
                        <label for="usr">Phone number</label><br>
                        @if($errors->has('phone_number'))<label style="color:red" for="usr">{{$errors->first('phone_number')}}</label>@endif
                        <input type="text" value="{{old('phone_number')}}" class="form-control @error('phone_number') is-invalid @enderror" id="usr" name="phone_number" placeholder="Address">
                    </div>
                    <label for="usr">Sex</label><br>
                    @if($errors->has('sex'))<label style="color:red" for="usr">{{$errors->first('sex')}}</label>@endif
                    <div class="form-group" style="display:flex">
                      <input type="radio" id="male" name="sex" value="1" class="mr-2">
                      <label for="male" class="mr-2">Boy</label><br>
                      <input type="radio" id="male" name="sex" value="2" class="mr-2">
                      <label for="male">Girl</label><br>
                    </div>
                    <label for="usr">Status</label><br>
                    @if($errors->has('status'))<label style="color:red" for="usr">{{$errors->first('status')}}</label>@endif
                    <div class="form-group" style="display:flex">
                      <input type="radio" id="male" name="status" value="0" class="mr-2">
                      <label for="male" class="mr-2">Ban</label><br>
                      <input type="radio" id="male" name="status" value="1" class="mr-2">
                      <label for="male">Show</label><br>
                      <input type="radio" id="male" name="status" value="2" class="mr-2">
                      <label for="male">Hide</label><br>
                    </div>
                    <div class="form-group card ">
                      <h3 for="" class="p-1">Roles</h3>
                      @foreach ($roles as $role)
                        <div class="checkbox p-1">
                          <label for="">
                          <input type="checkbox" name="role[]" value="{{$role->id}}">
                          {{$role->name}}
                          </label>
                        </div>
                      @endforeach
                    </div>
                    <button type="submit" class="btn btn-danger">Add</button>
                </form>
            </div>
        </div>
    </div>
    </section>
</div>
@endsection
