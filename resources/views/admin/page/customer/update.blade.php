@extends('admin.main')
@section('content')
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <div class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1 class="m-0 text-dark">list</h1>
        </div>
        <!-- /.col -->
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="#">Home</a></li>
          </ol>
        </div><!-- /.col -->
      </div><!-- /.row -->
    </div><!-- /.container-fluid -->
  </div>
  <!-- /.content-header -->
 <!-- Main content -->
  <section class="content">
    <div class="container-fluid">
      <!-- Info boxes -->
      <div class="card">
        <form class="form-inline ml-3 mb-5 mt-2">
          <div class="input-group input-group-sm">
            <input class="form-control form-control-navbar" type="search" placeholder="Search" aria-label="Search">
            <div class="input-group-append">
              <button class="btn btn-primary" type="submit">
                <i class="fas fa-search"></i>
              </button>
            </div>
          </div>
        </form>
        <a href="{{route('admin.customer.create')}}" class="btn btn-primary form-control" style="width=70px">Thêm</a>
      </div>
      <!-- /.card -->
    </div>
    <!-- /.col -->
    <section class="content mb-5">

        <div class="container-fluid" >
          <div class="row">
            <div class="col-md-3">
              <!-- Profile Image -->
              <div class="card card-primary card-outline">
                <div class="card-body box-profile">
                  <form role="form" method="POST" action="{{ route('admin.customer.avatar',['customer'=>$customer->id]) }}" enctype="multipart/form-data">
                    @csrf
                    <div class="avatar-upload">
                        <div class="avatar-edit">
                            <input type='file' id="imageUpload" name="upload" accept=".png, .jpg, .jpeg" />
                            <label for="imageUpload"><i class="fas fa-pencil-alt"></i></label>
                        </div>
                        <div class="avatar-preview">
                            <div id="imagePreview" style="background-image:url({{ url('public/upload/customer') }}/{{$customer->image }})">
                            </div>
                        </div>
                      </div>
                      <button type="submit" class="btn btn-primary btn-block"><b>Update</b></button>
                  </form>
                </div>
                <!-- /.card-body -->
              </div>
              <!-- /.card -->
              <!-- About Me Box -->
              <form class="form-horizontal" role="form" method="POST" action="{{ route('admin.customer.update',['customer'=>$customer->id]) }}" enctype="multipart/form-data">
                @csrf @method('PUT')
              <div class="card card-primary">
                <div class="card-header">
                  <h3 class="card-title">About Me</h3>
                </div>
                <!-- /.card-header -->
                <div class="card-body">
                  <strong><i class="fas fa-book mr-1"></i> Email</strong>
                  <p class="text-muted" style="text-align:center">
                    {{ $customer->email }}
                  </p>
                  <hr>
                  <strong><i class="fas fa-map-marker-alt mr-1"></i> Location</strong>
                  <p class="text-muted" style="text-align:center">{{ $customer->address }}</p>
                  <hr>
                  <strong><i class="far fa-file-alt mr-1"></i> Phone number</strong>
                  <p class="text-muted" style="text-align:center">{{ $customer->phone_number }}</p>
                </div>
                <!-- /.card-body -->
              </div>
              <!-- /.card -->
            </div>
            <!-- /.col -->
            <div class="col-md-7">
              <div class="card p-5 ">
                <div class="form-group row">
                  <label for="inputName" class="col-sm-3 col-form-label">Name</label>
                  <div class="col-sm-9">
                    <input type="text" class="form-control @error('name') is-invalid @enderror" name="name" placeholder="Name" value="{{ $customer->name }}">
                  </div>
                </div>
                <div class="form-group row">
                  <label for="inputEmail" class="col-sm-3 col-form-label" >Email</label>
                  <div class="col-sm-9">
                    <input type="email" class="form-control @error('email') is-invalid @enderror" name="email" placeholder="Email" value="{{ $customer->email }}">
                  </div>
                </div>
                <div class="form-group row">
                  <label for="inputName2" class="col-sm-3 col-form-label">Address</label>
                  <div class="col-sm-9">
                    <input type="text" class="form-control @error('address') is-invalid @enderror" name="address" placeholder="Address" value="{{ $customer->address }}">
                  </div>
                </div>
                <div class="form-group row">
                  <label for="inputName2" class="col-sm-3 col-form-label">Phone Number</label>
                  <div class="col-sm-9">
                    <input type="text" class="form-control @error('phone_number') is-invalid @enderror" name="phone_number" placeholder="Phone Number" value="{{ $customer->phone_number }}">
                  </div>
                </div>
                <div class="form-group row">
                  <label for="inputExperience" class="col-sm-3 col-form-label">Date of Birth</label>
                  <div class="col-sm-9">
                    <input type="date" class="form-control @error('date_of_birth') is-invalid @enderror" name="date_of_birth" placeholder="Date of Birth" value="{{ $customer->date_of_birth }}">
                  </div>
                </div>
                <div class="form-group row">
                  <label for="usr" class="col-sm-3 col-form-label">Sex</label>
                  <div class="form-group col-sm-9" style="display:flex">
                    <input type="radio" id="male" name="sex" value="1" class="mr-2" {{($customer->sex)==1?'checked':''}}>
                    <label for="male" class="mr-2">Boy</label><br>
                    <input type="radio" id="male" name="sex" value="2" class="mr-2" {{($customer->sex)==2?'checked':''}}>
                    <label for="male">Girl</label><br>
                  </div>
                </div>
                <div class="form-group row">
                  <label for="usr" class="col-sm-3 col-form-label">Status</label>
                  <div class="form-group col-sm-9" style="display:flex">
                    <input type="radio" id="male" name="status" value="0" class="mr-2" {{($customer->status)==0?'checked':''}}>
                    <label for="male" class="mr-2">Ban</label><br>
                    <input type="radio" id="male" name="status" value="1" class="mr-2" {{($customer->status)==1?'checked':''}}>
                    <label for="male" class="mr-2">Show</label><br>
                    <input type="radio" id="male" name="status" value="2" class="mr-2" {{($customer->status)==2?'checked':''}}>
                    <label for="male">Hide</label><br>
                  </div>
                </div>
                <div class="form-group row">
                  <div class="offset-sm-2 col-sm-10">
                    <button type="submit" class="btn btn-danger">Update</button>
                  </div>
                </div>
              </div>
              <!-- /.nav-tabs-custom -->
            </div>
            <div class="col-md-2 ">
              <div class="form-group card ">
                <h3 for="" class="p-1">Roles</h3>
                @php
                  $role_perrmission = App\Models\role_assignments::where('customer_id',$customer->id)->pluck('role_id')->toArray();
                @endphp
                @foreach ($roles as $role)
                  <div class="checkbox p-1">
                    <label for="">
                    <input type="checkbox" name="role[]" {{(in_array($role->id,$role_perrmission))?'checked':'  '}} value="{{$role->id}}">
                    {{$role->name}}
                    </label>
                  </div>
                @endforeach
              </div>
            </div>
            <!-- /.col -->
          </div>
          <!-- /.row -->
        </div><!-- /.container-fluid -->
      </form>
    </section>
  </section>
@endsection
