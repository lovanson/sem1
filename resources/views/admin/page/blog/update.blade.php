    @extends('admin/main')
    @section('content')
    <!-- Main content -->
    <div class="content-wrapper">
        <div class="content-header">
          <div class="container-fluid">
            <div class="row mb-2">
              <div class="col-sm-6">
                <h1 class="m-0 text-dark">list</h1>
            </div><!-- /.col -->
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                  <li class="breadcrumb-item"><a href="#">Home</a></li>
                  <li class="breadcrumb-item active">list</li>
              </ol>
          </div><!-- /.col -->
      </div><!-- /.row -->
  </div><!-- /.container-fluid -->
</div>
<section>
    <div class="col-md-12">
        <!-- general form elements -->
        <div class="card card-primary">
          <div class="card-header">
            <h3 class="card-title">Thêm mới</h3>
        </div>
        <!-- /.card-header -->
        <!-- form start -->
        <form role="form" method="POST" action="{{ route('admin.blog.update',['blog'=>$blog->id]) }}"  enctype="multipart/form-data">
            @csrf @method('PUT')
            <div class="card-body">


            <div class="form-group">
                <span>Chọn Ảnh tiêu đề</span>
                <div class="input-group">
                    <span class="input-group-btn">
                        <a  data-toggle="modal" href="#model-image" class="btn btn-danger" >Chọn ảnh</a>
                    </span>
                    <input type="text" name="Image" id="Image" class="d-none" value="" >
                    @if($errors->has('Image'))
                    <div class="text-danger">
                        <span >{{$errors->first('Image')}}</span>
                    </div>
                    @endif
                </div>
                <img src="{{ url('filemanager') }}" id="img"  class="p-2 w-25" alt="">
            </div> <div class="form-group">
                <span>Chọn Những ảnh baner-top</span>
                <div class="input-group">
                    <span class="input-group-btn">
                        <a  data-toggle="modal" href="#model-baner-top" class="btn btn-danger" >Chọn ảnh baner-top</a>
                    </span>
                    <input type="text" name="baner_top" id="baner_top" class=""  >
                    @if($errors->has('baner_top'))
                    <div class="text-danger">
                        <span >{{$errors->first('baner_top')}}</span>
                    </div>
                    @endif
                </div>
                <img src="{{ url('filemanager') }}" id="img_baner"  class="p-2 w-25" alt="">
            </div>
            @if($errors->has('upload'))
            <div class="text-danger">
                <span >{{$errors->first('upload')}}</span>
            </div>
            @endif

            <div class="form-group">
                <label for="exampleInputEmail1">Tên blog </label>
                <input type="text" class="form-control @error('Content')border border-danger @enderror" id="Content" name="Content" placeholder="Enter name" value="{{ $blog->Content }}">
                @if($errors->has('Content'))
                <div class="text-danger">
                    <span >{{$errors->first('Content')}}</span>
                </div>
                @endif
            </div>

            <div class="form-group">
                <label for="exampleInputPassword1">Người tạo</label>
                <input type="text" class="form-control @error('Creator')border border-danger @enderror" id="Creator"  name="Creator" placeholder="name" value="{{ $blog->Creator }}">
                @if($errors->has('Creator'))
                <div class="text-danger">
                    <span >{{$errors->first('Creator')}}</span>
                </div>
                @endif
            </div>

            <div class="form-group">
                <label>Tên chọn từ khóa </label>
                <select class="form-control @error('TagID')border border-danger @enderror" name="TagID">
                  <option value="" >chọn tên danh mục</option>
                  @foreach($tag as  $val)
                  <option value="{{ $val->id }}"{{($val->id==$blog->TagID)?'selected':'' }}>{{ $val->Name }}</option>
                  @endforeach
              </select>
              @if($errors->has('TagID'))
              <div class="text-danger">
                <span >{{$errors->first('TagID')}}</span>
            </div>
            @endif
        </div>


      <!-- /.card-body -->
      <div class="card-body pad p-0 ">
          <label for="exampleInputEmail1"> Giới thiệu </label>
          <div class="@error('CatName')border border-danger @enderror">
            <textarea  id="desc" placeholder="Place some text here"
            style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;" name="Contents">{{ $blog->Contents }}</textarea>
        </div>
        @if($errors->has('Contents'))
        <div class="text-danger">
            <span >{{$errors->first('Contents')}}</span>
        </div>
        @endif
     </div>

</div>
 </div>

<div class="card-footer">
    <button type="submit" class="btn btn-primary">Submit</button>
</div>
</form>
</div>
<!-- /.card -->

</div>
<!-- /.card -->

</section>
</div>
<!-- Modal 1 -->
<div class="modal fade" id="model-image" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true" >
    <div class="modal-dialog" style=" width: 1000px;max-width: 1000px;">
        <div class="modal-content"style="width: 1000px;height : 800px;">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLongTitle">Modal title</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
          </button>
      </div>
      <div class="modal-body ">
       <iframe src="{{ url('filemanager') }}/dialog.php?field_id=Image" class="w-100 h-100" ></iframe>
   </div>
   <div class="modal-footer">
    <button type="button" class="btn btn-primary|secondary|success|danger|warning|info|light|dark" data-dismiss="modal">Close</button>
    <button type="button" class="btn btn-primary|secondary|success|danger|warning|info|light|dark">Save changes</button>
</div>
</div>
</div>
</div>
<!-- Modal 3 -->
<div class="modal fade" id="model-baner-top" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitless" aria-hidden="true" >
  <div class="modal-dialog" style=" width: 1000px;max-width: 1000px;">
    <div class="modal-content"style="width: 1000px;height : 800px;">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitless">Modal title</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
      </button>
  </div>
  <div class="modal-body ">
     <iframe src="{{ url('filemanager') }}/dialog.php?field_id=baner_top" class="w-100 h-100" ></iframe>
 </div>
 <div class="modal-footer">
    <button type="button" class="btn btn-primary|secondary|success|danger|warning|info|light|dark" data-dismiss="modal">Close</button>
    <button type="button" class="btn btn-primary|secondary|success|danger|warning|info|light|dark">Save changes</button>
</div>
</div>
</div>
</div>
<!-- Modal 2 -->
<div class="modal fade" id="model-image2" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitles" aria-hidden="true" >
    <div class="modal-dialog" style=" width: 1000px;max-width: 1000px;">
        <div class="modal-content"style="width: 1000px;height : 800px;">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLongTitles">Modal title</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
          </button>
      </div>
      <div class="modal-body ">
       <iframe src="{{ url('filemanager') }}/dialog.php?field_id=Images" class="w-100 h-100" ></iframe>
   </div>
   <div class="modal-footer">
    <button type="button" class="btn btn-primary|secondary|success|danger|warning|info|light|dark" data-dismiss="modal">Close</button>
    <button type="button" class="btn btn-primary|secondary|success|danger|warning|info|light|dark">Save changes</button>
</div>
</div>
</div>


@endsection
@section('script')

<script>
 $('#model-image').on('hide.bs.modal',function(){
    // lấy value của input có id=images
    var image = $('#Image').val();
    $('#img').attr('src',image);
});$('#model-image').on('hide.bs.modal',function(){
    // lấy value của input có id=images
    var baner_top = $('#baner_top').val();
    $('#img_baner').attr('src',baner_top);
});

$('#model-image2').on('hide.bs.modal',function(){
    // lấy value của input có id=images
    var Images = $('input#Images').val();
    // $('img#ImagesSHOW').attr('src',Images);
    var imgList = $.parseJSON(Images);
    var _html = '';
    for(var i=0;i<imgList.length;i++){
      _html +='<div class="col-md-3 thumbneil">';
      _html +='<img src="'+imgList[i]+'"class="img-responsive" style="max-height: 100px">';
      _html +='</div>';
  }
  $('#show').html(_html);
});
</script>

@endsection
