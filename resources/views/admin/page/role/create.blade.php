@extends('admin.main')
@section('content')
<div class="content-wrapper" style="padding:20px" ng-app="role" ng-controller="roleController">
    <div class="content-header">
      <div class="container">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Add Role</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Add User</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <section>
    <div class="container">
      <!-- general form elements -->
      <div class="card card-primary">
          <div class="card-header">
              <h3 class="card-title">Add New</h3>
          </div>
      </div>
      <form role="form" method="POST" action="{{route('admin.role.store')}}" enctype="multipart/form-data">
      @csrf
        <div class="form-group ">
            <label for="usr">Permission Group Name</label>
            <input type="text" class="form-control" id="usr" name="name" placeholder="Permission Group Name">
        </div>
        <div class="row">
          <div class="col-md-4">
            <h3 for="usr">Routes</h3>
          </div>
          <div class="col-md-4 navbar navbar-expand  navbar-light">
              <div class="input-group input-group-sm">
                <input class="form-control" type="search" ng-model="rname" placeholder="Input role Name" aria-label="Search">
                <div class="input-group-append">
                  <a class="btn btn-navbar" >
                    <i class="fas fa-search"></i>
                  </a>
                </div>
              </div>
          </div>
          <div class="col-md-4">
              <input type="checkbox" id="check-all" class="ml-5 mt-3 mr-1">Check All
          </div>
        </div>
        <div class="card ">
          <div class="row ">
            <div class="col-md-4 " ng-repeat="a in roles | filter:rname">
              <div class="checkbox">
                <input type="checkbox" class="role-item" name="route[]" value="@{{a}}">
                @{{a}}
              </div> 
            </div>                   
          </div>
        </div>
        <button type="submit" class="btn btn-danger" style="width: 100px">Add</button>
      </form>
    </div>
  </section>
</div>
@endsection
@section('angularjs')
<script src="https://cdnjs.cloudflare.com/ajax/libs/angular.js/1.8.0/angular.min.js"></script>
<script type="text/javascript">
  var app = angular.module('role',[]);
  app.controller('roleController',function($scope){
    var roles = '<?php echo json_encode($routes);?>';
    $scope.roles = angular.fromJson(roles);
  });
  $('#check-all').click(function(){
    $('.role-item').not(this).prop('checked',this.checked);
  })
</script>    
@endsection