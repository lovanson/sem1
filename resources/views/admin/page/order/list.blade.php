@extends('admin/main')
@section('content')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">list</h1>
                     </div><!-- /.col -->

          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">list</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->
 <!-- Main content -->
   <section class="content">
      <div class="container-fluid">
        <!-- Info boxes -->
            <div class="card">

              <!-- /.card-header -->
              <div class="card-body ">
                <div class="table-responsive">
                  <table class="table m-0" id="example1">
                    <thead>
                    <tr>
                      <th>ID</th>
                      <th>UserID</th>
                      <th>PaymentID</th>
                      <th>Status</th>
                      <th>Total amount</th>
                      <th>
                        <a href="{{ route('admin.order.create') }}" class="btn btn-xs btn-primary">Thêm</a>
                      </th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($cats as $model)
                      <tr>
                        <td>{{ $model->id }}</td>
                            {{-- @foreach($model->customer as $val) --}}
                          <td>{{ $model->customer->name }}</td>
                             {{-- @endforeach --}}
                          <td>{{ $model->payment->Payment_type }}</td>
                            <td>{{ ($model->Status ==1)?"Hiện":"Ẩn"}}</td>
                            <td>{{ $model->Totalamount}}</td>
                            <td>

                              <form action="{{ route('admin.order.destroy',$model->id) }}" method="POST" accept-charset="utf-8">
                                @csrf @method('DELETE')
                                <div class="btn btn-xs btn-primary">
                                   <a data-toggle="modal" href="{{ route('admin.orderview') }}" class="text-white js_model" data-id="{{ $model->id }}"> Xem chi tiết</a>
                                </div>
                                <button type="submit" class="btn btn-xs btn-danger  " onclick="return confirm('Bạn có chắc muốn xóa không') " >Xóa</button>
                              </form>

                            </td>
                      </tr>
                      @endforeach
                    </tbody>
                  </table>
                </div>
                <!-- /.table-responsive -->
              </div>
              <!-- /.card-footer -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
          <div class="modal fade" id="modal-order" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true" >
          <div class="modal-dialog " style=" width: 1000px;max-width: 1000px;">
            <div class="modal-content"style="width: 1000px;height : 800px;">
              <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Chi tiết đơn hàng :<b class="data-id"></b></h5>

                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div class="modal-body " id="content_id">

              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-primary|secondary|success|danger|warning|info|light|dark" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary|secondary|success|danger|warning|info|light|dark">Save changes</button>
              </div>
            </div>
          </div>
        </div>
@section('script')

<script>
  $(function(){
   $(".js_model").click(function(event){
      var data_id = $(this).attr("data-id");
        let url = $(this).attr('href');
        $.ajax({
            url: url,
            type: 'GET',
            data: {id:data_id},
            success: function(data){
                $("#content_id").html(data);
                $("#modal-order").modal('show');
            }
        });
   });
  });
</script>
@endsection

@endsection
@section('data_table')
<script>
  $(function () {
    $("#example1").DataTable();
  });
</script>
@endsection
