<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>AdminLTE 3 | Registration Page</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <base href="{{asset('public/admin')}}/">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="plugins/fontawesome-free/css/all.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="plugins/toastr.css">
  <link rel="stylesheet" href="plugins/toastr.min.css">
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- icheck bootstrap -->
  <link rel="stylesheet" href="plugins/icheck-bootstrap/icheck-bootstrap.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="dist/css/adminlte.min.css">
  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
  <style>
    .is-invalid{
      background-image: none !important;
    }
  </style>
</head>
<body class="hold-transition register-page">
<div class="register-box">
  <div class="register-logo">
    <a><b>Admin</b>LTE</a>
  </div>

  <div class="card">
    <div class="card-body register-card-body">
      <p class="login-box-msg">Register a new membership</p>

    <form action="{{route('admin.post_register')}}" method="post">
        @csrf
        <div class="input-group mb-3">
          <input value="{{old('name')}}" class="form-control @error('name') is-invalid @enderror" id="usr" type="text" class="form-control" name="name" placeholder="Full name">
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-user"></span>
            </div>
          </div>
        </div>
        <div class="form-group">
            <input value="{{old('date_of_birth')}}" class="form-control @error('date_of_birth') is-invalid @enderror" id="usr" name="date_of_birth" placeholder="Date Of Birth" onfocus="(this.type='date')" class="textbox-n" type="text">
        </div>
        <div class="form-group input-group">
          <input type="text" value="{{old('phone_number')}}" class="form-control @error('phone_number') is-invalid @enderror" id="usr" name="phone_number" placeholder="Phone number">
          <div class="input-group-append">
              <div class="input-group-text">
                <span class="fa fa-phone"></span>
              </div>
            </div>
        </div>
        <div class="form-group input-group">
          <input type="text" value="{{old('address')}}" class="form-control @error('address') is-invalid @enderror" id="usr" name="address" placeholder="Address">
          <div class="input-group-append">
              <div class="input-group-text">
                <span class="fa fa-map-marker"></span>
              </div>
            </div>
        </div>
        <div class="input-group mb-3">
          <input type="text" class="form-control" placeholder="Email" name="email" value="{{old('email')}}" @error('email') is-invalid @enderror>
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-envelope"></span>
            </div>
          </div>
        </div>
        <div class="input-group mb-3">
          <input type="password" class="form-control" placeholder="Password" name="password" value="{{old('password')}}" @error('password') is-invalid @enderror>
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-lock"></span>
            </div>
          </div>
        </div>
        <div class="input-group mb-3">
          <input type="password" class="form-control" placeholder="Confirm password" name="confirm_password" value="{{old('confirm_password')}}" @error('confirm_password') is-invalid @enderror>
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-lock"></span>
            </div>
          </div>
        </div>
        <label for="usr">Sex</label><br>
        <div class="form-group" style="display:flex"> 
            <input type="radio" id="male" name="sex" value="1" class="mr-2" checked>
            <label for="male" class="mr-2">Boy</label><br>
            <input type="radio" id="male" name="sex" value="2" class="mr-2">
            <label for="male">Girl</label><br>
        </div>
       
        <div class="row">
          <div class="col-8">
            <div class="icheck-primary">
              <input type="checkbox" id="agreeTerms" name="terms" value="agree">
              <label for="agreeTerms">
               I agree to the <a href="#">terms</a>
              </label>
            </div>
          </div>
          <!-- /.col -->
          <div class="col-4">
            <button type="submit" class="btn btn-primary btn-block">Register</button>
          </div>
          <!-- /.col -->
        </div>
      </form>
    <a href="{{route('admin.getlogin')}}" class="text-center">I already have a membership</a>
    </div>
    <!-- /.form-box -->
  </div><!-- /.card -->
</div>
<!-- /.register-box -->

<!-- jQuery -->
<script src="plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap 4 -->
<script src="plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- AdminLTE App -->
<script src="dist/js/adminlte.min.js"></script>
<script src="plugins/toastr.min.js"></script>
<script type="text/javascript">
  toastr.options = 
      {
          newestOnTop      : true,
          closeButton      : false,
          progressBar      : true,
          preventDuplicates: false,
          showMethod       : 'slideDown',
          timeOut          : 2000, //default timeout,
      };
  @if(Session::has('message'))
    var type = "{{ Session::get('alert-type', 'info') }}";
    switch(type){
        case 'info':
            toastr.info("{{ Session::get('message') }}");
            break;
        case 'warning':
            toastr.warning("{{ Session::get('message') }}");
            break;
        case 'success':
            toastr.success("{{ Session::get('message') }}");
            break;
        case 'error':
            toastr.error("{{ Session::get('message') }}");
            break;
    }
  @endif
</script>
</body>
</html>
