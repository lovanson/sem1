<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>AdminLTE 3 | Dashboard</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- Font Awesome -->
  <base href="{{asset('public/admin')}}/">
  <link rel="stylesheet" href="plugins/fontawesome-free/css/all.min.css">
  <link rel="stylesheet" href="plugins/fontawesome-free/css/fontawesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Tempusdominus Bbootstrap 4 -->
  <link rel="stylesheet" href="plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css">
  <!-- DataTables -->
  <link rel="stylesheet" href="plugins/datatables-bs4/css/dataTables.bootstrap4.min.css">
  <link rel="stylesheet" href="plugins/datatables-responsive/css/responsive.bootstrap4.min.css">
  <!-- iCheck -->
  <link rel="stylesheet" href="plugins/icheck-bootstrap/icheck-bootstrap.min.css">
  <link rel="stylesheet" href="plugins/toastr.css">
  <link rel="stylesheet" href="plugins/toastr.min.css">
  <!-- JQVMap -->
  <link rel="stylesheet" href="plugins/jqvmap/jqvmap.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="dist/css/adminlte.min.css">
  <!-- overlayScrollbars -->
  <link rel="stylesheet" href="plugins/overlayScrollbars/css/OverlayScrollbars.min.css">
  <!-- Daterange picker -->
  <link rel="stylesheet" href="plugins/daterangepicker/daterangepicker.css">
  <!-- summernote -->
  <link rel="stylesheet" href="plugins/summernote/summernote-bs4.css">
  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
  <style>
    h1 small {
      display: block;
      font-size: 15px;
      padding-top: 8px;
      color: gray;
    }
    .avatar-upload {
      position: relative;
      max-width: 205px;
      margin: 50px auto;
    }
    .avatar-upload .avatar-edit {
      position: absolute;
      right: 12px;
      z-index: 1;
      top: 10px;
    }
    .avatar-edit input {
      display: none;
    }
    .avatar-edit input + label {
      display: inline-block;
      width: 34px;
      height: 34px;
      margin-bottom: 0;
      border-radius: 100%;
      background: #FFFFFF;
      border: 1px solid transparent;
      box-shadow: 0px 2px 4px 0px rgba(0, 0, 0, 0.12);
      cursor: pointer;
      font-weight: normal;
      transition: all .2s ease-in-out;
    }
    .avatar-upload .avatar-edit label:hover {
      background: #f1f1f1;
      border-color: #d6d6d6;
    }
    .avatar-upload .avatar-preview {
      width: 192px;
      height: 192px;
      position: relative;
      border-radius: 100%;
      border: 6px solid #F8F8F8;
      box-shadow: 0px 2px 4px 0px rgba(0, 0, 0, 0.1);
    }
    .avatar-upload .avatar-preview  div {
      width: 100%;
      height: 100%;
      border-radius: 100%;
      background-size: cover;
      background-repeat: no-repeat;
      background-position: center;
    }
    .avatar-upload .avatar-edit input + label i{
      color:#757575;
      position:absolute;
      top:10px;
      left:0;
      right: 0;
      text-align: center;
      margin: auto;
    }
    .dropdown:hover .dropdown-content {
    display: block;
    }
    .dropdown {
      position: relative;
      display: inline-block;
    }

    .dropdown-content {
      display: none;
      position: absolute;
      background-color: #f9f9f9;
      min-width: 160px;
      box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
      padding: 12px 16px;
      z-index: 1;
    }
  </style>
</head>
<body class="hold-transition sidebar-mini layout-fixed">
<?php
  $customer = Auth::user();
  $menu = config('menuadmin');
?>
<div class="wrapper">
  <!-- Navbar -->
  <nav class="main-header navbar navbar-expand navbar-white navbar-light">
    <!-- Left navbar links -->
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
      </li>
      <li class="nav-item d-none d-sm-inline-block">
        <a href="{{ route('admin.index') }}" class="nav-link">Home</a>
      </li>

    </ul>
    <!-- SEARCH FORM -->

    <!-- Right navbar links -->
    <ul class="navbar-nav ml-auto">
      <div class="btn-group dropdown d-flex" style="    margin-right: 136px;">
      <!-- Messages Dropdown Menu -->
      <!-- Notifications Dropdown Menu -->
        <div class="image d-flex">
        <img  style="width: 50px" src="{{ url('public/upload/customer') }}/{{Auth::user()->image}}" class="img-circle elevation-2" alt="User Image">
        </div>
      <div type="button" class="user-panel d-flex " >
        <div class="info">
          <a class="d-block" style="font-size: 20px; ml-1">{{Auth::user()->name}}</a>
        </div>
      </div>
      <div class=" dropdown-content" style="    margin-top: 50px;">
        <div class="info user-panel dropdown-item">
          <i class="fas fa-edit" type="button" data-toggle="modal" data-target="#myModal" style="font-size: 17px">Change Password</i>
        </div>
        <div class="info user-panel dropdown-item">
          <a href="{{ route('admin.getlogout') }}"  style="font-size:17px;margin-top:-1px;color:black">
            <i class="fas fa-sign-out-alt mr-1" style="font-size: 17px">
              Logout
            </i>

          </a>
        </div>
      </div>
      </div>
    </ul>
  </nav>
  <!-- /.navbar -->

  <!-- Main Sidebar Container -->
  <aside class="main-sidebar sidebar-dark-primary elevation-4">

    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
          @if($customer -> can('admin.customer.create'))
          <li class="nav-item has-treeview">
            <a class="nav-link">
              <i class="nav-icon fas fa-edit"></i>
              <p>
                Customer
                <i class="fas fa-angle-left right"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="{{ route('admin.customer.create') }}" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Add Customer</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="{{ route('admin.customer.index') }}" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>List Customer</p>
                </a>
              </li>
            </ul>
          </li>
          @endif

          @if($customer -> can('admin.role.create'))
          <li class="nav-item has-treeview">
            <a class="nav-link">
              <i class="nav-icon fas fa-edit"></i>
              <p>
                Role
                <i class="fas fa-angle-left right"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="{{ route('admin.role.create') }}" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Add Role</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="{{ route('admin.role.index') }}" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>List Role</p>
                </a>
              </li>
            </ul>
          </li>
          @endif
          @if($customer -> can('admin.category.create'))
          <li class="nav-item has-treeview">
            <a class="nav-link">
              <i class="nav-icon fas fa-edit"></i>
              <p>
                Danh mục(Category)
                <i class="fas fa-angle-left right"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="{{ route('admin.category.create') }}" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Thêm danh mục</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="{{ route('admin.category.index') }}" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Danh sách danh mục</p>
                </a>
              </li>
            </ul>
          </li>
          @endif
            @if($customer -> can('admin.tag.create'))
          <li class="nav-item has-treeview">
            <a class="nav-link">
              <i class="nav-icon fas fa-edit"></i>
              <p>
                Thẻ
                <i class="fas fa-angle-left right"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="{{ route('admin.tag.create') }}" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Thêm Dịch vụ</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="{{ route('admin.tag.index') }}" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Danh sách dịch vụ</p>
                </a>
              </li>
            </ul>
          </li>
          @endif
           @if($customer -> can('admin.payment.create'))
          <li class="nav-item has-treeview">
            <a class="nav-link">
              <i class="nav-icon fas fa-edit"></i>
              <p>
                Pt thanh toán
                <i class="fas fa-angle-left right"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="{{ route('admin.payment.create') }}" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Thêm Pt thanh toán</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="{{ route('admin.payment.index') }}" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Danh sách pt thanh toán</p>
                </a>
              </li>
            </ul>
          </li>
          @endif
          @if($customer -> can('admin.guide.create'))
          <li class="nav-item has-treeview">
            <a class="nav-link">
              <i class="nav-icon fas fa-edit"></i>
              <p>
                Hướng dẫn viên
                <i class="fas fa-angle-left right"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="{{ route('admin.guide.create') }}" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Thêm hướng dẫn viên </p>
                </a>
              </li>
              <li class="nav-item">
                <a href="{{ route('admin.guide.index') }}" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Danh sách </p>
                </a>
              </li>
            </ul>
          </li>
          @endif

          @if($customer -> can('admin.service.create'))
          <li class="nav-item has-treeview">
            <a class="nav-link">
              <i class="nav-icon fas fa-edit"></i>
              <p>
                Dịch vụ
                <i class="fas fa-angle-left right"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="{{ route('admin.service.create') }}" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Thêm Dịch vụ</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="{{ route('admin.service.index') }}" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Danh sách dịch vụ</p>
                </a>
              </li>
            </ul>
          </li>
          @endif

          @if($customer -> can('admin.contact.index'))
          <li class="nav-item has-treeview">
            <a class="nav-link">
              <i class="nav-icon fas fa-edit"></i>
              <p>
                ý kiến khách hàng
                <i class="fas fa-angle-left right"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="{{ route('admin.contact.index') }}" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Danh sách ý kiến</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="{{ route('admin.rating.index') }}" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Nhận xét khách hàng</p>
                </a>
              </li>
            </ul>
          </li>
          @endif

          @if($customer -> can('admin.customer_service.create'))
          <li class="nav-item has-treeview">
            <a class="nav-link">
              <i class="nav-icon fas fa-edit"></i>
              <p>
                Phản hồi khách hàng
                <i class="fas fa-angle-left right"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="{{ route('admin.customer_service.index') }}" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Danh sách</p>
                </a>
              </li>
            </ul>
          </li>
          @endif

          <!-- ---------------------- Tour ------------------- -->
           @if($customer -> can('admin.tour.create'))
          <li class="nav-item has-treeview">
            <a href="{{ route('admin.tour.index') }}" class="nav-link">
              <i class="nav-icon fas fa-edit"></i>
              <p>
                Tour
                <i class="fas fa-angle-left right"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="{{ route('admin.tour.create') }}" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Thêm danh mục</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="{{ route('admin.tour.index') }}" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Danh sách danh mục</p>
                </a>
              </li>
            </ul>
          </li>
           @endif

            @if($customer -> can('admin.order.create'))
          <li class="nav-item has-treeview">
            <a href="{{ route('admin.order.index') }}" class="nav-link">
              <i class="nav-icon fas fa-edit"></i>
              <p>
                Order
                <i class="fas fa-angle-left right"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="{{ route('admin.order.create') }}" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Thêm danh mục</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="{{ route('admin.order.index') }}" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Danh sách </p>
                </a>
              </li>
            </ul>
          </li>
           @endif

             @if($customer -> can('admin.blog.create'))
          <li class="nav-item has-treeview">
            <a href="{{ route('admin.blog.index') }}" class="nav-link">
              <i class="nav-icon fas fa-edit"></i>
              <p>
                Blog
                <i class="fas fa-angle-left right"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="{{ route('admin.blog.create') }}" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Thêm </p>
                </a>
              </li>
              <li class="nav-item">
                <a href="{{ route('admin.blog.index') }}" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Danh sách </p>
                </a>
              </li>
            </ul>
          </li>
           @endif


          <li class="nav-item has-treeview">
            <a class="nav-link">
              <i class="nav-icon fas fa-edit"></i>
              <p>
                Gallery
                <i class="fas fa-angle-left right"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="{{ route('admin.gallery.create') }}" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Add Gallery</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="{{ route('admin.gallery.index') }}" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>List Gallery</p>
                </a>
              </li>
            </ul>
          </li>
          <li class="nav-item has-treeview">
            <a class="nav-link">
              <i class="nav-icon fas fa-edit"></i>
              <p>
                Role Assignment
                <i class="fas fa-angle-left right"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="{{ route('admin.role_assignment.create') }}" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Add Role Assignment</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="{{ route('admin.role_assignment.index') }}" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>List Role Assignment</p>
                </a>
              </li>
            </ul>
          </li>
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>

  <!-- Content Wrapper. Contains page content -->



   @yield('content')



  <!-- /.content-wrapper -->

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">

  </aside>
  <!-- /.control-sidebar -->
</div>
<div class="modal fade" id="myModal">
  <div class="modal-dialog">
    <div class="modal-content">

      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title">Change Password</h4>
        <button type="button" class="close" data-dismiss="modal">×</button>
      </div>

      <!-- Modal body -->
        <form action="{{route('admin.post_change_pasword')}}" method="POST">
          @csrf
          <div class="modal-body">
            <div class="input-group mb-3">
              <input type="password" class="form-control" placeholder="Password" name="password">
              <div class="input-group-append">
                <div class="input-group-text">
                  <span class="fas fa-lock"></span>
                </div>
              </div>
            </div>
            <div class="input-group mb-3">
              <input type="password" class="form-control" placeholder="New Password" name="newpassword">
              <div class="input-group-append">
                <div class="input-group-text">
                  <span class="fas fa-lock"></span>
                </div>
              </div>
            </div>
            <div class="input-group mb-3">
              <input type="password" class="form-control" placeholder="Confirm New Password" name="confirmnewpassword">
              <div class="input-group-append">
                <div class="input-group-text">
                  <span class="fas fa-lock"></span>
                </div>
              </div>
            </div>
          </div>
          <!-- Modal footer -->
          <div class="modal-footer">
            <button type="submit" class="btn btn-danger" >Save Change</button>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
<!-- ./wrapper -->
</body>
<!-- jQuery -->
<script src="plugins/jquery/jquery.min.js"></script>
<!-- jQuery UI 1.11.4 -->
<script src="plugins/jquery-ui/jquery-ui.min.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button)
</script>
<!-- Bootstrap 4 -->
<script src="plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- ChartJS -->
<script src="plugins/chart.js/Chart.min.js"></script>
<!-- Sparkline -->
<script src="plugins/sparklines/sparkline.js"></script>
<!-- JQVMap -->
<script src="plugins/jqvmap/jquery.vmap.min.js"></script>
<script src="plugins/jqvmap/maps/jquery.vmap.usa.js"></script>
<!-- jQuery Knob Chart -->
<script src="plugins/jquery-knob/jquery.knob.min.js"></script>
<!-- daterangepicker -->
<script src="plugins/moment/moment.min.js"></script>
<script src="plugins/daterangepicker/daterangepicker.js"></script>
<!-- Tempusdominus Bootstrap 4 -->
<script src="plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js"></script>
<!-- Summernote -->
<script src="plugins/summernote/summernote-bs4.min.js"></script>
<!-- overlayScrollbars -->
<script src="plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js"></script>
<!-- AdminLTE App -->
<script src="dist/js/adminlte.js"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script src="dist/js/pages/dashboard.js"></script>
<!-- AdminLTE for demo purposes -->
<!-- DataTables -->
<script src="plugins/datatables/jquery.dataTables.min.js"></script>
<script src="plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>
<script src="plugins/datatables-responsive/js/dataTables.responsive.min.js"></script>
<script src="plugins/datatables-responsive/js/responsive.bootstrap4.min.js"></script>
{{-- Toastr --}}
<script src="plugins/toastr.min.js"></script>
<script src="dist/js/demo.js"></script>
<script src="tinymce/tinymce.min.js"></script>
<script src="tinymce/config.js"></script>
<script src="plugins/bs-custom-file-input/bs-custom-file-input.min.js"></script>
@yield('angularjs')
@yield('script')
<script>
  function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function(e) {
            $('#imagePreview').css('background-image', 'url('+e.target.result +')');
            $('#imagePreview').hide();
            $('#imagePreview').fadeIn(650);
        }
        reader.readAsDataURL(input.files[0]);
    }
}
$("#imageUpload").change(function() {
    readURL(this);
});
</script>
<script type="text/javascript">
  toastr.options =
      {
          newestOnTop      : true,
          closeButton      : false,
          progressBar      : true,
          preventDuplicates: false,
          showMethod       : 'slideDown',
          timeOut          : 2000, //default timeout,
      };
  @if(Session::has('message'))
    var type = "{{ Session::get('alert-type', 'info') }}";
    switch(type){
        case 'info':
            toastr.info("{{ Session::get('message') }}");
            break;
        case 'warning':
            toastr.warning("{{ Session::get('message') }}");
            break;
        case 'success':
            toastr.success("{{ Session::get('message') }}");
            break;
        case 'error':
            toastr.error("{{ Session::get('message') }}");
            break;
    }
  @endif
</script>
@yield('data_table')
<script>
  $(document).ready(function(){
    bsCustomFileInput.init();
  });
</script>
</html>
