<!DOCTYPE html>
<html lang="en">


<!-- Mirrored from andit.co/projects/html/wend/demo/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 26 May 2020 08:29:23 GMT -->
<!-- Added by HTTrack --><meta http-equiv="content-type" content="text/html;charset=UTF-8" /><!-- /Added by HTTrack -->
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="Wend - Tour, Travel, Travel Agency Template">
    <meta name="keywords" content="Tour, Travel, Travel Agency Template">
    <meta name="viewport" content="initial-scale=1, maximum-scale=1">
    <!-- Title-->
    <title>Travel | Home</title>


    <base href="{{asset('public/User')}}/" target="_top">
    <!--Favicon add-->
    <link rel="apple-touch-icon" sizes="57x57" href="favicon/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="favicon/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="favicon/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="favicon/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="favicon/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="favicon/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="favicon/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="favicon/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="favicon/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192" href="favicon/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="favicon/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="favicon/favicon-16x16.png">
    <link rel="manifest" href="favicon/manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="favicon/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">
    <!-- Bootstrap-->
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <!--magnific-popup.cs-->
    <link rel="stylesheet" href="css/magnific-popup.css">
    <!--owl css-->
    <link rel="stylesheet" href="css/owl.carousel.min.css">
    <link rel="stylesheet" href="css/owl.theme.default.min.css">
    <!--    normalize css-->
    <link rel="stylesheet" href="css/normalize.css">
    <!--    flacticon css-->
    <link rel="stylesheet" href="font/flaticon.css">
    <!--    animate css-->
    <link rel="stylesheet" href="css/animate.css">

    <link href="bootstrap-datepicker/1.3.0/css/datepicker.css" rel="stylesheet"
        type="text/css" />
    <!--Fontawesom -->
    <link rel="stylesheet" href="css/all.css" />
        <link rel="stylesheet" type="text/css" href="fontawesome/fontawesome-free-5.13.0-web/css/all.min.css">
    <!--Font Famely-->
    <!--Poppins-->
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700,800,900&amp;display=swap"
        rel="stylesheet">
    <!--Lato-->
    <link href="https://fonts.googleapis.com/css?family=Lato:300,400,700,900&amp;display=swap" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="css/font-awesome.css">
    <link rel="stylesheet" type="text/css" href="css/pygments.css">

    <link rel="stylesheet" type="text/css" href="css/ion.rangeSlider.css">
    <link rel="stylesheet" type="text/css" href="css/ion.rangeSlider.min.css">
    <link rel="stylesheet" type="text/css" href="css/toastr.css">
    <!-- DataTables -->
    <link rel="stylesheet" href="plugins/datatables-bs4/css/dataTables.bootstrap4.min.css">
    <link rel="stylesheet" href="plugins/datatables-responsive/css/responsive.bootstrap4.min.css">
    <!-- style.css-->
    <link rel="stylesheet" href="css/style.css">
    <!-- responsive.css-->
    <link rel="stylesheet" href="css/responsive.css">
<body>
  <div id="preloader">
    <div id="status">&nbsp;</div>
</div>
<!-- preloader-->
<!-- top header-->
<div class="header-most-top">
    <div class="container">
        <div class="row">
            <div class="col-lg-7 col-md-8 col-sm-8 col-12">
                <div class="main-flex-top">
                    <div class="email sel d-flex">
                        <div class="main-email-text d-flex">
                          <i class="fas fa-envelope" aria-hidden="true"></i>
                            <p>infp@a2z.com</p>
                        </div>
                        <div class="main-loc-text d-flex">
                            <i class="fas fa-map-marker-alt"></i>
                            <p>78 5th Ave, New York, Ny 10011, USA</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-5 col-md-4 col-sm-4 col-12 divusername" id="load_name_login">
                @if(!Auth::user())
                <div class="login-area">
                    <div class="User-log">

                        <a  data-toggle="modal" data-target="#myModal2" ><i class="far fa-user-circle m-1 " ></i>Sign In</a>
                    </div>
                </div>
                @else
                <div id="mySidenav" class="sidenav1">
                    <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>
                    <div class="card-primary card-outline">
                        <div class=" box-profile" >
                            <form role="form" method="POST" action="{{ route('customer.avatar',[Auth::user()->id ]) }}" enctype="multipart/form-data">
                            @csrf
                                <div class="avatar-upload">
                                    <div class="avatar-edit">
                                        <input type='file' id="imageUpload" name="upload" accept=".png, .jpg, .jpeg" />
                                        <label for="imageUpload"><i class="fas fa-pencil-alt"></i></label>
                                    </div>
                                    <div class="avatar-preview" style="overflow: hidden;">
                                    @if(Auth::user()->image != Null)
                                        <div id="imagePreview" >
                                                  <img class="w-100 " style="border-radius: 50px ;" src="{{ url('public/upload/customer')}} /{{Auth::user()->image }}" alt="">
                                        </div>
                                    @else
                                     <div id="imagePreview">
                                        <img class="w-100" src="https://i1.wp.com/placehold.it/160x160/38729e/ffffff/&text={{ Str::limit(Auth::user()->name, 1, $end='')  }}" alt="">
                                        </div>
                                        {{-- <img src="https://i1.wp.com/placehold.it/160x160/00a65a/ffffff/&text={{ Str::limit(Auth::user()->name, 1, $end='')  }}" style="border-radius: 60%; width:80px"> --}}
                                    @endif
                                    </div>
                                </div>
                                <a  style="font-size: 25px;color:#fff">{{Auth::user()->name}}</a>
                                <button type="submit" class="btn btn-1" style="margin:0 0px 20px 75px"><b>Update</b></button>
                            </form>
                            <a href="{{ route('User.logout') }}"  style="font-size:20px; color:#fff;border-bottom:1px solid #fff;border-top:1px solid #fff">
                                <i class="fas fa-sign-out-alt mr-1" style="font-size: 20px;color:#fff">
                                    Logout
                                </i>
                            </a>
                            <a href="#" data-toggle="modal" data-target="#usermyModal" style="color:#fff;margin-top:-2px;font-size:20px;border-bottom:1px solid #fff" >
                                <i class="far fa-edit" style="font-size: 20px;color:#fff">Change Password</i>
                            </a>
                            <a href="{{route('User.information')}}" style="color:#fff;margin-top:-2px;font-size:20px;border-bottom:1px solid #fff" >
                                <i class="fas fa-bars" style="font-size: 20px;color:#fff">My Tour</i>
                            </a>
                        </div>
                        <!-- /.card-body -->
                      </div>
                </div>

                <span  class="nameuserlogin" onclick="openNav()"><i class="far fa-user-circle " style="margin-right:4px"> </i>{{ Auth::user()->name}}</span>
                </div>
                @endif
            </div>
        </div>
    </div>
</div>
<!-- top header end-->
<div class="modal fade" id="usermyModal" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <div class="model-details">
                    <h5>Change Password Form</h5>
                    <meta name="csrf-token" content="{{ csrf_token() }}">
                    <div class="mdel-form">
                        <form action="{{ route('User.post_change_pasword') }}" method="POST">
                        @csrf
                        <div class="form-group">
                            <label for="name">Password</label>
                            <input type="password" class="form-control" placeholder="Enter your password" name="password" >
                        </div>
                        <div class="form-group">
                            <label for="password">New Password</label>
                            <input type="password" class="form-control" name="newpassword" placeholder="Enter your new pasword">
                        </div>
                        <div class="form-group">
                            <label for="name">Confirm Password</label>
                            <input type="text" class="form-control" id="confirm_passwords" name="confirm_password" placeholder="Confirm your pasword">
                        </div>
                        <br/>
                        <div class="sunb-btn-mod">
                            <button type="submit" class="btn btn-3 widet-2">Save Change</button>
                        </div><br/>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- navber start-->
<header id="top-heder-nav">
    <nav class="navbar navbar-expand-lg" data-toggle="sticky-onscroll">
        <div class="container">
            <a class="navbar-brand" href="{{ Route('User.home') }} ">
                <img src="img/logo.png" alt="">
            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive"
                aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
                <span><i class="fas fa-bars"></i></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarResponsive">
                <ul class="navbar-nav">
                    <li class="nav-item dropdown">
                        <a class="nav-link  active" href="{{ Route('User.home') }} ">Home</a>
                    </li>
                    <li class="nav-item dropdown">
                        <a class="nav-link ">Trip Package</a>
                        <div class="dropdown-content">
                            @foreach( $address as $address)
                            <a href="{{ Route('User.tour_packages',$address->id) }}">{{ $address->CatName }}</a>
                            @endforeach
                        </div>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link 123a" href="{{ Route('User.service') }}">Services</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{ Route('User.gallery') }}">Gallery </a>

                    </li>
                    <li class="nav-item dropdown">
                        <a class="nav-link" href="{{ Route('User.blog') }}">Blog</a>


                    </li>
                    <li class="nav-item dropdown">
                        <a class="nav-link" >More</a>

                        <div class="dropdown-content">
                            <a href="{{ Route('User.about') }}">About</a>
                            <a href="{{ Route('User.faq') }}">FAQs</a>
                            <a href="{{ Route('User.contact') }}">Contact</a>
                        </div>
                    </li>
                </ul>
            </div>
            <div class="left-menu-pho">
                <div class="icon-phon-men">
                   <i class="fas fa-phone-volume"></i>
                </div>
                <div class="phone-number-idel">
                    <h6><a href="tel:01994992011">1-1235-536-5896</a></h6>
                    <p>Toll Free & 24/7 Available</p>
                </div>
            </div>
        </div>
    </nav>
</header>
<!-- navber end-->
<div class="modal fade" id="myModal2" role="dialog">
<div class="modal-dialog">
<!-- Modal content-->
<div class="modal-content">
  <div class="modal-body">
    <button type="button" class="close" data-dismiss="modal">&times;</button>
    <div class="model-details">
      <h5>Login Form </h5>
      <div class="mdel-form">
            <form  id="loginForm" >

        <input type="hidden" name="modal" vlue="0">
          <div class="form-group">

            <label for="name">Email</label>
            <input type="text" class="form-control"  placeholder="Email" id="Email" name="email">
            <div id="vadilate_email" class="text-danger">
            </div>
          </div>
          <div class="form-group">
            <label for="password">Password</label>
            <input type="password" class="form-control"  placeholder="Password"
            id="password1" name="password">
            <div id="vadilate_password" class="text-danger"></div>
          </div>
          <div>
           <input type="checkbox" id="remember" name="remember" value="remember">
           <label for="remember"> Remember me</label><br>
         </div>
         <div id ='vadilate1' class="text-danger"></div>
         <div class="sunb-btn-mod">
          <button type="button" url="{{ Route('User.postlogin') }}" name="submit" class="btn btn-3 widet-2 loginUser">Login</button>

        </div><br/>
        <div>
          <a style="padding-right: 5px">Don't have account?</a>
          <a href="#" data-toggle="modal" data-target="#myModal3">Sign up now!</a>
          <a href="#" data-toggle="modal" class="close" data-dismiss="modal" data-target="#modalforgotpass" style="float:right;font-size:15px">Forgot password!</a>
        </div>
      </form>
    </div>
  </div>
</div>
</div>
</div>
</div>
<!--Modal sign up-->
<div class="modal fade" id="myModal3" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <div class="model-details">
                    <h5>Sign up Form</h5>
                    <div class="mdel-form">

                        <form id="signup" >
                            {{ csrf_field() }}
                        <div class="form-group">
                            <label for="name">Name</label>
                            <input type="text" class="form-control" placeholder="Enter your name" name="name" id="contact_name">

                        </div>

                        <div class="form-group">
                            <label for="name">Address</label>
                            <input type="text" class="form-control" placeholder="Enter your address" name="address" id="address_vadilate2">

                        </div>
                        <div class="form-group">
                            <label for="name">Phone Number</label>
                            <input type="phone" class="form-control" placeholder="Enter your phone number" name="phone_number" id="phone_number">

                        </div><div class="form-group">
                            <label for="name">Date Of Birth</label>
                            <input type="date" class="form-control" placeholder="Enter your birthday" name="date_of_birth" id="date_of_birth_vadilate2">
                             <div id="date_of_birth_vadilate" class="text-danger">

                            </div>
                        </div>
                        <div class="form-group">
                            <label for="name">Email</label>
                            <input type="text" class="form-control" placeholder="email" id="email2_vadilate2" name="email2" placeholder="Enter your email address">
                             <div id="email2_vadilate" class="text-danger">

                            </div>
                        </div>
                        <div class="form-group">
                            <label for="password">Password</label>
                            <input type="password" class="form-control" placeholder="Password" id="password2_vadilate2" name="password2" placeholder="Enter your pasword">
                            <div id="password2_vadilate" class="text-danger">

                            </div>
                        </div>
                        <div class="form-group">
                            <label for="name">Confirm Password</label>
                            <input type="text" class="form-control" id="confirm_password_vadilate2" name="confirm_password2" placeholder="Confirm your pasword">
                             <div id="confirm_password_vadilate" class="text-danger">

                            </div>
                        </div>
                        <div class="form-group">
                            <label for="exampleInputFile"> Sex </label>
                            <div class="custom-control custom-radio">
                              <input class="custom-control-input" type="radio" id="customRadio1" name="sex" value="1"checked>
                              <label for="customRadio1" class="custom-control-label">Male</label>
                            </div>
                            <div class="custom-control custom-radio">
                              <input class="custom-control-input" type="radio" id="customRadio2" name="sex"  value="0">
                              <label for="customRadio2" class="custom-control-label">Female</label>
                            </div>
                        </div>
                        <div class="alert alert-danger print-error-msg" style="display:none">
                           <ul></ul>
                         </div>

                        <br/>
                        <div class="sunb-btn-mod" >
                           <button type="button" id="contact_submit" url="" name="submit"  class="btn btn-3 widet-2 signup">Sign up</button>
                        </div>
                        <br/>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="modalforgotpass" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <div class="model-details">
                    <h5>Forgot Password Form</h5>
                    <div class="mdel-form">
                        <p class="login-box-msg" style="text-align:center">You forgot your password? Here you can easily retrieve a new password.</p>
                        <form action="{{ route('User.post_password') }}" method="POST">
                            @csrf
                        <div class="form-group">
                            <label for="name">Email</label>
                            <input type="text" class="form-control" placeholder="Enter your email" name="email" >
                        </div>
                        <br/>
                        <div class="sunb-btn-mod">
                        <button type="submit" href="{{route('User.get_code')}}" class="btn btn-3 widet-2">Request new password</button>
                        </div><br/>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
{{-- content --}}
@yield('content')
{{-- footer --}}
<!-- footer start-->
<footer id="footer-all-page">
  <div class="container">
      <div class="row">
          <div class="col-lg-4 col-md-6 col-sm-6 col-12">
              <div class="all-fott-cov">
                  <div class="footer0logo">
                      <a href="#"><img src="img/logo.png" alt=""></a>
                  </div>
                  <div class="footer-para">
                      <p>Pellentesque convallis, diam et feugiat volutpat, tellus ligula consequat augue, quis
                          malesuada nisi ante nec metus. Sed id pretium nunc. Mauris vitae porttitor tortor. Fusce
                          aliquet ac metus eget egestas. </p>
                  </div>
                  <div class="footer-form">
                      <div class="form-group d-flex">
                      </div>
                  </div>
              </div>
          </div>
          <div class="col-lg-2 col-md-6 col-sm-6 col-6">
              <div class="quick-link">
                  <div class="comm-foot-hed">
                      <h6>Quick Links</h6>
                  </div>
                  <div class="foot-list ">
                      <ul>
                          <li><a href="{{ Route('User.about') }}">About</a></li>
                          {{--  @foreach( $address as $address)
                          <li><a href="{{Route('User.tour_packages',$address->id) }}">Tours & Trips</a></li>
                          @endforeach --}}
                          {{-- <li><a href="tour-details.html">Locations Find</a></li> --}}
                          <li><a href="{{ Route('User.contact') }}">Contact Us.</a></li>
                          <li><a href="{{ Route('User.faq') }}">Terms & conditions</a></li>
                          {{-- <li><a href="faq.html">Praivcy Policy</a></li> --}}
                      </ul>
                  </div>
              </div>
          </div>
          <div class="col-lg-2 col-md-6 col-sm-6 col-6">
              <div class="quick-link-2">
                  <div class="foot-list-2 ">
                      <ul>
                          <li><a href="{{ Route('User.home') }}">Home</a></li>
                          {{-- <li><a href="">Testimonials</a></li> --}}
                          <li><a href="{{ Route('User.gallery') }}">Team</a></li>
                          <li><a href="{{ Route('User.service') }}">Service</a></li>
                          <li><a href="{{ Route('User.blog') }}">News</a></li>
                      </ul>
                  </div>
              </div>
          </div>
          <div class="col-lg-4 col-md-6 col-sm-6 col-12">
              <div class="recent-fot-post mar-top-responsive">
                  <div class="comm-foot-hed">
                      <h6>Recent Post</h6>
                  </div>
                  <div class="post-cover-foot">
                    @foreach($blog as $val)
                    @if($loop->index+1 <= 3)
                      <div class="pos-rece-1">
                          <div class="post-rect-img">
                              <a href="{{ route('User.blog_single',['id'=>$val->id]) }}"><img  src="{{ url('upload')}}/{{ $val->Image }}" alt="" class=""></a>
                          </div>
                          <div class="podt-text-1">
                              <p><a href="{{ route('User.blog_single',['id'=>$val->id]) }}">{{ $val->Content }}</a></p>
                              <span>{{ $val->created_at->format('d-m-Y') }}</span>
                          </div>
                      </div>
                      @endif
                     @endforeach
                  </div>
              </div>
          </div>
      </div>
  </div>
</footer>
 <div class="modal fade" id="modal-guide" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true" >
              <div class="modal-dialog " style=" width: 700px;max-width: 700px;">
                <div class="modal-content"style="width: 700px;">
                  <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle">Thông tin chi tiết<b class="data-id"></b></h5>

                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                  </div>
                  <div class="modal-body p-0 m-0 h-auto" id="content_guide">

                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-primary|secondary|success|danger|warning|info|light|dark" data-dismiss="modal">Close</button>

                  </div>
                </div>
              </div>

<!-- footer bottom-->
<div class="footre-bottom">
  <div class="container">
      <div class="row">
          <div class="col-lg-6 col-md-8 col-sm-8 col-8">
              <div class="copy-right-para">
                  {{-- <p>And IT Themes © 2019. All Rights Reserved</p> --}}
              </div>
          </div>
          <div class="col-lg-6 col-md-4 col-sm-4 col-4">
              <div class="copy-right-icon">
                  <a href="#"><i class="fab fa-facebook-f face no-ag"></i></a>
              </div>
          </div>
      </div>
  </div>
</div>
<!-- footer end-->
</body>



<!-- Main js -->
<script src="js/jquery-3.3.1.min.js"></script>
<script src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.9/jquery.validate.min.js"></script>
<!-- Bootstrap-->
<script src="js/bootstrap.min.js"></script>

<!-- owl.carousel-->
<script src="js/owl.carousel.min.js"></script>

<!-- wow.min-->
<script src="js/wow.min.js"></script>

<!-- html5shiv-->
<script src="js/html5shiv.js"></script>

<!-- respondl-->
<script src="js/respond.min.js"></script>
<!-- toastr -->

<script src="js/toastr.js"></script>

<!-- prettifyl-->
<script src="js/prettify.js"></script>
<script src="rating-star-icons/dist/rating.js"></script>

<script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>

<!--bootstrap-datepicker-->
<script src="bootstrap-datepicker/1.3.0/js/bootstrap-datepicker.js"></script>
<!-- DataTables -->
<script src="plugins/datatables/jquery.dataTables.min.js"></script>
<script src="plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>
<script src="plugins/datatables-responsive/js/dataTables.responsive.min.js"></script>
<script src="plugins/datatables-responsive/js/responsive.bootstrap4.min.js"></script>
@yield('datatable')

<script type="text/javascript">
    toastr.options =
        {
            newestOnTop      : true,
            closeButton      : false,
            progressBar      : true,
            preventDuplicates: false,
            showMethod       : 'slideDown',
            timeOut          : 2000, //default timeout,
        };
    @if(Session::has('message'))
        var type = "{{ Session::get('alert-type', 'info') }}";
        switch(type){
            case 'info':
                toastr.info("{{ Session::get('message') }}");
                break;
            case 'warning':
                toastr.warning("{{ Session::get('message') }}");
                break;
            case 'success':
                toastr.success("{{ Session::get('message') }}");
                break;
            case 'error':
                toastr.error("{{ Session::get('message') }}");
                break;
        }
    @endif
</script>
     <script>
//Get the button
var mybutton = document.getElementById("myBtn");

// When the user scrolls down 20px from the top of the document, show the button
window.onscroll = function() {scrollFunction()};

function scrollFunction() {
  if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
    mybutton.style.display = "block";
  } else {
    mybutton.style.display = "none";
  }
}

// When the user clicks on the button, scroll to the top of the document
function topFunction() {
  document.body.scrollTop = 0;
  document.documentElement.scrollTop = 0;
}
</script>

    <!-- custom-->
    <script src="js/custom.js"></script>
    <script src="js/ion.rangeSlider.js"></script>
    <script src="js/ion.rangeSlider.min.js"></script>
    @yield('script')
    <script src="js/silderprice.js"></script>

    <script>
    function openNav() {
      document.getElementById("mySidenav").style.width = "250px";
    }

    function closeNav() {
      document.getElementById("mySidenav").style.width = "0";
    }
    </script>
<script>
    $(document).ready(function(){
        $('.loginUser').click(function(){
            var email =$("[name='email']").val();
            var password =$('#password1').val();
            var url = $(this).attr('url');
            var _token= $('meta[name="csrf-token"]').attr('content');
            if(email == null || email=="" ){
              document.getElementById("vadilate_email").innerHTML= 'Bạn cần nhập Eamil';
              $('#Email').addClass('red');
            }
            var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
            if(email == regex){
              document.getElementById("vadilate_email").innerHTML= 'Bạn cần nhập đúng định dạng';
              $('#Email').addClass('red');
            }
            if (password == null || password=="") {
                document.getElementById("vadilate_password").innerHTML= 'Bạn cần nhập password';
              $('#password1').addClass('red');
            }

            $.ajax({
                url:url,
                method:"POST",
                data:{email:email,password:password, _token:_token},
                success:function(data){
                    if(data.alert_type == "error"){
                        document.getElementById("vadilate1").innerHTML= data.message;
                        $('#Email').addClass('red');
                        $('#password1').addClass('red');
                    }else if(data.alert_type == "success"){
                        Swal.fire({
                                position: '',
                                icon: 'success',
                                title: 'Đăng nhập thành công',
                                showConfirmButton: false,
                                timer: 3000
                             })
                        setTimeout(function(){
                            window.location.reload();
                        },"2000");
                     }
                    }


            });
        });
    });

$(document).ready(function(){
$('#contact_submit').click(function(e){
e.preventDefault();
var _token = $("input[name='_token']").val();
var name = $("input[name='name']").val();
var address = $("input[name='address']").val();
var phone_number = $("input[name='phone_number']").val();
var date_of_birth = $("input[name='date_of_birth']").val();
var email2 = $("input[name='email2']").val();
var password2 = $("input[name='password2']").val();
var confirm_password2 = $("input[name='confirm_password2']").val();
var sex = $("input[name='sex']").val();
$.ajax({
    url:"{{route('User.postpegister')}}",
    type:"POST",
    data:{_token:_token,name:name,address:address,phone_number:phone_number,date_of_birth:date_of_birth,email2:email2,password2:password2,confirm_password2:confirm_password2,sex:sex},
    success:function(data){
        if($.isEmptyObject(data.error)){
             Swal.fire({
                        position: '',
                        icon: 'success',
                        title: 'Đăng kí thành công',
                        showConfirmButton: false,
                        timer: 3000
                     })
                        setTimeout(function(){
                            window.location.reload();
                        },"2000");
        }else{
            printErrorMsg(data.error);
        }
        }

});

});

function printErrorMsg(msg){
      $(".print-error-msg").find("ul").html('');
            $(".print-error-msg").css('display','block');
            $.each( msg, function( key, value ) {
                $(".print-error-msg").find("ul").append('<li>'+value+'</li>');
            });
}

});


</script>
<script>
     $(function(){
$(".js_model").click(function(event){
  var data_id = $(this).attr("data-id");
  // alert(data);
    let url = $(this).attr('href');
    // alert(url);
    $.ajax({
        url: url,
        type: 'GET',
        data: {id:data_id},
        success: function(data){
            $("#content_guide").html(data);
            $("#modal-guide").modal('show');
        }
    });
});
});
</script>


</body>


</html>
