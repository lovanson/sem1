@extends('User/layout/main')
@section('content')


    <!-- who-are start-->
    <section id="wer-service">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 col-md-12 col-sm-12 col-12">
                    <div class="ser-ab-deta">
                        <div class="service-ads-heading">
                            <h2>Every Time We Provide <span>Best Service</span></h2>
                        </div>
                        <div class="services-pata">
                            <p>Aliquam erat volutpat. Curabitur tempor nibh quis arcu convallis, sed viverra quam
                                sollicitudin. Proin sed augue sed neque ultricies condimentum. In ac ultrices lectus.
                                Nullam ex elit, vestibulum ut urna non, tincidunt condimentum sem. Sed enim tortor,
                                accumsan.Nullam ex elit, vestibulum ut urna non, tincidunt </p>
                            <p>condimentum sem. Sed enim tortor, accumsan. Nullam ex elit, vestibulum ut urna non,
                                tincidunt condimentum sem. Sed enim tortor, accumsan.Nullam ex elit, vestibulum ut urna
                                non, tincidunt condimentum .</p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 col-md-12 col-sm-12 col-12">
                    <div class="row">
                        <div class="col-lg-6 col-md-6 col-sm-12 col-12">
                            <div class="service-deta-ovjet ersd-2 wow fadeIn" data-wow-duration="1.5s">
                                <div class="weject-on">
                                    <img src="img/service/1.png" alt="">
                                    <h6>Advice & Support</h6>
                                    <p>Proin sed auguet sed neque andk ultricies aasr dffgfd condimentum. .</p>
                                </div>
                                <div class="weject-on ers wow fadeIn" data-wow-duration="2s">
                                    <img src="img/service/2.png" alt="">
                                    <h6>Hotel Accomodations</h6>
                                    <p>Proin sed auguet sed neque andk ultricies aasr dffgfd condimentum. .</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-12 col-12">
                            <div class="service-deta-ovjet ers-2 wow fadeIn" data-wow-duration="3s">
                                <div class="weject-on">
                                    <img src="img/service/3.png" alt="">
                                    <h6>Advice & Support</h6>
                                    <p>Proin sed auguet sed neque andk ultricies aasr dffgfd condimentum. .</p>
                                </div>
                                <div class="weject-on ers  wow fadeIn" data-wow-duration="1s">
                                    <img src="img/service/4.png" alt="">
                                    <h6>Hotel Accomodations</h6>
                                    <p>Proin sed auguet sed neque andk ultricies aasr dffgfd condimentum. .</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- who-are end-->

    <!-- testimonial-top start-->
    <section id="home-testimonial-top">
        <div class="content-box">
            <h6 class="color-1">Motion</h6>
            <h2 class="color-2">Watch Our <span> Video Tour</span></h2>
        </div>
    </section>
    <!-- testimonial-top end-->

    <!-- testimonial start-->
    <section id="service-testmoinal">
        <div class="container">
            <div class="heading">
                <h2>ab-section</h2>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="vt-img">
                        <img src="img/v-t.png" alt="">
                    </div>
                    <div class="video-play-test">
                        <a href="https://www.youtube.com/embed/Kb8CW3axqRE" data-width="800" data-height="880"
                            class="video-play-btn video-link"><i class="fas fa-play"></i></a>
                    </div>

                </div>
            </div>
        </div>
    </section>
    <!-- testimonial end-->


    <!-- team start-->
    <section id="home-team" class="oiuytr">
        <div class="content-box">
            <h6>Travel Guides, Tips & Advice</h6>
            <h2>Travel <span>Agents</span></h2>
            <p>Aliquam erat volutpat. Curabitur tempor nibh quis arcu convallis, sed viverra quam sollicitudin. Proin
                sed augue sed neque ultricies condimentum. </p>
        </div>
        <div class="container">
            <div class="row">
                    @foreach($guide as $model)
            <div class="col-lg-3  col-md-6 col-sm-6 col-12">
                <div class="team-opa wow fadeIn" data-wow-duration="1s">
                    <div class="coa-app">
                        <img src="{{ url('upload') }}/{{ $model->avata }}" height="250px" alt="">
                        <div class="team-overlay">
                            <div class="team-contact">
                                {{-- <a href="#!" class="btn btn-4 js_model">HIRE ME</a> --}}
                                 <a data-toggle="modal" href="{{ route('guideview') }}" class="btn btn-4 js_model" data-id="{{ $model->id }}"> chi tiết</a>
                                <h6>{{ $model->Name }}</h6>
                                <p>{!! $model->Contactmethod !!}</p>
                                <div class="team-icon">
                                    <a ><i class="fab fa-facebook-f team-over no-te"></i></a>
                                    <a ><i class="fab fa-twitter team-over"></i></a>
                                    <a><i class="fab fa-linkedin-in team-over"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
           @endforeach

            </div>
            <div class="logo-slilder">
                <div class="all-logo owl-carousel owl-theme">
                    <a href="#!"><img src="img/about/lo-1.png" alt=""></a>
                    <a href="#!"><img src="img/about/lo-2.png" alt=""></a>
                    <a href="#!"><img src="img/about/lo-3.png" alt=""></a>
                    <a href="#!"><img src="img/about/lo-4.png" alt=""></a>
                    <a href="#!"><img src="img/about/lo-5.png" alt=""></a>
                    <a href="#!"><img src="img/about/lo-6.png" alt=""></a>
                </div>
            </div>
        </div>
    </section>
    <!-- team end-->

    <!-- nature-img start-->
    <section id="img-nature">
        <div class="heading">
            <h2>ab-section</h2>
        </div>
        <div class="container-fluid">
            <div class="img-nature-slider owl-carousel owl-theme">
               @foreach($blog as $val)
                <div class="nuture-img-item">
                    <a href="{{ route('User.blog_single',['id'=>$val->id]) }}"><img src="{{ url('upload')}}/{{ $val->Image }}" alt=""></a>
                    <div class="overay-nutre">
                        <a href="{{ route('User.blog_single',['id'=>$val->id]) }}"><i class="fas fa-search-plus"></i></a>
                    </div>
                </div>
                @endforeach

            </div>
        </div>
    </section>
    <!-- nature-img end-->


    <!-- Scroll to top-->
    <div class="to-top pos-rtive">
        <a onclick="topFunction()" id="myBtn" title="Go to top"><i class="fas fa-angle-up"></i></a>
    </div>
    <!-- Scroll to top-->

@endsection
