@extends('User/layout/main')
@section('content')

    <!-- gallery-banner start-->
    <section id="gallery-banner">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="common-banner-text  wow zoomIn" data-wow-duration="2s">
                        <div class="common-heading">
                            <h1>Gallery</h1>
                        </div>
                        <div class="commom-sub-heading">
                            <h6>
                                <a href="{{ Route('User.home') }}">Home</a>
                                <span>/</span>
                                <a >Gallery</a>
                            </h6>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--  gallery-banner end-->

    <!-- gallery-are start-->
    <section id="gallery-area">
        <div class="container">
            <div class="row">
                @foreach($blog as $val)
                <div class="col-lg-4 col-md-4 col-sm-6 col-12">
                    <div class="gallery-main-hover wow fadeIn" data-wow-duration="1s">
                        <a href="{{ route('User.blog_single',['id'=>$val->id]) }}"><img src="{{ url('upload')}}/{{ $val->Image }}" alt=""></a>
                        <div class="gall-overlay">
                            <div class="all-cover-hall">
                                <div class="icon-tsdg">
                                    <a href="{{ route('User.blog_single',['id'=>$val->id]) }}"><i class="fas fa-search-location"></i></a>
                                </div>
                                <div class="gall-heding-travel">
                                    <h6>{{ $val->Content }}</h6>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                @endforeach
                <div class="gallerry-buttopn">
                </div>
            </div>
        </div>
    </section>
    <!-- gallery-are end-->

    <!-- Scroll to top-->
    <div class="to-top pos-rtive">
              <a onclick="topFunction()" id="myBtn" title="Go to top"><i class="fas fa-angle-up"></i></a>
    </div>
    <!-- Scroll to top-->

@endsection
