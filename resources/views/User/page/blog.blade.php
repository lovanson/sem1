@extends('User/layout/main')
@section('content')
        <!-- blog-banner start-->
    <section id="blog-banner">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="common-banner-text wow zoomIn" data-wow-duration="2s">
                        <div class="common-heading">
                            <h1>Blog</h1>
                        </div>
                        <div class="commom-sub-heading">
                            <h6>
                                <a href="index.html">Home</a>
                                <span>/</span>
                                <a href="#!">Blog</a>
                            </h6>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--  blog-banner end-->
    <section id="blog-content">
        <div class="container">
            <div class="row">
                     @foreach($blog as $val)
                <div class="col-lg-4 col-md-6 col-sm-6 col-12 wow fadeIn" data-wow-duration="1s">
                    <div class="blog-items">
                        <div class="blog-item-img h-75 mt-2">
                            <a href="{{ route('User.blog_single',['id'=>$val->id]) }}"><img src="{{ url('upload')}}/{{ $val->Image }}" alt="" class="w-100"></a>
                        </div>
                        <div class="dtes-not">
                            <div class="blog-item-det">
                                <h6><a href="{{ route('User.blog_single',['id'=>$val->id]) }}">{{ $val->Content }}</a></h6>
                            </div>
                            <div class="blog-author">
                                <div class="blog-flex-same">
                                    <div class="blog-athou-img">
                                        <a ><img src="img/blog/ic.png" alt=""></a>
                                        <p>By:<a >{{ $val->Creator }}</a></p>
                                    </div>
                                    <div class="blog-times">
                                        <i class="far fa-clock"></i>
                                        <p><a >{{ $val->created_at->format('d-m-Y') }}</a></p>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
                     @endforeach

            </div>
            <!-- pagination start -->
            <div class="btn btn-sm  float-right mt-2 ">
                       <div class="m-auto"> {{$blog->links()}}</div>
            </div>
            <div class="blog-pagination d-flex justify-content-center text-center">
                <div class="wrapper blog-wrapper">
                    <p id="pagination-here"></p>
                </div>
            </div>
            <!-- pagination end -->
        </div>
    </section>

     <!-- Scroll to top-->
    <div class="to-top pos-rtive">
        <a onclick="topFunction()" id="myBtn" title="Go to top"><i class="fas fa-angle-up"></i></a>
    </div>
    <!-- Scroll to top-->
@endsection
