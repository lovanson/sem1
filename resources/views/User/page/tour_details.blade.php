@extends('User/layout/main')
@section('content')
<!-- tour-banner start-->
<section id="tour-packes-deatils">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-12">
                <div class="common-banner-text  wow zoomIn" data-wow-duration="2s">
                    <div class="common-heading">
                        <h1>Tour Details</h1>
                    </div>
                    <div class="commom-sub-heading">
                        <h6>
                            <a href="index.html">Home</a>
                            <span>/</span>
                            <a href="#!">Tour Details</a>
                        </h6>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- tour-banner end-->

<!-- tour-details info-->
<section id="tour-detailes-main">
    <div class="container">
        <div class="row">
            <div class="col-lg-8 col-md-12 col-sm-12 col-12">
                <div class="all-details-tour">

                    <div class="all-price">
                        <div class="tour-heading-detailse">

                            <h6>{{ $tour->category->CatName}}</h6>

                            <h2>Special <span>{{ $tour->Name }}</span></h2>


                            <div class="start-text-details">
                                <div class="start-icon-deta">
                                    @for($i = 1;$i<=5;$i++)
                                    @if($i <= $ratings)
                                        <a ><i class="fas fa-star"></i></a>
                                    @else
                                    <a ><i class="far fa-star"></i></a>
                                    @endif
                                    @endfor
                                </div>
                                <div class="revews">
                                    <h6>{{ $countReview }} Review</h6>
                                </div>
                            </div>

                        </div>
                        <div class="price-tour">

                        </div>
                    </div>
                    <div class="tour-main-informa">
                        <h6>{{ $tour->Duration }}day <span>|</span></h6>
                        <h6>{{ $tour->MaxGuest }}max guest <span>|</span></h6>
                        <h6>{{ $tour->tag->Name }}</h6>
                    </div>
                    <div id="owl-demo" class="owl-carousel mt-2 w-100">
                       @foreach($tourimage as $val)
                            @php
                                  $listImage = json_decode($val->Images);
                            // $data=
                             @endphp
                            {{-- <img src="{{$key }}" class="w-100"> --}}
                             @foreach($listImage as $key)
                            <div id="demo" class="carousel slide" data-ride="carousel">

                              <!-- Indicators -->
                              <ul class="carousel-indicators">
                                <li data-target="#demo" data-slide-to="0" class="active"></li>
                                <li data-target="#demo" data-slide-to="1"></li>
                                <li data-target="#demo" data-slide-to="2"></li>
                              </ul>

                              <!-- The slideshow -->
                              <div class="carousel-inner">
                                <div class="carousel-item active">
                                  <img src="{{ url('upload') }}/{{$key }}" alt="Los Angeles">
                                </div>
                              </div>

                              <!-- Left and right controls -->
                              <a class="carousel-control-prev" href="#demo" data-slide="prev">
                                <span class="carousel-control-prev-icon"></span>
                              </a>
                              <a class="carousel-control-next" href="#demo" data-slide="next">
                                <span class="carousel-control-next-icon"></span>
                              </a>

                            </div>
                            @endforeach

                        @endforeach
                    </div>
                    <div class="rweal-reat w-100 " >
                        <h5>Tour Overview</h5>
                        <p class="" style=" font-family: Arial, sans-serif;">{!! $tour->Content !!}  </p>
                    </div>

                    <div class="packages-includ">
                        <h5>Package Included </h5>
                        <div class="all-ul-includ">
                            <div class="right-includ row w-100">
                                 @foreach($ServiceDetail as $val)
                                <div class="col-6 col-sm-3 mt-2">
                                    <div class=" "> <i class="far fa-check-circle color-red"></i> {{ $val->service->Name }}</div>
                                </div>
                                    @endforeach
                                <div class="wrapper">
                                </div>

                            </div>

                        </div>
                    </div>
                    <div class="map-inclid">
                        <h5>Tour Location</h5>
                        <iframe
                        src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3677.6962663570607!2d89.56355961427838!3d22.813715829827952!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x39ff901efac79b59%3A0x5be01a1bc0dc7eba!2sAnd+IT!5e0!3m2!1sen!2sbd!4v1557901943656!5m2!1sen!2sbd"
                        width="600" height="450" allowfullscreen=""></iframe>
                    </div>
                    <div class="client-revews" id="client-revews">
                        <h5>Our Clients Review</h5>

                            @foreach($comment as $val)
                        <div class="client-info-rev">
                            <div class="cliennt-img">
                              @if($val->customer->image != Null)
                                        <div class="">
                                            <img src="{{ url('public/upload/customer')}} /{{$val->customer->image }}" alt="">
                                        </div>
                                    @else
                                     <div id="imagePreview" >
                                        <img src="https://i1.wp.com/placehold.it/160x160/38729e/ffffff/&text={{ Str::limit( $val->customer->name, 1, $end='')  }}" alt="">
                                        </div>

                                    @endif
                            </div>
                            <div class="clients-desnigation">
                                <h5>{{ $val->customer->name }}</h5>
                                <p>{{ $val->Content }}</p>
                            </div>
                        </div>
                            @endforeach
                        @if(Auth::check() )
                        {{-- {{ dd($tour ->review_me(Auth::user()->id,$tour->id)) }} --}}
                                @if($tour ->review_me(Auth::user()->id,$tour->id) ==0)

                                 <div class="  mt-5 p-3 text-white" style="border-radius: 20px ;background: #ff9d33">

                                   <p class="text-white pl-3">Bạn chưa có đánh giá</p>
                                </div>
                                @else
                                <div class="  mt-5 p-3 text-white" style="border-radius: 20px ;background: #ff9d33">
                                   <p class="text-white m-1 ">Đánh giá của bạn</p>
                                   <h5 class="text-white pl-2">{{ Auth::user()->name }}</h5>
                                   @for($i = 1;$i<=5;$i++)
                                       @if($i <= $tour ->review_me(Auth::user()->id,$tour->id))

                                           <a ><i class="fas fa-star " style="color: #014073"></i></a>

                                       @else
                                       <a class="text-warning"><i class="far fa-star " style="color: #014073"></i></a>
                                       @endif
                                   @endfor
                                   <p class="text-white pl-3">{{$tour ->content(Auth::user()->id,$tour->id)}}</p>
                                </div>
                                @endif


                        @else
                         <div class="  mt-5 p-3 text-white" >

                         </div>
                        @endif

                    </div>
                    <div class="client-start-comment">
                        <div class="all-women-heading">
                            <h3>Write a Review</h3>
                        </div>
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="dtart-one">
                                    <div class="start-one-ras">
                                        <h6>Services</h6>
                                        <div class="stat-serv">
                                            <div id="Services" data-rating-stars="5" data-rating-input="#dataInput"></div>
                                             <!-- <input type="text" readonly id="dataInput" class="form-control form-control-sm"> -->
                                        </div>
                                    </div>
                                    <div class="start-one-ras">
                                        <h6>Hospitality</h6>
                                        <div class="stat-serv">
                                            <div id="Hospitality" data-rating-stars="5" data-rating-input="#dataInput"></div>
                                        </div>
                                    </div>
                                    <div class="start-one-ras">
                                        <h6>Cleanliness</h6>
                                        <div class="stat-serv">
                                             <div id="Cleanliness" data-rating-stars="5" data-rating-input="#dataInput"></div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="dtart-one">
                                    <div class="start-one-ras">
                                        <h6>Rooms</h6>
                                        <div class="stat-serv">
                                             <div id="Rooms" data-rating-stars="5" data-rating-input="#dataInput"></div>
                                        </div>
                                    </div>
                                    <div class="start-one-ras">
                                        <h6>Comfort</h6>
                                        <div class="stat-serv">
                                            <div id="Comfort" data-rating-stars="5" data-rating-input="#dataInput"></div>
                                        </div>
                                    </div>
                                    <div class="start-one-ras">
                                        <h6>Satisfaction</h6>
                                        <div class="stat-serv">
                                           <div id="Satisfaction" data-rating-stars="5" data-rating-input="#dataInput"></div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                        <div class="revs-form">
                             @if(Auth::check())
                              <meta name="csrf-token" content="{{ csrf_token() }}">
                            <form id="price">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="form-group">
                                        <!-- ------------------------- lấy ra số sao đánh giá của người dùng ---------------- -->
                                           <!--  Lấy id của tour ra  -->
                                             <input type="hidden" name="TourID" value="{{ $tour->id }}">

                                            <input type="hidden" readonly name="NumberRating" id="totalrating" class="form-control form-control-sm">
                                        </div>

                                    </div>

                                    <div class="col-lg-12">
                                        <div class="form-group nessage-text">
                                            <textarea name="Content"  rows="6" class="form-control"
                                            placeholder="Enter Your Message:" ></textarea>
                                        </div>

                                            <input type="hidden" name="UserID" value="{{ $customers->id }}">
                                        <div class="contact-sub-btn">
                                             <button type="button" url="{{ route('User.rating_post') }}" name="submit"class="btn submit postrating" id="showrating">submit</button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                            @else


                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="form-group">
                                        <!-- ------------------------- lấy ra số sao đánh giá của người dùng ---------------- -->
                                           <!--  Lấy id của tour ra  -->
                                             <input type="hidden" name="TourID" value="{{ $tour->id }}">

                                            <input type="hidden" readonly name="NumberRating" id="totalrating" class="form-control form-control-sm">
                                        </div>

                                    </div>

                                    <div class="col-lg-12">
                                        <div class="form-group nessage-text">
                                            <textarea name="Content"  rows="6" class="form-control"
                                            placeholder="Enter Your Message:" ></textarea>
                                        </div>

                                            {{-- <input type="hidden" name="UserID" value="{{ $customers->id }}"> --}}
                                        <div class="contact-sub-btn">
                                            <button class="btn submit postrating" data-toggle="modal" data-target="#myModal2 ">submit</button>
                                        </div>
                                    </div>
                                </div>

                              @endif
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-12 col-sm-12 col-12">
                <div class="bookoing-secrty">
                    <div class="all-pacj-dfgh">
                        <h6>Package Details</h6>
                    </div>
                    <div class="bookk0-natd-form">
                        <form action="{{ route('User.tour_details_post') }}" id="price" method="POST" enctype="multipart/form-data">
                            @csrf
                            <div class="form-group">
                                <label for="name">Chọn ngày bắt đầu  </label>
                                 @php
                                $currendate=  date('Y-m-d', strtotime(' + 2 days'));
                                @endphp

                               {{--  @php
                                $currendate=date('Y-m-d');
                                $currendate1=date('Y-m-d',,strtotime($currendate.**' +2 days'**));

                                dd($currendate1);
                                @endphp --}}
                                <input type="date"  name="Start" value class="form-control dateinput" min="{{$currendate}}" max="" id="dateinput">
                                 @if($errors->has('Start'))
                                <div class="" style="color: #014073">
                                    <span >{{$errors->first('Start')}}</span>
                                </div>
                                @endif
                                {{-- số ngày đi --}}
                                {{-- <input type="hidden" name="day" id="day" value="{{ $tour->Duration }}"> --}}
                                <input type="hidden" name="Duration" value="{{ $tour->Duration }}">
                            </div>


                                    <!-- lấy id của tour gửi lên database -->
                            <input type="hidden" name="TourID" value="{{ $tour->id }}">
                            <!-- lấy tên của tour gửi lên database <--></-->
                            <input type="hidden" name="Destination" value="{{ $tour->Name }}">
                           {{--  giá của người lớn --}}
                                    <!-- lấy ra số trẻ em từ 1 -5 tuổi  -->
                            <input type="hidden" class="children" name="guestchild" >

                                    <!-- lấy ra số trẻ em từ 6 -10 tuổi  -->
                            <input type="hidden" class="children2" name="guestchildren" value="0" >
                            <input type="hidden" class="form-control alult_total" name="guestadult" id="alult_total" value="0">

                            <div id="oldChild" class="oldChild">
                                <div class="row">
                                    <div class="col-md-4 col-4">
                                         <label for="name" class="text-center">Độ tuổi </label>
                                       <div class="border-danger p-2 ">
                                           <button type="button" class="border btn btn-primary w-100 text-center text-white"style="background: #014073">1-5
                                           </button>


                                       </div>
                                       <div class="border-danger p-2 ">
                                           <button type="button" class="border btn btn-primary w-100  text-center text-white"style="background: #014073">6-10
                                           </button>

                                       </div>
                                       <div class="border-danger p-2 ">
                                           <button type="button" class="border btn btn-primary w-100 text-center text-white"style="background: #014073">11-16
                                           </button>

                                       </div>
                                       <span class="text-detail">
                                   <label for="name">Người lớn  </label>
                                </span>
                                    </div>
                                    <div class="col-md-3 col-3">
                                            <label for="name" class="text-center w-100"> Giá </label>
                                        <div class="m-top-price">
                                             <label for="name"class="number-price price1 w-100">x{{ $tour->Pricechild }}$</label>
                                             <label for="name"class="number-price w-100">x{{ $tour->Pricechildren }}$</label>
                                             <label for="name"class="number-price w-100">x{{ $tour->Priceadult }}$</label>
                                               <label for="name"class="number-price price w-100">x{{ $tour->Priceadult }}$</label>
                                        </div>
                                    </div>
                                    <div class="col-md-5 col-5 justify-content-center">
                                        <label for="name" class="text-center ">Số người </label>
                                        <div class="m-top">
                                            <div class="d-flex m-2 justify-content-center ">
                                               <h5>
                                                    <div class="children d-flex m-2 ">
                                                       0
                                                   </div>
                                               </h5>
                                                 <div class="btn-group">
                                                      <button type="button" class="btn btn-primary minus-childrens "style="background: #014073;">
                                                        <i class="fas fa-minus"></i>
                                                      </button>
                                                      <button type="button" class="btn btn-primary plus-childrens "style="background: #014073">
                                                        <i class="fas fa-plus"></i>
                                                      </button>
                                                </div>
                                            </div>
                                            <div class="d-flex m-2 justify-content-center">
                                               <h5>
                                                    <div class="children2 d-flex m-2 justify-content-center" id="children2">
                                                       0
                                                   </div>
                                               </h5>
                                                 <div class="btn-group">
                                                      <button type="button" class="btn btn-primary minus-childrens2 "style="background: #014073;">
                                                        <i class="fas fa-minus"></i>
                                                      </button>
                                                      <button type="button" class="btn btn-primary plus-childrens2 "style="background: #014073">
                                                        <i class="fas fa-plus"></i>
                                                      </button>
                                                </div>
                                            </div>
                                            <div class="d-flex m-2 justify-content-center">
                                               <h5>
                                                    <div class="children3 d-flex m-2 justify-content-center">
                                                       0
                                                   </div>
                                               </h5>
                                                 <div class="btn-group">
                                                      <button type="button" class="btn btn-primary minus-childrens3 "style="background: #014073;">
                                                        <i class="fas fa-minus"></i>
                                                      </button>
                                                      <button type="button" class="btn btn-primary plus-childrens3 "style="background: #014073">
                                                        <i class="fas fa-plus"></i>
                                                      </button>
                                                </div>

                                            </div>
                                             <div class="d-flex m-2 justify-content-center">
                                               <h5>
                                                     <span class="number-detail number-adult m-2" >5</span>
                                               </h5>
                                                 <div class="btn-group">
                                                  <button type="button" class="btn btn-primary minus-adult " style="background: #014073;"><i class="fas fa-minus"></i></button>
                                                  <button type="button" class="btn btn-primary plus-adult "style="background: #014073;"><i class="fas fa-plus"></i></button>
                                                </div>


                                            </div>
                                        </div>

                                     </div>
                                </div>
                            </div>
                             <div class="form-group">
                                <label for="name">Chọn Phương thức thanh toán  </label>
                                <div class="">
                                    <select class="form-control " name="PaymentID" >
                                      <option  class="bg-dark" value="" >Chon phương thức</option>
                                        @foreach($paymentMethods as $model)
                                        <option class="bg-dark" value="{{ $model->id }}">{{ $model->Payment_type }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                @if($errors->has('PaymentID'))
                                <div class=""  style="color: #014073">
                                    <span >{{$errors->first('PaymentID')}}</span>
                                </div>
                                @endif
                            </div>
                            <div class="travel-tyepe">
                                <div class="flex-type">
                                    <label for="text">Total Price:</label>

                                </div>
                                <div class="check-box-many">
                                    <h3 class="d-flex">
                                        <div class="total-price ">
                                           {{ $tour->Priceadult }}

                                        </div>
                                         <div class="text-white">
                                            $
                                         </div>
                                    </h3>
                                </div>
                            </div>
                             @if(Auth::check())
                            <input type="hidden" name="UserID" value="{{ $customers->id }}">
                             <input type="hidden" class="form-control" name="Totalamount" id="total">
                             <input type="hidden" class="form-control" name="TotalPrice" id="total">
                            <div class="sunb-btn-naple">
                                <button type="submit" class="btn submit widet vadilate" id="showtoast" >Đặt tour</button>
                                {{-- <button type="button" class="btn submit widet" id="showtoast">Đặt tour</button> --}}
                            </div>
                            @else
                               <div class="sunb-btn-naple">
                                <a  class="btn submit widet vadilate" data-toggle="modal" data-target="#myModal2" >Đặt tour</a>
                                {{-- <button type="button" class="btn submit widet" id="showtoast">Đặt tour</button> --}}
                            </div>
                            @endif
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- tour-details end-->
<!-- Scroll to top-->
<div class="to-top pos-rtive">
    <a onclick="topFunction()" id="myBtn" title="Go to top"><i class="fas fa-angle-up"></i></a>
</div>
<!-- Scroll to top-->
{{--
<script>
    let today = new Date().toISOString().substr(0, 10);
    document.querySelector("#today").value() = today;

    document.querySelector("#today2").valueAsDate() = new Date();
</script> --}}

@endsection

@section('script')
<script type="text/javascript">

</script>
<script>
$(document).ready(function(){
    $('.postrating').click(function(){
        var TourID =$("[name='TourID']").val();
        var NumberRating =$("[name='NumberRating']").val();
        var Content =$("[name='Content']").val();
        var UserID =$("[name='UserID']").val();
        var url = $(this).attr('url');
        var _token= $('meta[name="csrf-token"]').attr('content');
        $.ajax({
            url:url,
            method:"POST",
            data:{TourID:TourID,NumberRating:NumberRating,Content:Content,UserID:UserID, _token:_token},
            success:function(data){
                if(data.alert_type == "success"){
                        Swal.fire({
                                position: '',
                                icon: 'success',
                                title: 'Cảm ơn bạn đã đánh giá',
                                showConfirmButton: false,
                                timer: 2000
                             });
                    $('#client-revews').load( window.location.href + ' #client-revews ');
                     }else if (data.alert_type == "error") {
                           Swal.fire({
                                position: '',
                                icon: 'error',
                                title: 'Sorry',
                                showConfirmButton: false,
                                timer: 2000
                             });

                     }
                }


        });
    });
});
    $(document).ready(function() {
      $("#owl-demo").owlCarousel({
        autoPlay : 3000,
        stopOnHover : true,
        navigation:true,
        paginationSpeed : 1000,
        goToFirstSpeed : 2000,
        singleItem : true,
        autoHeight : true,
        transitionStyle:"fade"
      });
      //stop sumit
     // $("#showtoast").click(function (e){
     //    e.preventDefault();

     //  });
});

$(document).ready(function(){
    /*..................lấy ra giá trị số sao đánh giá của người dùng .....................*/
        var star1 = 0;
        var star2 = 0;
        var star3 = 0;
        var star4 = 0;
        var star5 = 0;
        var star6 = 0;


         $("#Services").rating({
        "click": function (e) {
           star1 = e.stars;
           averageRating();
        }
    });
        $("#Hospitality").rating({
        "click": function (e) {
            star2 = e.stars;
            averageRating();
        }

    });
        $("#Cleanliness").rating({
        "click": function (e) {
            star3 = e.stars;
            averageRating();
        }

    }); $("#Rooms").rating({
        "click": function (e) {
            star4 = e.stars;
            averageRating();
        }

    });$("#Comfort").rating({
        "click": function (e) {
            star5 = e.stars;
            averageRating();
        }

    });$("#Satisfaction").rating({
        "click": function (e) {
            star6 = e.stars;
            averageRating();
        }
    });
    function averageRating(){
        var average = (star1+star2+star3+star4+star5+star6)/6;
        console.log(average);
     var totalrating = Math.round(average);
        $("#totalrating").val(totalrating);

    }

    // ---------------------end--------------------

    // });
    var numberChild = 0;
    $('.number-children').html(numberChild);

    // số trẻ em

    $('.plus-children').on('click',function(e){
        numberChild+=1;
        $('.number-children').html(numberChild);

        $('#oldChild').html($('#oldChild').html() + chooseAge);
        // var c = $("#value_children").val();
        // for (var i = 0; i <= numberChild ; i++) {

        // }
    });
    var total=1;


    //tăng số trẻ em
    var numberChilds2 = 0;
    var numberChilds3 = 0;
    var numberadult = 1;
    var alult_total = 1;
    var n0 = sessionStorage.getItem('numberChilds');
    var numberChilds = n0 ? parseInt(n0) : 0;
    var n1 = sessionStorage.getItem('numberChilds2');
    var numberChilds2 = n1 ? parseInt(n1) : 0;
    var n2 = sessionStorage.getItem('numberChilds3');
    var numberChilds3 = n2 ? parseInt(n2) : 0;
    var n3 = sessionStorage.getItem('numberadult');
    var numberadult = n3 ? parseInt(n3) : 1;



    function getTotal(){
         total= numberadult*{{ $tour->Priceadult  }} + (numberChilds*{{$tour->Priceadult  }})/5 + (numberChilds2*{{$tour->Pricechildren }}) + (numberChilds3*{{ $tour->Priceadult  }}) ;
        $('.total-price').html(total);
        $('input#total').val(total);
        return total;
    }

       /* total = getTotal();
        $("#price").submit(function( event ) {
            var url = "{{ route('ajax-post') }}";
            $.ajax({

               type:'POST',

               url: url,

               data:{
                "_token": "{{ csrf_token() }}",
                "total": total,
               },

               success:function(data){

                  // alert(data);

               }

            });
        });*/
   // tang so tre em 1
      $('.children').html(numberChilds);
      var numberChilds = sessionStorage.getItem('numberChilds') ? sessionStorage.getItem('numberChilds') : 0;
     $('.children').val(numberChilds);
    $('.plus-childrens').on('click',function(e){
        numberChilds+=1;
        $('.children').html(numberChilds);
        sessionStorage.setItem('numberChilds',numberChilds);
         $('.children').val(numberChilds);
        getTotal();
          sessionStorage.clear();
    });
    $('.minus-childrens').on('click',function(e){
        if(numberChilds >= 1){
            numberChilds-=1;
            $('.children').html(numberChilds);
            sessionStorage.setItem('numberChilds',numberChilds);
            $('.children').val(numberChilds);
             getTotal();
               sessionStorage.clear();
        }
    });

     //tăng số trẻ em2
    $(".children2").html(numberChilds2);
     var numberChilds2 = sessionStorage.getItem('numberChilds2') ? sessionStorage.getItem('numberChilds2') : 0;
         $('.children2').val(numberChilds2);
        $('.plus-childrens2').on('click',function(e){
            numberChilds2+=1;
            $('.children2').html(numberChilds2);
             sessionStorage.setItem('numberChilds2',numberChilds2);
            $('.children2').val(numberChilds2);
             getTotal();
               sessionStorage.clear();
        });

    //giảm số trẻ em2
    $('.minus-childrens2').on('click',function(e){
        if(numberChilds2 >= 1){
            numberChilds2-=1;
             sessionStorage.setItem('numberChilds2',numberChilds2);
            $(".children2").html(numberChilds2);
            $(".children2").val(numberChilds2);
             getTotal();
               sessionStorage.clear();
        }
    });

 //tăng số trẻ em3
    $('.children3').html(numberChilds3);
    $('.plus-childrens3').on('click',function(e){

        numberChilds3+=1;
        $('.children3').html(numberChilds3);
         sessionStorage.setItem('numberChilds3',numberChilds3);
         getTotal();
         totaladult();
           sessionStorage.clear();
    });
    //giảm số trẻ em3
    $('.minus-childrens3').on('click',function(e){
        if(numberChilds3 >= 1){
            numberChilds3-=1;
            sessionStorage.setItem('numberChilds3',numberChilds3);
            $('.children3').html(numberChilds3);
             getTotal();
        totaladult();
          sessionStorage.clear();

        }
    });

     // giam so nguoi lớn đi

    $('.number-adult').html(numberadult);
    $('.minus-adult').on('click',function(e){
        if(numberadult > 1){
            numberadult-=1;
             sessionStorage.setItem('numberadult',numberadult);
            $('.number-adult').html(numberadult);

            getTotal();
        totaladult();
          sessionStorage.clear();

        }
    });
    // tăng số người lớn

    $('.plus-adult').on('click',function(e){
        numberadult+=1;
        $('.number-adult').html(numberadult);
        sessionStorage.setItem('numberadult',numberadult);

         getTotal();
    totaladult();
      sessionStorage.clear();

    });
    // tổng số người từ 11 tuổi trở lên

        function totaladult(){
            alult_total = numberChilds3 + numberadult;
             $('input.alult_total').val(alult_total);
              $('.alult_total').html(alult_total);
                // sessionStorage.clear();
                return alult_total;

        }

     totaladult();
     getTotal();

    });


    @if(Session::has('no'))
    <script>
        Swal.fire({
  position: '',
  icon: 'error',
  title: 'Bạn cần đăng nhập',
  showConfirmButton: false,
  timer: 3000
})
 </script>
    @endif






</script>

@endsection



