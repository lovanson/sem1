<p>Chào {{$name}}!</p>
<p>Bạn đã đặt tour thành công!</p>
<table style="border:1px ">
    <thead>
        <th>Tên Tour</th>
        <th>Ngày khởi hành</th>
        <th>Ngày kết thúc</th>
        <th>Trẻ em</th>
        <th>Trẻ vị thành viên</th>
        <th>Người lớn</th>
        <th>Tổng giá</th>
    </thead>
    <tbody>
        <td>{{$tour_name}}</td>
        <td>{{$start}}</td>
        <td>{{$end}}</td>
        <td>{{$guestchild}}</td>
        <td>{{$guestchildren}}</td>
        <td>{{$guestadult}}</td>
        <td>{{$total}}</td>
    </tbody>
</table>
<h3>Chúc bạn có một chuyến du lịch vui vẻ và an toàn!</h3>
<p>Chân thành cảm ơn!</p>