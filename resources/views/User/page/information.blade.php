@extends('User/layout/main')
@section('content')
<section id="information">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-12">
                <div class="table-responsive  ">
                    <div class=" text-center mb-3">
                        <h3 class="text-white">List Tour</h3>
                    </div>

                    <table class="table table-responsive  " style="width: 80%; margin: 0 auto" id="example2">
                        <thead class="">
                        <tr>
                            <th scope="col">Tour Name<p>*--*</p></th>
                            <th scope="col">Payment Methods<p>*--*</p></th>
                            <th scope="col">Start Date<p>*--*</p></th>
                            <th scope="col">End Date<p>*--*</p></th>
                            <th scope="col">Number of Children<p>*1-5 years old*</p></th>
                            <th scope="col">Number of Children<p>*5-10 years old*</p></th>
                            <th scope="col">Adults<p>*--*</p></th>
                            <th scope="col">Total<p>*--*</p></th>
                        </tr>
                        </thead>
                        <tbody class="w-100">
                             @foreach($showorder as $model)
                        <tr>
                            @foreach($model->orderDetail as $val)
                            @if($val->tours!=null)
                            <td><a title="" class="text-white " style="text-transform: capitalize;"> {{ $val->tours->Name }}</a>
                               </td>
                            <td >{{ $model->payment->Payment_type }}</td>
                            @endif
                            @endforeach
                            <td>{{ $val->Start }}</td>
                            <td>{{ $val->End }}</td>
                            <td>{{ $val->guestchild }}</td>
                            <td>{{ $val->guestchildren }}</td>
                            <td>{{ $val->guestadult }}</td>
                            <td>{{ $model->Totalamount }}</td>
                           {{--  <td><a href="{{ route('User.tour_details', ['id'=>$val->TourID]) }} " class="text-white" title="">Book</a></td> --}}
                        </tr>
                        @endforeach
                         </tbody>
                    </table>
                    <div class="item-hover" style="width:90%">
                        {{ $showorder->links() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
@section('datatable')
<<script>
$('#example2').DataTable({
      "paging": false,
      "lengthChange": false,
      "searching": true,
      "ordering": false,
      "info": false,
      "autoWidth": false,
      "responsive": false,
    });
</script>
@endsection
