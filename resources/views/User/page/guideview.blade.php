
 <div class=" col-sm-12 col-md-12 d-flex align-items-stretch p-0">
              <div class="card bg-light">
                <div class="card-header text-muted border-bottom-0">
                <h2>{{ $guide->Name }}</h2>
                </div>
                <div class="card-body pt-3 ">
                  <div class="row">
                    <div class="col-7 mt-4" style=" font-size: 20px;padding-left: 50px;">
                      <ul class="ml-4 mb-0 fa-ul text-muted">
                        <li class="m-2 w-100 text-white ">
                            <p class="text-white "style=" font-size: 20px;" class=""><b class="text-white">About: </b > {!! $guide->Contactmethod !!}</p>
                        </li>
                        <li class="m-2 w-100 text-white ">
                            <span class="">
                            <i class="fas fa-lg fa-phone"></i>
                            </span> Phone #: {{ $guide->Phone }}
                        </li>
                        <li class="w-100 m-2 text-white ">
                            <a href="https://www.facebook.com/profile.php?id=100012363010039" class=" d-flex">
                            <i class="fab fa-facebook-f face no-ag">Fakebook</i><span class="text-white">:</span>
                            <label class="m-auto text-white">{{ $guide->Name }}
                            </label>
                            </a>
                         </li><li class="w-100 m-2 text-white">
                            <a href="https://www.facebook.com/profile.php?id=100012363010039" class=" d-flex">
                            <i class="fab fa-twitter face">Twitter</i><span class="text-white">:</span>
                            <label class="m-auto text-white">{{ $guide->Name }}
                            </label>
                            </a>

                      </ul>
                    </div>

                       {{--  <li class="small "><span class="fa-li d-flex"><i class="fab fa-twitter face">Twitter</i><label class="m-2 text-white">{{ $guide->Name }}</label></span> </li>

                        <li class="small "><span class="fa-li d-flex"><i class="fab fa-linkedin-in face">Linkedin</i><label class="m-2 text-white">{{ $guide->Name }}</label></span> </li> --}}
                    <div class="col-5 text-center">
                      <img src="{{ url('upload') }}/{{ $guide->avata }}" alt="" style=" border-radius: 50%;max-width: 100%;  height: auto;">
                    </div>
                  </div>
                </div>

              </div>
            </div>
