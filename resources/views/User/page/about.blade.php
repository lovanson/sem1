@extends('User/layout/main')
@section('content')
    <!-- about-banner start-->
    <section id="about-banner ">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-12">
                    <div class="common-banner-text wow zoomIn" data-wow-duration="2s">
                        <div class="common-heading">
                            <h1>About</h1>
                        </div>
                        <div class="commom-sub-heading">
                            <h6>
                                <a href="index.html">Home</a>
                                <span>/</span>
                                <a href="#!">About</a>
                            </h6>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--  about-banner end-->

    <!-- who-are start-->
    <section id="who-area">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-12 col-12">
                    <div class="who-img wow zoomIn" data-wow-duration="2s">
                        <img src="img/about/pl.png" alt="">
                    </div>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-12 col-12">
                    <div class="left-side-text-ab mar-o res-raju">
                        <h6>We have 25+ years experience </h6>
                        <h2 class="left-op">We try to the serve best service</h2>
                        <p>Aliquam erat volutpat. Curabitur tempor nibh quis arcu convallis, sed viverra quam
                            sollicitudin. Proin sed augue sed neque ultricies condimentum. In ac ultrices lectus. </p>
                        <a href="#!" class="btn btn-2 mar-top">Read More</a>
                    </div>
                </div>
            </div>
            <div class="sec-who">
                <div class="row">
                    <div class="col-lg-4 col-md-12">
                        <div class="who-headinf-inner">
                            <h2>Who we are?</h2>
                            <p>Aliquam erat volutpat. Curabitur tempor nibh quis arcu convallis, sed viverra quam
                                sollicitudin. Proin sed augue sed neque ultricies condimentum. In ac ultrices</p>
                            <p>Nullam ex elit, vestibulum ut urna non, tincidunt condimentum sem. Sed enim tortor,
                                accumsan.</p>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6">
                        <div class="who-box">
                            <h6>Our Vission </h6>
                            <p>Proin sed augue sed neque andsa ultricies condimentum. In ac ana ultrices lectus. Proin
                                sed augue sed neque ultricies condimentum. In ac ultrices Proin sed augue sed neque
                                andsa ultricies. </p>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6">
                        <div class="who-box">
                            <h6>Our Mission </h6>
                            <p>Proin sed augue sed neque andsa ultricies condimentum. In ac ana ultrices lectus. Proin
                                sed augue sed neque ultricies condimentum. In ac ultrices Proin sed augue sed neque
                                andsa ultricies. </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- who-are end-->

    <!-- best-service start-->
    <section id="best-service">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="best-service-heading">
                        <h2>All time we provide best service</h2>
                        <p>Aliquam erat volutpat. Curabitur tempor nibh quis arcu convallis, sed viverra quam
                            sollicitudin.</p>
                    </div>
                </div>
            </div>
            <div class="ter-icon-flash">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="all-item-ter">
                            <div class="ter-item-one">
                                <div class="gift-img wow zoomIn" data-wow-duration="1.5s">
                                    <img src="img/about/gifft.png" alt="">
                                </div>
                                <div class="des-ter">
                                    <h6>Exclusive package</h6>
                                    <p>Aliquam erat volutpat. Curabitur nibh quis arcu convallis, </p>
                                </div>
                            </div>
                            <div class="all-ineer-der-ter">
                                <img src="img/about/ter.png" alt="" class="shape-2">
                            </div>
                            <div class="ter-item-one">
                                <div class="gift-img wow zoomIn" data-wow-duration="2.5s">
                                    <img src="img/about/plan.png" alt="">
                                </div>
                                <div class="des-ter">
                                    <h6>Exclusive package</h6>
                                    <p>Aliquam erat volutpat. Curabitur nibh quis arcu convallis, </p>
                                </div>
                            </div>
                            <div class="all-ineer-der-ter">
                                <img src="img/about/ter.png" alt="" class="shape-2">
                            </div>
                            <div class="ter-item-one">
                                <div class="gift-img wow zoomIn" data-wow-duration="3.5s">
                                    <img src="img/about/tak.png" alt="">
                                </div>
                                <div class="des-ter">
                                    <h6>Exclusive package</h6>
                                    <p>Aliquam erat volutpat. Curabitur nibh quis arcu convallis, </p>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- best-service end-->



    <!-- team start-->
    <section id="home-team">
        <div class="content-box">
            <h6>Travel Guides, Tips & Advice</h6>
            <h2>Travel <span>Agents</span></h2>
            <p>Aliquam erat volutpat. Curabitur tempor nibh quis arcu convallis, sed viverra quam sollicitudin. Proin
                sed augue sed neque ultricies condimentum. </p>
        </div>
        <div class="container">
            <div class="row">
                @foreach($guide as $model)
            <div class="col-lg-3  col-md-6 col-sm-6 col-12">
                <div class="team-opa wow fadeIn" data-wow-duration="1s">
                    <div class="coa-app">
                        <img src="{{ url('upload') }}/{{ $model->avata }}" height="250px" alt="">
                        <div class="team-overlay">
                            <div class="team-contact">
                                {{-- <a href="#!" class="btn btn-4 js_model">HIRE ME</a> --}}
                                 <a data-toggle="modal" href="{{ route('guideview') }}" class="btn btn-4 js_model" data-id="{{ $model->id }}"> chi tiết</a>
                                <h6>{{ $model->Name }}</h6>
                                <p>{{ $model->Contactmethod }}</p>
                                <div class="team-icon">
                                    <a href="#!"><i class="fab fa-facebook-f team-over no-te"></i></a>
                                    <a href="#!"><i class="fab fa-twitter team-over"></i></a>
                                    <a href="#!"><i class="fab fa-linkedin-in team-over"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
           @endforeach

            </div>
            <div class="logo-slilder">
                <div class="all-logo owl-carousel owl-theme">
                    <a href="#!"><img src="img/about/lo-1.png" alt=""></a>
                    <a href="#!"><img src="img/about/lo-2.png" alt=""></a>
                    <a href="#!"><img src="img/about/lo-3.png" alt=""></a>
                    <a href="#!"><img src="img/about/lo-4.png" alt=""></a>
                    <a href="#!"><img src="img/about/lo-5.png" alt=""></a>
                    <a href="#!"><img src="img/about/lo-6.png" alt=""></a>
                </div>
            </div>
        </div>
    </section>
    <!-- team end-->
    <!-- nature-img start-->
    <section id="img-nature">
        <div class="heading">
            <h2>ab-section</h2>
        </div>
        <div class="container-fluid">
            <div class="img-nature-slider owl-carousel owl-theme">
                @foreach($blog as $val)
                <div class="nuture-img-item">
                    <a href="{{ route('User.blog_single',['id'=>$val->id]) }}"><img src="{{ url('upload')}}/{{ $val->Image }}" alt=""></a>
                    <div class="overay-nutre">
                        <a href="{{ route('User.blog_single',['id'=>$val->id]) }}"><i class="fas fa-search-plus"></i></a>
                    </div>
                </div>
                @endforeach
            </div>
        </div>
    </section>
    <!-- nature-img end-->

    <!-- Scroll to top-->
    <div class="to-top pos-rtive">
        <a onclick="topFunction()" id="myBtn" title="Go to top"><i class="fas fa-angle-up"></i></a>
    </div>
    <!-- Scroll to top-->
@endsection
