@extends('User/layout/main')
@section('content')

    <!-- tour-banner start-->
    <section id="tour-packes">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="common-banner-text  wow zoomIn" data-wow-duration="2s">
                        <div class="common-heading">
                            <h1>Tour Packges</h1>
                        </div>
                        <div class="commom-sub-heading">
                            <h6>
                                <a href="index.html">Home</a>
                                <span>/</span>
                                <a href="#!">Tour Packges</a>
                            </h6>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--  tour-banner end-->


    <!-- Ab start-->
    <section id="ab-home" class="travel-pac">
        <div class="heading">
            <h2>ab-section</h2>
        </div>
        <div class="container">
             <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-12 main-com">
                <nav>
                    <div class="nav nav-tabs nav-fill" id="nav-tab" role="tablist">
                        <a class="nav-item nav-link active" id="nav-home-tab" data-toggle="tab" href="#nav-home"
                            role="tab" aria-controls="nav-home" aria-selected="true">Search Tour</a>

                    </div>
                </nav>
                <form  action="{{ route('User.search_home') }}" method="GET" enctype="multipart/form-data">
                <div class="tab-content py-3 px-3 px-sm-0" id="nav-tabContent">
                    <div class="tab-pane fade show active" id="nav-home" role="tabpanel"
                        aria-labelledby="nav-home-tab">
                        <div class="row">
                            <div class="col-lg-3 col-md-6 col-sm-6 col-12">
                                <div class="booking-info">
                                    <div class="select-box">
                                        <span class="sec-po"><i class="fas fa-map-marker-alt"></i></span>
                                        <select name="category">

                                             @foreach($category as $model)
                                            <option value="{{ $model->id }}" @if(isset($req)) {{ $req->category == $model->id ? "selected" : "" }} @endif > {{ $model->CatName }}
                                            </option>
                                        @endforeach
                                        </select>
                                        <i class="fas fa-angle-down map-io"></i>
                                    </div>

                                </div>
                            </div>
                            <div class="col-lg-3 col-md-6 col-sm-6 col-12">
                                <div class="booking-info">
                                    <div class="select-box">
                                        <span class="sec-po"><i class="fas fa-mountain"></i></span>

                                        <select name="tag">

                                           @foreach($tag as $model)
                                            <option value="{{ $model->id }}" @if(isset($req)) {{ ($req->tag == $model->id) ? "selected" : "" }}@endif> {{ $model->Name }} </option>
                                        @endforeach
                                        </select>
                                        <i class="fas fa-angle-down map-io"></i>
                                    </div>

                                </div>
                            </div>
                            <div class="col-lg-6 col-md-12 col-sm-12 col-12 mt-2">
                                  <div class="row">
                                    <div class="col-sm-12">
                                      <div id="slider-range"></div>
                                    </div>
                                  </div>
                                  <div class="row slider-labels">
                                    <div class="col-6 col-xs-6 col-lg-6 caption">
                                      <strong>Min:</strong> <span id="slider-range-value1">@if(isset($req)) {{ $priceMin }} @endif</span>
                                    </div>
                                    <div class="col-6 col-xs-6 col-lg-6 text-right caption float-right">
                                      <strong>Max:</strong> <span id="slider-range-value2">@if(isset($req)) {{ $priceMax }} @endif</span>
                                    </div>
                                  </div>
                                  <div class="row">
                                    <div class="col-sm-12">
                                        {{-- <input type="hidden" name=""> --}}
                                        <input type="hidden" name="min_value" value="" id="minPrice">
                                        <input type="hidden" name="max_value" value="" id="maxPrice">
                                    </div>
                                  </div>
                            </div>


                            <div class="col-lg-12">
                                <div class="book-ctn">
                                    <button type="submit" class="btn btn-2 pad">Search Now</button>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </form>
                <div class="valedoter">
                    <h2>Home</h2>
                </div>
            </div>

             <div class="col-lg-6 col-md-12">
                <div class="left-side-text-ab max-live">
                    <h6>Amazing Places To Enjony Your Travel </h6>
                    <h2>About Our <span>A2Z</span> Travel </h2>
                    <p>Aliquam erat volutpat. Curabitur tempor nibh quis arcu convallis, sed viverra quam
                        sollicitudin. Proin sed augue sed neque ultricies condimentum. In ac ultrices lectus. </p>
                    <p>Nullam ex elit, vestibulum ut urna non, tincidunt condimentum sem. Sed enim tortor, accumsan
                        at consequat et, tempus sit ame</p>
                    <div class="left-blog-tree mt-lg-5 mt-md-4 mt-sm-2">
                    <div class="alo-search">
                        <form class="form-inline ml-3" action="http://localhost:88/travel-master-c/search_blogs" method="GET">
                        <div class="form-group d-flex w-100">
                            <input type="text" placeholder="Search Blog" name="search" class="form-control w-100">
                            <button type="submit" class="btn search-icon-blog"><i class="fas fa-search"></i></button>
                        </div>
                        </form>
                    </div>
                </div>
                </div>
            </div>
            <div class="col-lg-6 col-md-12">
                <div class="ab-slider">
                    <div class="slider-main-ab owl-carousel owl-theme">
                       @foreach($blog as $model)
                        <a href="{{ route('User.blog_single',['id'=>$model->id]) }}"><img src="{{ url('upload')}}/{{ $model->Image }}" alt=""></a>
                        @endforeach
                    </div>

                </div>
            </div>
        </div>
        </div>
    </section>
    <!-- Ab end-->

    <!-- tour start-->
    <section id="tour-des">
        <div class="content-box can-if">
            <h6>Travel Express Provide</h6>
            <h2>Select your <span>Destination</span></h2>
            <p>Aliquam erat volutpat. Curabitur tempor nibh quis arcu convallis, sed viverra quam sollicitudin. Proin
                sed augue sed neque ultricies condimentum. </p>
        </div>
        <div class="container">
            <div class="row">
                @foreach($tour as $model)
                <div class="col-lg-4 col-md-6 col-sm-6 col-12">
                    <div class="des-cov-1">

                        <div class="des-img-1" >
                            <a href="{{ route('User.tour_details',$model->id) }}"><img src="{{ url('upload')}}/{{ $model->Image }}" alt="" ></a>
                        </div>

                        <div class="des-para">
                            <div class="dayt">

                                <h6><a style="text-transform: capitalize" href="{{ route('User.tour_details',$model->id) }}">{{ $model->Name }}</a></h6>
                                <p><a href="{{ route('User.tour_details',$model->id) }}">{{ $model->Duration }}day{{ $model->Priceadult }}$</a></p>
                            </div>
                            <div class="real-dat-para">
                                <p class="" >{!! $model->Title !!}</p>
                            </div>
                            <div class="des-button-icon">
                                <div class="das-into-btn">
                                    <a href="{{ route('User.tour_details',$model->id) }}" class="btn btn-3" >Rewiew</a>
                                </div>
                                <div class="start-icon-des">
                                    @for($i = 1;$i<=5;$i++)
                                    @if($i <= $model ->avg_rating($model->id))

                                        <a ><i class="fas fa-star " style="color: #014073"></i></a>

                                    @else
                                    <a class="text-warning"><i class="far fa-star " style="color: #014073"></i></a>
                                    @endif
                                @endfor
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                @endforeach

                <div class="float-right mt-2 w-100">
                    {{ $tour->appends(request()->only('category','max_value','min_value','tag'))->links() }}
                </div>
                    </div>

                <div class="modal fade" id="myModal" role="dialog">
                    <div class="modal-dialog">
                        <!-- Modal content-->
                        <div class="modal-content">
                            <div class="modal-body">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                <div class="model-details">
                                    <h5>Travel Booking Form</h5>
                                    <div class="mdel-form">
                                        <form action="#">
                                            <div class="form-group">
                                                <label for="name">First name</label>
                                                <input type="text" class="form-control" placeholder="First Name"
                                                    id="name">
                                            </div>
                                            <div class="form-group">
                                                <label for="last-name">Last Name</label>
                                                <input type="text" class="form-control" placeholder="Last Name"
                                                    id="last-name">
                                            </div>
                                            <div class="form-group">
                                                <label for="duse">Departure Date</label>
                                                <input type="text" name="date" class="form-control" id="duse">
                                            </div>
                                            <div class="form-group">
                                                <label for="deate-area-2">Return Date</label>
                                                <input type="text" name="date" class="form-control xDateContainer"
                                                    id="deate-area-2">
                                            </div>
                                            <div class="form-group mainm-sel">
                                                <label for="text" id="form-control">Guest:</label>
                                                <div class="select-box">
                                                    <span class="sec-po"></span>
                                                    <select id="text">
                                                        <option value="0">Number of Guest</option>
                                                        <option value="1">2</option>
                                                        <option value="2">3</option>
                                                        <option value="3">4</option>
                                                    </select>
                                                    <div class="serv-ivmf-2">
                                                        <i class="fas fa-angle-down"></i>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="travel-tyepe">
                                                <div class="flex-type">
                                                    <label for="text">Travel Type</label>
                                                </div>
                                                <div class="check-box-many">
                                                    <div class="custom-control custom-checkbox">
                                                        <input type="checkbox" class="custom-control-input"
                                                            id="defaultChecked1">
                                                        <label class="custom-control-label"
                                                            for="defaultChecked1">Economy</label>
                                                    </div>
                                                    <div class="custom-control custom-checkbox">
                                                        <input type="checkbox" class="custom-control-input"
                                                            id="defaultChecked2">
                                                        <label class="custom-control-label"
                                                            for="defaultChecked2">Premium</label>
                                                    </div>
                                                    <div class="custom-control custom-checkbox">
                                                        <input type="checkbox" class="custom-control-input"
                                                            id="defaultChecked3">
                                                        <label class="custom-control-label"
                                                            for="defaultChecked3">Business</label>
                                                    </div>
                                                    <div class="custom-control custom-checkbox">
                                                        <input type="checkbox" class="custom-control-input"
                                                            id="defaultChecked4">
                                                        <label class="custom-control-label" for="defaultChecked4">High
                                                            Class</label>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="sunb-btn-mod">
                                                <a href="#!" class="btn btn-3 widet-2">BOOKING NOW</a>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- tour start-->



    <!-- summery start-->
       @foreach($tour as $firstItem)
    @if($loop->index+1 <= 1)
     <section id="summery" style="background-image: url({{ url('upload') }}/{{ $firstItem->baner }})">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 offset-lg-2">
                    <div class="all-space-to  wow zoomIn" data-wow-duration="1.5s">
                        <div class="summery-cover">
                            <h6>{{ $firstItem->Title }}</h6>
                            <h2>{{ $firstItem->Name }} </h2>
                        </div>
                        <div class="all-spance">
                            <span>{{ $firstItem->Duration }} day/{{ $firstItem->Duration }}nights</span>
                        </div>
                        <div class="all-span-btn-com">
                            <a href="{{ route('User.tour_details',$firstItem->id) }}" class="btn btn-2 mar-top">View Details</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    @endif
    @endforeach
    <!-- summery end-->

    <!-- team start-->
    <section id="home-team">
        <div class="content-box">
            <h6>Travel Guides, Tips & Advice</h6>
            <h2>Travel <span>Agents</span></h2>
            <p>Aliquam erat volutpat. Curabitur tempor nibh quis arcu convallis, sed viverra quam sollicitudin. Proin
                sed augue sed neque ultricies condimentum. </p>
        </div>
        <div class="container">
            <div class="row">
               @foreach($guide as $model)
            <div class="col-lg-3  col-md-6 col-sm-6 col-12">
                <div class="team-opa wow fadeIn" data-wow-duration="1s">
                    <div class="coa-app">
                        <img src="{{ url('upload') }}/{{ $model->avata }}" height="250px" alt="">
                        <div class="team-overlay">
                            <div class="team-contact">
                                {{-- <a href="#!" class="btn btn-4 js_model">HIRE ME</a> --}}
                                 <a data-toggle="modal" href="{{ route('guideview') }}" class="btn btn-4 js_model" data-id="{{ $model->id }}"> chi tiết</a>
                                <h6>{{ $model->Name }}</h6>
                                <p>{!! $model->Contactmethod !!}</p>
                                <div class="team-icon">
                                    <a ><i class="fab fa-facebook-f team-over no-te"></i></a>
                                    <a ><i class="fab fa-twitter team-over"></i></a>
                                    <a><i class="fab fa-linkedin-in team-over"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
           @endforeach
            </div>
        </div>
    </section>
    <!-- team end-->

    <!-- testimonial-top start-->
    <section id="home-testimonial-top">
        <div class="content-box">
            <h6 class="color-1">Motion</h6>
            <h2 class="color-2">Watch Our <span> Video Tour</span></h2>
        </div>
    </section>
    <!-- testimonial-top end-->

    <!-- testimonial start-->
    <section id="home-testimonial">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="vt-img">
                        <img src="img/v-t.png" alt="">
                    </div>
                    <div class="video-play-test">
                        <a href="https://www.youtube.com/embed/Kb8CW3axqRE" data-width="800" data-height="880"
                            class="video-play-btn video-link"><i class="fas fa-play"></i></a>
                    </div>

                </div>
            </div>
            <div class="test-slider-home-1 owl-carousel owl-theme">
                   @foreach($rating as $model)
            @if($model->NumberRating = 5)
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-12">
                    <div class="row">
                        <div class="col-lg-7 col-md-7 col-sm-7">
                            <div class="test-monial-item ">
                                <div class="test-heading">
                                    <h6>Lots of Smiles </h6>
                                    <h2>More Than 960+ People <span>Are Happy With Us.</span></h2>
                                </div>
                                <div class="test-flex">
                                    <p>{{ $model->Content }}</p>
                                    <h6>{{ $model->customer->name }}</h6>
                                    <div class="start-icon">
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-5 col-sm-5">
                            <div class="qyaty">
                                <i class="flaticon-quotation"></i>
                            </div>
                            <div class="test-moinal-ing-left">
                                <img src="{{ url('public/upload/customer')}} /{{$model->customer->image }}" alt="">
                            </div>

                        </div>
                    </div>
                </div>
            </div>
            @endif
            @endforeach
            </div>

        </div>
    </section>
    <!-- testimonial end-->

    <!-- contact start-->
    <section id="home-contact">
        <div class="map-inner">
            <iframe
                src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3677.6962663570607!2d89.56355961427838!3d22.813715829827952!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x39ff901efac79b59%3A0x5be01a1bc0dc7eba!2sAnd+IT!5e0!3m2!1sen!2sbd!4v1557901943656!5m2!1sen!2sbd"
                width="600" height="450" allowfullscreen=""></iframe>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-md-12 col-sm-12 col-12">
                    <div class="contact-cover">
                        <div class="contact-heading">
                            <h2>Do You Have Any Questions?</h2>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut vehicula volutpat porta. Cras
                                in vulputate est</p>
                        </div>
                        <div class="info-office">
                            <div class="phone-deta">
                                <div class="phone-info">
                                    <i class="flaticon-telephone"></i>
                                </div>
                                <div class="sams">
                                    <p>+124 (2486) 444</p>
                                    <p>+133 (4444) 878</p>
                                </div>
                            </div>
                            <div class="email-deta">
                                <div class="phone-info">
                                    <i class="flaticon-paper-plane"></i>
                                </div>
                                <div class="sams">
                                    <p>mail@example.com</p>
                                    <p>info@mail.com</p>
                                </div>
                            </div>
                        </div>
                        <div class="contact-form">
                            <form role="form" method="POST" action="{{route('User.contact_post')}}" enctype="multipart/form-data">
                                @csrf
                                <div class="row">
                                    @if(Auth::check())
                                 <div class="col-lg-12 col-md-12 col-sm-12 col-12">
                                         <input type="hidden" name="email" value="{{ $customers->email }}">
                                        <div class="form-group nessage-text">
                                            <textarea name="message" id="message" rows="6" class="form-control"
                                                placeholder="Enter Your Message:" required></textarea>
                                        </div>
                                        <div class="contact-sub-btn">
                                            <button type="submit" class="btn submit contactsuccess">gửi</button>
                                        </div>
                                        <div class="sending-gif" style="display: none">
                                            <img src="img/loading.gif" alt="send-gif">
                                        </div>
                                    </div>
                                    @else
                                     <div class="col-lg-12 col-md-12 col-sm-12 col-12">

                                        <div class="form-group nessage-text">
                                            <textarea name="message" id="message" rows="6" class="form-control"
                                                placeholder="Enter Your Message:" required></textarea>
                                        </div>
                                        <div class="contact-sub-btn">
                                           <a  data-toggle="modal" data-target="#myModal2" class="btn submit contactsuccess">Submit</a>
                                        </div>
                                        <div class="sending-gif" style="display: none">
                                            <img src="img/loading.gif" alt="send-gif">
                                        </div>
                                    </div>
                                    @endif
                                </div>
                            </form>
                            <div class="success-msg alert alert-success" style="display: none">
                                <strong>Success!</strong> Email Send Succesfully.
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- contact end-->

   <!-- Scroll to top-->
    <div class="to-top pos-rtive">
        <a onclick="topFunction()" id="myBtn" title="Go to top"><i class="fas fa-angle-up"></i></a>
    </div>
    <!-- Scroll to top-->

@endsection
@section('script')
    @if(Session::has('ok'))
    <script>
        Swal.fire({
  position: '',
  icon: 'error',
  title: 'Không tìm thấy',
  showConfirmButton: false,
  timer: 3000
})



    </script>
    @endif
    <script>
        $(document).ready(function() {
  $('.noUi-handle').on('click', function() {
    $(this).width(50);
  });
  var rangeSlider = document.getElementById('slider-range');
  var moneyFormat = wNumb({
    decimals: 0,
    thousand: ',',
    prefix: '$'
  });
  noUiSlider.create(rangeSlider, {
    start: [ @if(isset($req)) {{  $priceMin }} @else 100 @endif ,  @if(isset($req)) {{ $priceMax }} @else 300 @endif],
    step: 1,
    range: {
      'min': [10],
      'max': [1000]
    },
    format: moneyFormat,
    connect: true
  });

  // Set visual min and max values and also update value hidden form inputs
  rangeSlider.noUiSlider.on('update', function(values, handle) {
    document.getElementById('slider-range-value1').innerHTML = values[0];
    document.getElementById('slider-range-value2').innerHTML = values[1];
    document.getElementsByName('min-value').value = moneyFormat.from(
      values[0]);
    $('#minPrice').val(values[0]);
    $('#maxPrice').val(values[1]);
    document.getElementsByName('max-value').value = moneyFormat.from(
      values[1]);

  });
});
    </script>
@endsection
