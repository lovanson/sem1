@extends('User/layout/main')
@section('content')

<section class="flex-grow-1">
    <!--code here-->

<section id="blog-banner">
    <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-12">
                    <div class="common-banner-text wow zoomIn text-center">
                        <div class="common-heading">
                            <h1>Blog Single</h1>
                        </div>
                        <div class="commom-sub-heading">
                            <h6>
                                <a  class="text-warning">Home</a>
                                <span class="text-white">/</span>
                                <a  class="text-white">Blog Single</a>
                            </h6>
                        </div>
                    </div>
                </div>
            </div>
        </div>
</section>
<section id="blog-single-main">
    <div class="container">
        <div class="row">
            <div class="col-lg-9 col-md-12 col-sm-12 col-12">
                <div class="all-single-cover">
                    <div class="blog-single-img">
                        <div id="owl-demo" class="owl-carousel mt-2 w-100">

                            <img src="{{ url('upload')}}/{{ $blog->baner_top }}" alt="" class="w-100">

                    </div>
                    </div>
                </div>
                <div class="authour-single d-flex ">
                    <div class="al-img-at1 d-flex align-items-center">
                        <a >
                            <img src="img/blog-single/man.png" alt="" >
                        </a>
                        <p>By <a > {{ $blog->Creator }} </a></p>
                    </div>
                    <div class="al-img-at2 d-flex align-items-center ">
                        <a >
                            <i class="far fa-calendar-alt mr-2"></i>
                        </a>
                        <p><a >{{ $blog->created_at->format('d-m-Y') }}</a></p>
                    </div>
                    <div class="al-img-at2 d-flex align-items-center ">
                        <a >
                            <i class="far fa-comments"></i>
                        </a>
                        <p><a href="">Commnents</a></p>
                    </div>
                </div>
                <div class="blog-single-dd-hed">
                    {!! $blog->Contents !!}
                </div>


                <div class="box-tes-bl d-flex mt-4">
                    <div class="box-tes-img">

                        <img src="assets/img/blog-single/new-icon.png" alt="">
                    </div>
                    <blockquote>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusamus sunt optio eveniet distinctio doloremque ea laborum expedita dolorem hic ipsum.</blockquote>
                </div>
                <div class="inner-tahes">
                    <ul>
                        <li><a ><i class="fas fa-tag"></i></a></li>
                        <li><a >Travel,</a></li>
                        <li><a >Tour,</a></li>
                        <li><a > Summer,</a></li>
                        <li><a >Hotels,</a></li>
                        <li><a >Flights</a></li>

                    </ul>
                    <ul>
                        <li><a ><i class="fas fa-share-alt"></i></a></li>
                        <li><a > Share this article</a></li>
                    </ul>
                </div>
                <div class="fb-comments" data-href="https://developers.facebook.com/docs/plugins/comments/photo/?fbid=901669456875381&amp;set=a.141678226207845" data-numposts="5" data-width="100%">
                </div>
            </div>
            <div class="col-lg-3 col-md-12 col-sm-12 col-12">
                <div class="left-blog-tree mt-lg-5 mt-md-4 mt-sm-2">
                    <div class="alo-search">
                        <form class="form-inline ml-3" action="{{ route('User.search_blogs') }}" method="GET">
                        <div class="form-group d-flex">
                            <input type="text" placeholder="Search" name="search" class="form-control">
                            <button  type="submit" class="btn search-icon-blog"><i class="fas fa-search"></i></button>
                        </div>
                        </form>
                    </div>
                </div>
                <div class="coomm-seclitor">
                    <div class="blog-hki-hed">
                        <h5>Categories</h5>
                    </div>
                    <div class="all-boytr">
                        @foreach($tag as $val)
                        <div class="item-cata">
                            <div class="icon-catr">
                                <a ><i class="far fa-dot-circle"></i>{{ $val->Name }}</a>
                            </div>
                            <div class="co-num">
                                <a >{{ $val->Blog->count() }}</a>
                            </div>
                        </div>
                        @endforeach
                    </div>

                </div>
                <div class="coomm-seclitor">
                    <div class="blog-hki-hed">
                        <h5>Recent Posts</h5>
                    </div>
                    <div class="blog-cljg">
                        @foreach($blogmini as $val)
                         @if($loop->index+1 <= 3)
                        <div class="blog-cliccs">
                            <div class="blog-clss-img  wow zoomIn" data-wow-duration="1s" style="visibility: visible; animation-duration: 1s; animation-name: zoomIn;">
                                <a href="{{ route('User.blog_single',['id'=>$val->id]) }}"><img src="{{ url('upload')}}/{{ $val->Image }}" alt=""></a>
                            </div>
                            <div class="alo-blog-clss-text">
                                <h6><a href="{{ route('User.blog_single',['id'=>$val->id]) }}">{{ $val->Content }}</a></h6>
                                <p class="datre">{{ $val->created_at->format('d-m-Y') }}</p>
                            </div>
                        </div>
                        @endif
                        @endforeach

                    </div>
                </div>
                <div class="coomm-seclitor">
                    <div class="blog-hki-hed">
                        <h5>Recent Posts</h5>
                    </div>
                    <div class="catago-item row">
                        @foreach($tag as $val)
                        <div class="col-6 col-md-6 col-lg-6 d-flex">
                        <a href="{{ route('User.blog',['id'=>$val->id]) }}"class=" w-100" >{{ $val->Name }}</a>
                        </div>
                        @endforeach
                    </div>

                </div>
                <div class="coomm-seclitor">
                    <div class="blog-hki-hed">
                        <h5>Instagram</h5>
                    </div>
                    <div class="inteagram-img">
                        @foreach($blogmini as $val)
                        <a href="{{ route('User.blog_single',['id'=>$val->id]) }}"><img src="{{ url('upload')}}/{{ $val->Image }}" alt="img" class="wow zoomIn" data-wow-duration="1s" style="visibility: visible; animation-duration: 1s; animation-name: zoomIn;"></a>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
</section>
 <!-- Scroll to top-->
    <div class="to-top pos-rtive">
        <a onclick="topFunction()" id="myBtn" title="Go to top"><i class="fas fa-angle-up"></i></a>
    </div>
    <!-- Scroll to top-->
    <div id="fb-root"></div>
    <script async defer crossorigin="anonymous" src="https://connect.facebook.net/vi_VN/sdk.js#xfbml=1&version=v7.0" nonce="Sk95R4yp"></script>
@endsection
@section('script')
        @if(Session::has('ok'))
    <script>
        Swal.fire({
  position: '',
  icon: 'error',
  title: 'Xin lỗi không tìm thấy',
  showConfirmButton: false,
  timer: 3000
})
    </script>
    @endif
@endsection
