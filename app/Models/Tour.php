<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Category;
use App\Models\TourImage;
use App\Models\Rating;

class Tour extends Model
{
    protected $primaryKey = 'id';
    protected $fillable = [
        'CatID','Name','Duration','Pricechild','Priceadult','Pricechildren','Content','Status','Piority','TagID','Title','Image','MaxGuest','baner',
    ];
    public function  category(){
        return $this->belongsTo('App\Models\Category','CatID','id');
    } public function  tag(){
        return $this->belongsTo('App\Models\Tag','TagID','id');
    }
    public function  orderDetail(){
        return $this->hasMany('App\Models\OrderDetail','TourID','id');
    }

    public function tourimage(){
        return $this->hasMany('App\Models\TourImage','tour_id','id');
    }
    public function rating(){
        return $this->hasMany('App\Models\Rating','TourID','id');
    }
    public function review_me($id,$tour_id)
    {
      if( $rating=Rating::where([
            'UserID'=>$id,
            'TourID'=>$tour_id
        ])->first()){

       return $rating->NumberRating;

      }else {
          return 0;
      }


    }
    public function content($id,$tour_id)
    {
      if( $rating=Rating::where([
            'UserID'=>$id,
            'TourID'=>$tour_id
        ])->first()){

       return $rating->Content;

      }else {
          return 'Bạn chưa có đánh Giá';
      }


    }
    public function avg_rating($id)
    {
        $rating = Rating::where('TourID',$id)->avg('NumberRating');
        $ratings = round($rating);
        return $ratings;
    }
    public function add(){
        // ];
        // dd($arr);

        // $file = request()->upload;
        // $file_name = $file->getClientOriginalName();
        // $file->move(public_path('upload/tour'),$file_name);

        $Image = str_replace(url('upload').'/', '', request()->Image);
        $baner = str_replace(url('upload').'/', '', request()->baner);
        $model = Tour::create([
            'CatID'=>request()->CatID,
            'Name'=>request()->Name,
            'Pricechild'=>request()->Pricechild,
            'Priceadult'=>request()->Priceadult,
            'Pricechildren'=>request()->Pricechildren,
            'Piority'=>request()->Piority,
            'Duration'=>request()->Duration,
            'Content'=>request()->Content,
            'MaxGuest'=>request()->MaxGuest,
            'Title'=>request()->Title,
            'TagID'=>request()->TagID,
            'Status'=>request()->Status,
            'Image'=>$Image,
            'baner'=>$baner,
        ]);
        $ids=$model->id;

        $Images = str_replace(url('upload').'/', '', request()->Images);
        $models = TourImage::create([
            'Name'=>request()->Name,
            'Images'=>$Images,
            'tour_id'=>$ids

        ]);


          return $model;

      }
      public function edit($id){
        if (request()->Image != null || request()->baner_top != null || request()->Images != null) {
            if (request()->Image != null) {
                if (request()->baner_top != null) {
                    if (request()->Images != null) {
                         $Image = str_replace(url('upload').'/', '', request()->Image);
                        $baner = str_replace(url('upload').'/', '', request()->baner);
                        $model = $this->where('id',$id)->update([
                            'CatID'=>request()->CatID,
                            'Name'=>request()->Name,
                            'Pricechild'=>request()->Pricechild,
                            'Priceadult'=>request()->Priceadult,
                            'Pricechildren'=>request()->Pricechildren,
                            'Piority'=>request()->Piority,
                            'Duration'=>request()->Duration,
                            'Content'=>request()->Content,
                            'MaxGuest'=>request()->MaxGuest,
                            'Title'=>request()->Title,
                            'TagID'=>request()->TagID,
                            'Status'=>request()->Status,
                            'Image'=>$Image,
                            'baner'=>$baner,
                        ]);
                        $Images = str_replace(url('upload').'/', '', request()->Images);
                        $models = TourImage::where('id',$id)->update([
                            // 'Name'=>request()->Name,
                            'Images'=>$Images,
                            'tour_id'=>$id
                        ]);
                          return $model;
                    }else {
                         $Image = str_replace(url('upload').'/', '', request()->Image);
                        $baner = str_replace(url('upload').'/', '', request()->baner);
                        $model = $this->where('id',$id)->update([
                            'CatID'=>request()->CatID,
                            'Name'=>request()->Name,
                            'Pricechild'=>request()->Pricechild,
                            'Priceadult'=>request()->Priceadult,
                            'Pricechildren'=>request()->Pricechildren,
                            'Piority'=>request()->Piority,
                            'Duration'=>request()->Duration,
                            'Content'=>request()->Content,
                            'MaxGuest'=>request()->MaxGuest,
                            'Title'=>request()->Title,
                            'TagID'=>request()->TagID,
                            'Status'=>request()->Status,
                            'Image'=>$Image,
                            'baner'=>$baner,
                        ]);
                        return $model;
                    }

                }elseif (request()->Images != null) {
                     $Image = str_replace(url('upload').'/', '', request()->Image);
                        $baner = str_replace(url('upload').'/', '', request()->baner);
                        $model = $this->where('id',$id)->update([
                            'CatID'=>request()->CatID,
                            'Name'=>request()->Name,
                            'Pricechild'=>request()->Pricechild,
                            'Priceadult'=>request()->Priceadult,
                            'Pricechildren'=>request()->Pricechildren,
                            'Piority'=>request()->Piority,
                            'Duration'=>request()->Duration,
                            'Content'=>request()->Content,
                            'MaxGuest'=>request()->MaxGuest,
                            'Title'=>request()->Title,
                            'TagID'=>request()->TagID,
                            'Status'=>request()->Status,
                            'Image'=>$Image,
                            // 'baner'=>$baner,
                        ]);
                        $Images = str_replace(url('upload').'/', '', request()->Images);
                        $models = TourImage::where('id',$id)->update([
                            // 'Name'=>request()->Name,
                            'Images'=>$Images,
                            'tour_id'=>$id
                        ]);
                          return $model;
                }else {
                     $Image = str_replace(url('upload').'/', '', request()->Image);
                        $baner = str_replace(url('upload').'/', '', request()->baner);
                        $model = $this->where('id',$id)->update([
                            'CatID'=>request()->CatID,
                            'Name'=>request()->Name,
                            'Pricechild'=>request()->Pricechild,
                            'Priceadult'=>request()->Priceadult,
                            'Pricechildren'=>request()->Pricechildren,
                            'Piority'=>request()->Piority,
                            'Duration'=>request()->Duration,
                            'Content'=>request()->Content,
                            'MaxGuest'=>request()->MaxGuest,
                            'Title'=>request()->Title,
                            'TagID'=>request()->TagID,
                            'Status'=>request()->Status,
                            'Image'=>$Image,
                            // 'baner'=>$baner,
                        ]);
                        return $model;
                }

            }elseif ( request()->baner_top != null ) {
                if (request()->Images != null) {
                     $Image = str_replace(url('upload').'/', '', request()->Image);
                        $baner = str_replace(url('upload').'/', '', request()->baner);
                        $model = $this->where('id',$id)->update([
                            'CatID'=>request()->CatID,
                            'Name'=>request()->Name,
                            'Pricechild'=>request()->Pricechild,
                            'Priceadult'=>request()->Priceadult,
                            'Pricechildren'=>request()->Pricechildren,
                            'Piority'=>request()->Piority,
                            'Duration'=>request()->Duration,
                            'Content'=>request()->Content,
                            'MaxGuest'=>request()->MaxGuest,
                            'Title'=>request()->Title,
                            'TagID'=>request()->TagID,
                            'Status'=>request()->Status,
                            // 'Image'=>$Image,
                            'baner'=>$baner,
                        ]);
                        $Images = str_replace(url('upload').'/', '', request()->Images);
                        $models = TourImage::where('id',$id)->update([
                            // 'Name'=>request()->Name,
                            'Images'=>$Images,
                            'tour_id'=>$id
                        ]);
                          return $model;
                }else {
                    $Image = str_replace(url('upload').'/', '', request()->Image);
                        $baner = str_replace(url('upload').'/', '', request()->baner);
                        $model = $this->where('id',$id)->update([
                            'CatID'=>request()->CatID,
                            'Name'=>request()->Name,
                            'Pricechild'=>request()->Pricechild,
                            'Priceadult'=>request()->Priceadult,
                            'Pricechildren'=>request()->Pricechildren,
                            'Piority'=>request()->Piority,
                            'Duration'=>request()->Duration,
                            'Content'=>request()->Content,
                            'MaxGuest'=>request()->MaxGuest,
                            'Title'=>request()->Title,
                            'TagID'=>request()->TagID,
                            'Status'=>request()->Status,
                            // 'Image'=>$Image,
                            'baner'=>$baner,
                        ]);
                        return $model;
                }
            }
        }else {
            $Image = str_replace(url('upload').'/', '', request()->Image);
                        $baner = str_replace(url('upload').'/', '', request()->baner);
                        $model = $this->where('id',$id)->update([
                            'CatID'=>request()->CatID,
                            'Name'=>request()->Name,
                            'Pricechild'=>request()->Pricechild,
                            'Priceadult'=>request()->Priceadult,
                            'Pricechildren'=>request()->Pricechildren,
                            'Piority'=>request()->Piority,
                            'Duration'=>request()->Duration,
                            'Content'=>request()->Content,
                            'MaxGuest'=>request()->MaxGuest,
                            'Title'=>request()->Title,
                            'TagID'=>request()->TagID,
                            'Status'=>request()->Status,
                            // 'Image'=>$Image,
                            // 'baner'=>$baner,
                        ]);
                        return $model;
        }



    }



    public function categorys(){
        return $this->hasMany(Tour::class,'CatID','id');
    }
}
