<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OrderDetail extends Model
{
    protected $primaryKey = 'id';
    protected $fillable = [
        'TourID','OrderID',
        'Destination','Start',
        'End','guestchild','TotalPrice',
        'guestchildren','guestadult',
    ];

    public function  tour(){
        return $this->belongsTo('App\Models\Tour','id');
    }public function  order(){
        return $this->hasOne('App\Models\Order','id','OrderID');
    }
    public function  tours(){
        return $this->hasOne('App\Models\Tour','id','TourID');
    }


    public function add($id_order){
// dd($id_order);
        // $day_t = OrderDetail::where('')
        // dd(request()->all());
        $temp = request()->Start;
        $time = strtotime($temp);
        $day = request()->Duration;
        // dd($day);
        $models = OrderDetail::create([
            'TourID'=>request()->TourID,
            'OrderID'=>$id_order,
            'Destination'=>request()->Destination,
            'Start'=>$temp,
            'End'=>date("Y-m-d",strtotime($temp . '+'.$day.' days')),
            'guestchild'=>request()->guestchild,
            'TotalPrice'=>request()->TotalPrice,
            'guestchildren'=>request()->guestchildren,
            'guestadult'=>request()->guestadult,
        ]);
        return $models;
        // dd($model->End);

    }

    function name($id_tour, $id_order)
    {
         $models = OrderDetail::create([
            'TourID'=>$id_tour,
            'OrderID'=>$id_order,
            'Destination'=>request()->Destination,
            'Start'=>$temp,
            'End'=>date("Y-m-d",strtotime($temp . '+'.$day.' days')),
            'guestchild'=>request()->guestchild,
            'TotalPrice'=>request()->TotalPrice,
            'guestchildren'=>request()->guestchildren,
            'guestadult'=>request()->guestadult,
        ]);
        return $models;
    }
}

