<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class gallery extends Model
{
    protected $primaryKey = 'id';
    protected $fillable = ['title','img','images'];
    public function add(){
        // $file = request()->upload;
        // $file_name = $file->getClientOriginalName();
        // $file -> move(public_path('upload/gallery'),$file_name);
        // $request->merge(['img'=>$file_name]);
          $img = str_replace('http://localhost:88/travel-master-c/upload/', '', request()->img);
        $images = str_replace('http://localhost:88/travel-master-c/upload/', '', request()->images);
        $model = $this->create([
            'title'=>request()->title,
            'img'=>$img,
            'images'=>$images,
        ]);
        return $model;
    }
    public function edit($id){
        if (request()->Image != null || request()->baner_top != null) {
           if (request()->Image != null) {
                if (request()->baner_top != null) {
          $img = str_replace('http://localhost:88/travel-master/upload-c/', '', request()->img);
        $images = str_replace('http://localhost:88/travel-master/upload-c/', '', request()->images);
        $model =$this->where('id',$id)->update([
            'title'=>request()->title,
            'img'=>$img,
            'images'=>$images,
        ]);
          return $model;
      }else{
        $img = str_replace('http://localhost:88/travel-master/upload/', '', request()->img);
        $images = str_replace('http://localhost:88/travel-master/upload/', '', request()->images);
        $model =$this->where('id',$id)->update([
            'title'=>request()->title,
            'img'=>$img,
            // 'images'=>$images,
        ]);
          return $model;
      }
  }elseif (request()->images != null) {
     $img = str_replace('http://localhost:88/travel-master/upload/', '', request()->img);
        $images = str_replace('http://localhost:88/travel-master/upload/', '', request()->images);
        $model =$this->where('id',$id)->update([
            'title'=>request()->title,
            // 'img'=>$img,
            'images'=>$images,
        ]);
          return $model;

  }
}else{

        $model =$this->where('id',$id)->update([
            'title'=>request()->title,
            // 'img'=>$img,
            // 'images'=>$images,
        ]);
          return $model;

    }
}
}
