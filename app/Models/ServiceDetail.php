<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Service;

class ServiceDetail extends Model
{
    protected $primaryKey = 'id';
    protected $fillable = [
        'ServiceID','TourID','Quantity','Totalamount',
    ];
    public function service()
    {
         return $this->belongsTo('App\Models\Service','ServiceID','id');
    }
     public function tour()
    {
         return $this->belongsTo('App\Models\Tour','TourID','id');
    }
    public function add($ServiceID, $TourID, $Quantity, $Totalamount){
        $modelss = $this->create([
            'ServiceID' => $ServiceID,
            'TourID' => $TourID,
            'Quantity' => $Quantity,
            'Totalamount' => $Totalamount,
        ]);
        return $modelss;
    }
}
