<?php

namespace App\Models;

use App\Models\customer;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\Hash;

class registeruser extends Authenticatable
{

    public function add()
    {
        $model = customer::create([
            'name' => request()->name,
            'address' => request()->address,
            'phone_number' => request()->phone_number,
            'date_of_birth' => request()->date_of_birth,
            'email' => request()->email2,
            'sex' => request()->sex,
            'password' => Hash::make(request()->password),
            'status' => 2,
            'level' => 2,

        ]);
        return $model;
    }
}
