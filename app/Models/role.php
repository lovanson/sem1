<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class role extends Model
{
    protected $fillable = ['name', 'permissions'];

    public function add()
    {
        $model = $this->create([
            'name' => request()->name,
            'permissions' => request()->permissions,
        ]);
        return $model;
    }

    public function edit($id)
    {
        $model = $this->where('id', $id)->update([
            'name' => request()->name,
            'permissions' => request()->permissions,
        ]);
        return $model;

    }

}
