<?php

namespace App\Models;
use App\Models\Service;
use Illuminate\Database\Eloquent\Model;
class Service extends Model
{
     protected $primaryKey = 'id';
    protected $fillable = [
        'Name','Description','Price','Status',
    ];
     public function add(){
        $model = Service::create([
            'Name'=>request()->Name,
            'Description'=>request()->Description,
            'Price'=>request()->Price,
            'Status'=>request()->Status,
        ]);
        return $model;
    }
    public function edit($id){

        $model =$this->where('id',$id)->update([
            'Name'=>request()->Name,
            'Description'=>request()->Description,
            'Price'=>request()->Price,
            'Status'=>request()->Status,
        ]);
        return $model;
    }
}
