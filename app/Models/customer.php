<?php

namespace App\Models;

use App\Models\role;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Http\Request;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Hash;

class customer extends Authenticatable
{
    use Notifiable;

    protected $primaryKey = 'id';
    protected $fillable = ['name', 'address', 'phone_number', 'date_of_birth', 'email', 'password', 'status', 'image', 'sex', 'email_verified_at', 'level'];

    public function add()
    {
        $file = request()->upload;
        $file_name = $file->getClientOriginalName();
        $file->move(public_path('upload/customer'), $file_name);
        $model = $this->create([
            'name' => request()->name,
            'address' => request()->address,
            'phone_number' => request()->phone_number,
            'date_of_birth' => request()->date_of_birth,
            'email' => request()->email,
            'image' => $file_name,
            'sex' => request()->sex,
            'password' => Hash::make(request()->password),
            'status' => 1,
            'level' => 1,

        ]);
        return $model;
    }
    public function edit($id)
    {
        $model = $this->where('id', $id)->update([
            'name' => request()->name,
            'address' => request()->address,
            'phone_number' => request()->phone_number,
            'date_of_birth' => request()->date_of_birth,
            'email' => request()->email,
            'sex' => request()->sex,
            'status' => request()->status,

        ]);
        return $model;

    }
    public function upload_avatar($id)
    {
        $customer = $this->where('id', $id)->first();
        if (request()->has('upload')) {
            $file = request()->upload;
            $file_name = $file->getClientOriginalName();
            $file->move(public_path('upload/customer'), $file_name);
        } else {
            $file_name = $customer->image;
        }

        $model = $this->where('id', $id)->update([
            'image' => $file_name,
        ]);
    }
    public function hasPermission($route)
    {
        $routes = $this->routes();
        return in_array($route, $routes) ? true : false;
        // return true;
    }
    public function routes()
    {
        $data = [];
        foreach ($this->getRoles as $role) {
            $permissions = json_decode($role->permissions);
            foreach ($permissions as $per) {
                if (!in_array($per, $data)) {
                    array_push($data, $per);
                }
            }
        }
        return $data;
    }
    public function getRoles()
    {
        return $this->belongsToMany(Role::class, 'role_assignments', 'customer_id', 'role_id');
    }
}
