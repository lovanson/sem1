<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Rating extends Model
{
    public $timestamps = false;
     protected $primaryKey = 'id';
    protected $fillable = ['TourID','UserID','NumberRating','Content'];
    public function  tour(){
        return $this->belongsTo('App\Models\Tour','TourID','id');
    }public function  customer(){
        return $this->belongsTo('App\Models\customer','UserID','id');
    }

    public function add(){
        $model = $this->create([
            'TourID'=>request()->TourID,
            'UserID'=>request()->UserID,
            'NumberRating'=>request()->NumberRating,
            'Content'=>request()->Content,
        ]);
        return $model;
    }
    public function edit($id){
        $model =$id->update([

            'NumberRating'=>request()->NumberRating,
            'Content'=>request()->Content,

        ]);
        return $model;

    }

}
