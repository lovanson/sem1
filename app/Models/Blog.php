<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\BlogImage;
class Blog extends Model
{
    protected $primaryKey = 'id';
    protected $fillable = [
        'Image','Content','Creator','TagID','BlogID','baner_top','Contents'
    ];
     public function blogimage(){
        return $this->hasMany('App\Models\BlogImage','BlogID','id');
    }

    public function  tag(){
        return $this->belongsTo('App\Models\Tag','TagID','id');
    }
     public function add(){


        $Image = str_replace('http://localhost:88/travel-master/upload/', '', request()->Image);
        $baner_top = str_replace('http://localhost:88/travel-master/upload/', '', request()->baner_top);
        $model = Blog::create([
            'Content'=>request()->Content,
            'Contents'=>request()->Contents,
            'Creator'=>request()->Creator,
            'TagID'=>request()->TagID,
           'Image'=>$Image,
           'baner_top'=>$baner_top,
        ]);
          return $model;
      }



       public function edit($id){
        if (request()->Image != null || request()->baner_top != null) {
           if (request()->Image != null) {
                if (request()->baner_top != null) {
                     $Image = str_replace('http://localhost:88/travel-master/upload/', '', request()->Image);
                    $baner_top = str_replace('http://localhost:88/travel-master/upload/', '', request()->baner_top);
                    $model =$this->where('id',$id)->update([
                        'Content'=>request()->Content,
                        'Contents'=>request()->Contents,
                        'Creator'=>request()->Creator,
                        'TagID'=>request()->TagID,
                        'Image'=>$Image,
                        'baner_top'=>$baner_top,
                    ]);
                      return $model;
                    dd("update both");
                }else{
                     $Image = str_replace('http://localhost:88/travel-master/upload/', '', request()->Image);
                    $baner_top = str_replace('http://localhost:88/travel-master/upload/', '', request()->baner_top);
                    $model =$this->where('id',$id)->update([
                        'Content'=>request()->Content,
                        'Contents'=>request()->Contents,
                        'Creator'=>request()->Creator,
                        'TagID'=>request()->TagID,
                        'Image'=>$Image,
                    ]);
                      return $model;
                    dd("update image");
                }
            }elseif (request()->baner_top != null) {
                 $Image = str_replace('http://localhost:88/travel-master/upload/', '', request()->Image);
                $baner_top = str_replace('http://localhost:88/travel-master/upload/', '', request()->baner_top);
                $model =$this->where('id',$id)->update([
                    'Content'=>request()->Content,
                    'Contents'=>request()->Contents,
                    'Creator'=>request()->Creator,
                    'TagID'=>request()->TagID,
                    'baner_top'=>$baner_top,
                ]);
                  return $model;
                dd("update banner top");
            }
        }else{
            $Image = str_replace('http://localhost:88/travel-master/upload/', '', request()->Image);
            $baner_top = str_replace('http://localhost:88/travel-master/upload/', '', request()->baner_top);
            $model =$this->where('id',$id)->update([
                'Content'=>request()->Content,
                'Contents'=>request()->Contents,
                'Creator'=>request()->Creator,
                'TagID'=>request()->TagID,
            ]);
              return $model;

        }
            dd("not update");

        }




}
