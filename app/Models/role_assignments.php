<?php

namespace App\Models;
use App\Models\role;
use App\Models\role_assignments;
use Illuminate\Database\Eloquent\Model;

class role_assignments extends Model
{
    public $timestamps = false;
    protected $fillable = [
        'customer_id','role_id'
    ];
}
