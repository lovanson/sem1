<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
// use App\Models\PaymentMethods;

class PaymentMethods extends Model
{
      protected $primaryKey = 'id';
    protected $fillable = [
        'Payment_type','Status',
    ];
     public function add(){
        $model = PaymentMethods::create([
            'Payment_type'=>request()->Payment_type,
            'Status'=>request()->Status,

        ]);
        return $model;
    }
    // public function edit($id){
    //     // dd(request()->all());

    //     dd($model);
    // }
     public function orders(){
        return $this->hasMany(Order::class,'PaymentID','id');
    }

}
