<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Category;
use App\Models\Tour;
class Category extends Model
{
    protected $primaryKey = 'id';
    protected $fillable = [
        'Keyword_Sell','CatName','Status'
    ];
    public function add(){

        $model = Category::create([
            'CatName'=>request()->CatName,
            'Keyword_Sell'=>request()->Keyword_Sell,
            'Status'=>request()->Status,
        ]);
        return $model;
    }
    public function edit($id){

        $model =$this->where('id',$id)->update([
            'CatName'=>request()->CatName,
            'Keyword_Sell'=>request()->Keyword_Sell,
            'Status'=>request()->Status,
        ]);
        return $model;
    }
    public function products(){
        return $this->hasMany(Tour::class,'CatID','id');
    }

}
