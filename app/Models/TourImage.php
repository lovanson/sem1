<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TourImage extends Model
{
     protected $table = 'tour_images';
    protected $fillable = [
        'Images','tour_id',
    ];
}
