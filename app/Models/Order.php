<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $primaryKey = 'id';
    protected $fillable = [
        'UserID','PaymentID',
        'Status','Totalamount',
    ];
     public function  orderDetail(){
        return $this->hasMany('App\Models\OrderDetail','OrderID','id');
    }

     public function  customer(){
        return $this->hasOne('App\Models\customer','id','UserID');
    }
     public function  payment(){
        return $this->hasOne('App\Models\PaymentMethods','id','PaymentID');
    }
    public function add(){

        $model = Order::create([
            'UserID'=>request()->UserID,
            'PaymentID'=>request()->PaymentID,
            'Status'=>1,
            'Totalamount'=>request()->Totalamount,
        ]);
        return $model;
    }
    public function edit($id){

        $model =$this->where('id',$id)->update([
          'UserID'=>request()->UserID,
            'PaymentID'=>request()->PaymentID,
            'Status'=>request()->Status,
            'Totalamount'=>request()->Totalamount,
        ]);
        return $model;
    }
}
