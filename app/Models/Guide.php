<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Guide extends Model
{
     protected $primaryKey = 'id';
    protected $fillable = [
        'Name','DoB','Email','Phone','Contactmethod','Status','avata'
    ];
    public function add(){
     $avata = str_replace('http://localhost:88/travel-master/upload/', '', request()->avata);

        $model = Guide::create([
            'Name'=>request()->Name,
            'DoB'=>request()->DoB,
            'Email'=>request()->Email,
            'Phone'=>request()->Phone,
            'Contactmethod'=>request()->Contactmethod,
            'Status'=>request()->Status,
            'avata'=>$avata,
        ]);
        return $model;
    }
    public function edit($id){
        if (request()->avata != null ) {
            $avata = str_replace('http://localhost:88/travel-master/upload/', '', request()->avata);

        $model =$this->where('id',$id)->update([
            'Name'=>request()->Name,
            'DoB'=>request()->DoB,
            'Email'=>request()->Email,
            'Phone'=>request()->Phone,
            'Contactmethod'=>request()->Contactmethod,
            'Status'=>request()->Status,
            'avata'=>$avata,
        ]);
        return $model;
        }else {

        $model =$this->where('id',$id)->update([
            'Name'=>request()->Name,
            'DoB'=>request()->DoB,
            'Email'=>request()->Email,
            'Phone'=>request()->Phone,
            'Contactmethod'=>request()->Contactmethod,
            'Status'=>request()->Status,
            // 'avata'=>$avata,
        ]);
        return $model;
        }


     }
}
