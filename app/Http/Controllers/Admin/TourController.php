<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Tour;
use App\Models\Tag;
use App\Models\Category;
use App\Models\Service;
use App\Models\ServiceDetail;
use Illuminate\Http\Request;
use App\Http\Requests\Tour\TourAddRequest;

class TourController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
         $cats = Tour::paginate(10);
        return view('admin.page.tour.list',compact('cats'));
    }
   public function search_tour(Request $request)
    {
        $search =$request->get('search');
            $cats = Tour::where('Name','like','%' .$search . '%')->paginate(10);
            return view('admin.page.tag.list',compact('cats'));
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
          $category = Category::all();
          $tag = Tag::all();
          $service = Service::all();
       return  view('admin.page.tour.create',[
        'category'=>$category,
        'tag'=>$tag,
        'service'=>$service,
       ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request,Tour $tour)
    {

       $model =$tour->add();

        foreach ($request->role as $value) {
            $service = new ServiceDetail();
            $service->TourID = $model->id;
            $service->ServiceID = $value;
             $modelss = $service->add($value, $model->id,1,1);
        }

        // dd();
         if ($model) {
             return redirect()->route('admin.tour.index')->with('yes','them thanh cong ');
       }else{
             return redirect()->back()->with('no','toang ');
       }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Tour  $tour
     * @return \Illuminate\Http\Response
     */
    public function show(Tour $tour)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Tour  $tour
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $tour=Tour::find($id);
        // dd($tour);
        $category = Category::all();
          $tag = Tag::all();
       return  view('admin.page.tour.update',[
        'category'=>$category,
        'tag'=>$tag,
        'tour'=>$tour
       ]);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Tour  $tour
     * @return \Illuminate\Http\Response
     */
    public function update(Tour $tour)
    {
          $tours= New Tour();
        $tours->edit($tour->id);
        return redirect()->route('admin.tour.index')->with('yes','caa nhat thanh cong!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Tour  $tour
     * @return \Illuminate\Http\Response
     */
    public function destroy(Tour $tour)
    {
     if ($tour) {
dd('ok');
        $tour->delete();
        return redirect()->route('admin.tour.index')->with('yes','xoa thanh cong ');
           }else{
            dd('no');
                 return redirect()->back()->with('no','danh muc co trong bang con khong the xoa ');
           }
    }
}
