<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\LoginRequest;
use App\Models\contact;
use App\Models\customer;
use App\Models\Order;
use App\Models\Tour;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Mail;

class AdminLoginController extends Controller
{
    public function getlogin()
    {

        if (Auth::check()) {
            // nếu đăng nhập thàng công thì
            return redirect()->route('admin.index');

        } else {
            return view('admin.page.login');
        }
    }
    public function postlogin(LoginRequest $request)
    {
        $success = array(
            'message' => 'Đăng nhập thành công',
            'alert-type' => 'success',
        );
        $error = array(
            'message' => 'Email hoặc Password không chính xác',
            'alert-type' => 'error',
        );
        $remember = ($request->remember == 1) ? true : false;
        $login = [
            'email' => $request->email,
            'password' => $request->password,
        ];
        if (Auth::attempt($login, $remember)) {
            return redirect()->route('admin.index')->with($success);

        } else {
            return redirect()->back()->with($error);
        }
    }
    public function getlogout()
    {
        $success = array(
            'message' => 'Đăng xuất thành công!',
            'alert-type' => 'success',
        );
        Auth::logout();
        return redirect()->route('admin.getlogin')->with($success);
    }
    public function get_code()
    {
        return view('admin.page.password');
    }
    public function post_code()
    {

        $success = array(
            'message' => 'Đổi mật khẩu thành công',
            'alert-type' => 'success',
        );
        $error = array(
            'message' => 'Đổi mật khẩu thành công',
            'alert-type' => 'error',
        );
        $checkcustomer = customer::where([
            'email' => request()->email,
            'email_verified_at' => request()->email_verified_at,
        ])->first();
        if ($checkcustomer) {
            customer::where('email', request()->email)->update([
                'password' => bcrypt(request()->password),
            ]);
            return redirect()->route('admin.getlogin')->with($success);
        } else {
            return redirect()->back()->with($error);
        }
    }
    public function get_password()
    {
        return view('admin.page.forgotpassword');
    }
    public function post_password(Request $request)
    {
        $success = array(
            'message' => 'Email chưa được đăng kí tài khoản!',
            'alert-type' => 'error',
        );
        $checkcustomer = customer::where('email', request()->email)->first();
        if (!$checkcustomer) {

            return redirect()->back()->with($success);
        }
        $date = Carbon::now('Asia/Ho_Chi_Minh')->subMinutes(1);
        customer::where('updated_at', '<=', $date)->update([
            'email_verified_at' => null,
        ]);
        $id = $checkcustomer->id;
        $code = mt_rand(100000, 999999);
        $checkcustomer->email_verified_at = $code;
        $checkcustomer->save();
        $url = route('admin.get_code', ['code' => bcrypt($checkcustomer->email_verified_at), 'email' => request()->email]);
        $email = request()->email;
        $data = [
            'code' => $checkcustomer->email_verified_at,
            'route' => $url,
        ];
        Mail::send('admin.page.mail', $data, function ($message) use ($email) {
            $message->from('wendtravel68@gmail.com');
            $message->to($email, 'Reset Password')->subject('lấy lại mật khẩu!');

        });

        return view('admin.page.password');
    }
    public function post_change_pasword(Request $request)
    {
        $error = array(
            'message' => 'Sai mật khẩu!',
            'alert-type' => 'error',
        );
        $email = Auth::user()->email;
        $password = request()->password;
        $newpassword = request()->newpassword;
        if (Auth::attempt(['email' => $email, 'password' => $password])) {
            customer::where('email', $email)->update([
                'password' => Hash::make($newpassword),
            ]);
            return redirect()->route('admin.getlogout');
        } else {
            return redirect()->back()->with($error);
        }
    }
    public function dashboard()
    {
        $Order = Order::all()->count();
        $customer = customer::all()->count();
        $tour = Tour::all()->count();
        $contact = contact::all()->count();
        return view('admin/page/index', [
            'order' => $Order,
            'customer' => $customer,
            'tour' => $tour,
            'contact' => $contact,
        ]);
    }

}
