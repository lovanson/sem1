<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\register as register;
use App\Models\RegisterCustomer;

class RegisterController extends Controller
{
    public function post_register(register $register, RegisterCustomer $RegisterCustomer)
    {
        $success = array(
            'message' => 'Đăng kí tài khoản thành công!',
            'alert-type' => 'success',
        );
        $error = array(
            'message' => 'Đăng kí tài khoản không thành công!',
            'alert-type' => 'error',
        );
        $model = $RegisterCustomer->add();

        if ($model) {
            return redirect()->route('admin.getlogin')->with($success);
        } else {
            return redirect()->back()->with($error);
        }
        $request->offsetUnset('_token');
    }
    public function get_register()
    {
        return view('admin.page.Register');
    }
}
