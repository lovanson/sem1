<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Customer as cus;
use App\Models\customer;
use App\Models\role;
use App\Models\role_assignments;
use Illuminate\Http\Request;

class customerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $customer = customer::paginate(10);

        return view('admin.page.customer.list', compact('customer'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $roles = Role::orderBy('name', 'ASC')->get();
        return view('admin.page.customer.create', compact('roles'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(cus $request, customer $customer)
    {
        $success = array(
            'message' => 'Thêm mới thành công!',
            'alert-type' => 'success',
        );
        $error = array(
            'message' => 'Thêm mới thất bại!',
            'alert-type' => 'error',
        );
        $model = $customer->add();

        if (is_array($request->role)) {
            foreach ($request->role as $role_id) {
                role_assignments::create(['customer_id' => $model->id, 'role_id' => $role_id]);
            }
        }

        if ($model) {
            return redirect()->route('admin.customer.index')->with($success);
        } else {
            return redirect()->back()->with($error);
        }
        $request->offsetUnset('_token');
        $request->validate([
            'name' => 'required|unique:customer',
        ], [
            'name.required' => 'This field cannot be empty!',
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\customer  $customer
     * @return \Illuminate\Http\Response
     */
    public function show(customer $customer)
    {

    }
      public function search_customer(Request $request)
    {
        $search =$request->get('search');
            $customer = customer::where('name','like','%' .$search . '%')->paginate(10);
            return view('admin.page.customer.list',compact('customer'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\customer  $customer
     * @return \Illuminate\Http\Response
     */
    public function edit(customer $customer)
    {
        $roles = Role::orderBy('name', 'ASC')->get();
        return view('admin.page.customer.update', compact('customer', 'roles'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\customer  $customer
     * @return \Illuminate\Http\Response
     */
    public function update(cus $request, customer $customer)
    {
        $success = array(
            'message' => 'Cập nhật thành công!',
            'alert-type' => 'success',
        );
        role_assignments::where('customer_id', $customer->id)->delete();
        if (is_array($request->role)) {
            foreach ($request->role as $role_id) {
                role_assignments::create(['customer_id' => $customer->id, 'role_id' => $role_id]);
            }
        }
         $customer->edit($customer->id);
        return redirect()->route('admin.customer.index')->with($success);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\customer  $customer
     * @return \Illuminate\Http\Response
     */
    public function destroy(customer $customer)
    {
        $success = array(
            'message' => 'xóa thành công!',
            'alert-type' => 'success',
        );
        $error = array(
            'message' => 'không thể xóa thành công!',
            'alert-type' => 'error',
        );
        if ($customer) {
            $customer->delete();
            return redirect()->route('admin.customer.index')->with($success);
        } else {
            return redirect()->back()->with($error);
        }
    }
    public function postavatar(customer $customer)
    {
        $success = array(
            'message' => 'Cập nhật ảnh thành công!',
            'alert-type' => 'success',
        );
        $customer->upload_avatar($customer->id);

        return redirect()->route('admin.customer.index')->with($success);
    }
    public function changestatus($id)
    {
        $success = array(
            'message' => 'Cập nhật thành công!',
            'alert-type' => 'success',
        );
        $c = customer::where('id', $id)->first();
        if ($c->status == 1) {
            customer::where('id', $id)->update([
                'status' => 2,
            ]);
        } else {
            customer::where('id', $id)->update([
                'status' => 1,
            ]);
        }
        return redirect()->back()->with('message', 'Status changed!');
    }
    public function error()
    {
        $code = request()->code;
        return view('admin.page.error');
    }
}
