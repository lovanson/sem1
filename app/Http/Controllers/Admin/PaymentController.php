<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\PaymentMethods;
use Illuminate\Http\Request;
use App\Http\Requests\Payment\PaymentAddRequest;
class PaymentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
           $cats = PaymentMethods::paginate(10);
        return view('admin.page.payment.list',compact('cats'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return  view('admin.page.payment.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(PaymentAddRequest $request,PaymentMethods $paymentMethods)
    {
       $model =$paymentMethods->add();

         if ($model) {
             return redirect()->route('admin.payment.index')->with('yes','them thanh cong ');
       }else{
             return redirect()->back()->with('no','toang ');
       }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\PaymentMethods  $paymentMethods
     * @return \Illuminate\Http\Response
     */
    public function show(PaymentMethods $paymentMethods)
    {
        //
    }
    public function search_payment(Request $request)
    {
        $search =$request->get('search');
            $cats = PaymentMethods::where('Payment_type','like','%' .$search . '%')->paginate(10);
            return view('admin.page.payment.list',compact('cats'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\PaymentMethods  $paymentMethods
     * @return \Illuminate\Http\Response
     */
    public function edit(PaymentMethods $paymentMethods,$id)
    {
        // dd($paymentMethods);
        $paymentMethods = PaymentMethods::find($id);
        return view ('admin.page.payment.update',[
            'paymentMethods'=>$paymentMethods
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\PaymentMethods  $paymentMethods
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, PaymentMethods $paymentMethods,$id)
    {
          $paymentMethods =PaymentMethods::find($id)->update([
            'Payment_type'=>request()->Payment_type,
            'Status'=>request()->Status,
        ]);


        return redirect()->route('admin.payment.index')->with('yes','caa nhat thanh cong!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\PaymentMethods  $paymentMethods
     * @return \Illuminate\Http\Response
     */
    public function destroy(PaymentMethods $paymentMethods ,$id)
    {
        PaymentMethods::find($id)->delete();
       if ($paymentMethods && $paymentMethods->orders()->count()==0) {
        $paymentMethods->delete();

        return redirect()->route('admin.payment.index')->with('yes','xoa thanh cong ');
           }else{
                 return redirect()->back()->with('no','danh muc co trong bang con khong the xoa ');
           }


    }
}
