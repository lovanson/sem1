<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Blog;
use App\Models\Tag;
use Illuminate\Http\Request;
use App\Http\Requests\Blog\BlogAddRequest;

class BlogController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
          $cats = Blog::paginate(10);
        return view('admin.page.blog.list',compact('cats'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
            $cats = Blog::all();
            $tag = Tag::all();
           return  view('admin.page.blog.create',[
                'cats'=>$cats,
                'tag'=>$tag,
           ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request,Blog $blog)
    {
        $model =$blog->add();

         if ($model) {
             return redirect()->route('admin.blog.index')->with('yes','them thanh cong ');
       }else{
             return redirect()->back()->with('no','toang ');
       }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Blog  $blog
     * @return \Illuminate\Http\Response
     */
    public function show(Blog $blog)
    {
        //
    }
     public function search_blog(Request $request)
    {
        $search =$request->get('search');
            $cats = Blog::where('Content','like','%' .$search . '%')->paginate(10);
            return view('admin.page.blog.list',compact('cats'));
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Blog  $blog
     * @return \Illuminate\Http\Response
     */
    public function edit( $id)
    {
        $blog = Blog::find($id);
       $cats = Blog::all();
            $tag = Tag::all();
           return  view('admin.page.blog.update',[
                'cats'=>$cats,
                'tag'=>$tag,
                'blog'=>$blog
           ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Blog  $blog
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Blog $blog )
    {
        $blog->edit($blog->id);

        return redirect()->route('admin.blog.index')->with('ss','caa nhat thanh cong!');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Blog  $blog
     * @return \Illuminate\Http\Response
     */
    public function destroy(Blog $blog)
    {
        if ($blog) {
        $blog->delete();

        return redirect()->route('admin.blog.index')->with('yes','xoa thanh cong ');
           }else{
                 return redirect()->back()->with('no','danh muc co trong bang con khong the xoa ');
           }
    }
}
