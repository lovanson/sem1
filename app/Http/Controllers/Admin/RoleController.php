<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\role;
use App\Models\role_assignments;
use Illuminate\Http\Request;
use Route;

class RoleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $role = role::paginate(10);
        return view('admin.page.role.list', compact('role'));
    }

 public function seach_role(Request $request)
    {
        $search =$request->get('search');
            $role = role::where('name','like','%' .$search . '%')->paginate(10);
            return view('admin.page.role.list',compact('role'));
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        $routes = [];
        $all = Route::getRoutes();
        foreach ($all as $a) {
            $name = $a->getName();
            $pos = strpos($name, 'admin');
            if ($pos !== false && !in_array($name, $routes)) {
                array_push($routes, $a->getName());
            }
        }
        return view('admin.page.role.create', compact('routes'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $arrRoute = $request->route;
        array_push($arrRoute, 'admin.index', 'admin.getlogin', 'admin.postlogin', 'admin.get_password', 'admin.post_password', 'admin.get_code', 'admin.post_code', 'admin.get_register', 'admin.post_change_pasword', 'admin.getlogout', 'admin.post_register');
        $success = array(
            'message' => 'Thêm mới nhóm quyền thành công !',
            'alert-type' => 'success',
        );
        $routes = json_encode($arrRoute);
        Role::create(['name' => $request->name, 'permissions' => $routes]);
        return redirect()->route('admin.role.index')->with($success);

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\role  $role
     * @return \Illuminate\Http\Response
     */
    public function show(role $role)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\role  $role
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        $model = Role::find($id);
        $permissions = json_decode($model->permissions);
        $routes = [];
        $all = Route::getRoutes();
        foreach ($all as $a) {
            $name = $a->getName();
            $pos = strpos($name, 'admin');
            if ($pos !== false && !in_array($name, $routes)) {
                array_push($routes, $a->getName());
            }
        }
        return view('admin.page.role.update', compact('routes', 'model', 'permissions'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\role  $role
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $role)
    {
        $arrRoute = $request->route;
        array_push($arrRoute, 'admin.index', 'admin.getlogin', 'admin.postlogin', 'admin.get_password', 'admin.post_password', 'admin.get_code', 'admin.post_code', 'admin.get_register', 'admin.post_change_pasword', 'admin.getlogout', 'admin.post_register');
        $rolemodel = new role();
        $success = array(
            'message' => 'Cập nhật nhóm quyền thành công!',
            'alert-type' => 'success',
        );
        // $role->edit($role->id);
        // $data->active();

        $routes = json_encode($arrRoute);
        $rolemodel::where('id', $role)->update(['name' => $request->name, 'permissions' => $routes]);
        return redirect()->route('admin.role.index')->with($success);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\role  $role
     * @return \Illuminate\Http\Response
     */
    public function destroy(role $role)
    {
        $success = array(
            'message' => 'Thêm mới nhóm quyền thành công!',
            'alert-type' => 'success',
        );
        $error = array(
            'message' => 'Thêm mới nhóm quyền thành công!',
            'alert-type' => 'error',
        );
        if ($role) {
            $role->delete();
            return redirect()->route('admin.role.index')->with($success);
        } else {
            return redirect()->back()->with($error);
        }
    }
}
