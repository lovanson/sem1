<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Service;
use Illuminate\Http\Request;
use App\Http\Requests\Service\ServiceAddRequest;

class ServiceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
         $cats = Service::paginate(10);
        return view('admin.page.service.list',compact('cats'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $cats = service::all();
       return view ('admin.page.service.create',compact('cats'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ServiceAddRequest $request,Service $service)
    {
        $model =$service->add();

         if ($model) {
             return redirect()->route('admin.service.index')->with('yes','them thanh cong ');
       }else{
             return redirect()->back()->with('no','toang ');
       }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Service  $service
     * @return \Illuminate\Http\Response
     */
    public function show(Service $service)
    {
        //
    }
      public function search_service(Request $request)
    {
        $search =$request->get('search');
            $cats = Service::where('Name','like','%' .$search . '%')->paginate(10);
            return view('admin.page.service.list',compact('cats'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Service  $service
     * @return \Illuminate\Http\Response
     */
    public function edit(Service $service)
    {
          return view ('admin.page.service.update',['service'=>$service]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Service  $service
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Service $service)
    {
        $service->edit($service->id);

                return redirect()->route('admin.service.index')->with('yes','caa nhat thanh cong!');
            }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Service  $service
     * @return \Illuminate\Http\Response
     */
    public function destroy(Service $service)
    {
        if ($service ) {
        $service->delete();

        return redirect()->route('admin.service.index')->with('yes','them thanh cong ');
           }else{
                 return redirect()->back()->with('no','danh muc co trong bang con khong the xoa ');
           }


    }
}
