<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Order;
use App\Models\OrderDetail;
use Illuminate\Http\Request;
use App\Models\Tag;
use App\Models\Tour;
use App\Models\customer;
use App\Models\PaymentMethods;

class OrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
         $cats = Order::paginate(10);
        return view('admin.page.order.list',compact('cats'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
         $paymentMethods = PaymentMethods::all();
          $customer = customer::all();
       return  view('admin.page.order.create',[
        'paymentMethods'=>$paymentMethods,
        'customer'=>$customer,
       ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request,Order $order)
    {

       $model =$order->add();

         if ($model) {
             return redirect()->route('order.index')->with('yes','them thanh cong ');
       }else{
             return redirect()->back()->with('no','toang ');
       }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function show(Order $order)
    {
        //
    }
     public function seach_order(Request $request)
    {
        $search =$request->get('search');
            $cats = Order::where('Name','like','%' .$search . '%')->paginate(10);
            return view('admin.page.order.list',compact('cats'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function edit(Order $order)
    {
        $paymentMethods = PaymentMethods::all();
          $tag = Tag::all();
       return  view('admin.page.tour.create',[
        'paymentMethods'=>$paymentMethods,
        'tag'=>$tag,
       ]);
    }
    /**

     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Order $order)
    {
        $order->edit($order->id);

        return redirect()->route('order.index')->with('yes','caa nhat thanh cong!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function destroy(Order $order)
    {
        //
    }
     public function orderview(Request $request){
        $id = $request->id;
        $order = OrderDetail::with('tour')->where('OrderID',$id)->first();
        // dd($order);
         // $customer = customer::all();

        return view('admin.page.orderDetail.show',[
            'order' => $order,
            // 'customer' => $customer,
        ]);
    }
}
