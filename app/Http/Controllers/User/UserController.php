<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Http\Requests\signup as signup;
use Validator;
use App\Models\Blog;
use App\Models\Category;
use App\Models\contact;
use App\Models\customer;
use App\Models\gallery;
use App\Models\Order;
use App\Models\OrderDetail;
use App\Models\PaymentMethods;
use App\Models\registeruser;
use App\Models\Service;
use App\Models\ServiceDetail;
use App\Models\Tag;
use App\Models\Tour;
use App\Models\TourImage;
use Auth;
use Carbon\Carbon;
use Hash;
use Illuminate\Http\Request;
use Mail;
use App\Models\Rating;
use App\Models\BlogImage;
use App\Models\Guide;
use App\Http\Requests\Booktour\BooktourDetailAddRequest;
use App\Http\Requests\LoginRequest;

class UserController extends Controller
{

    public function home()
    {
        // $rating = Rating::where('TourID',$id);
        $customers = Auth::user();
        $category= Category::all();
        $tour =Tour::orderBy('id','desc')->paginate(8);
        $guide = Guide::where('Status',1)->paginate(4);
        $tag =Tag::all();
        $rating = Rating::paginate(3);
        $blog = Blog::paginate(6);
        // $ratings = round($rating);
        return view('User.page.home',[
            'category'=>$category,
            'tour'=>$tour,
            'guide'=>$guide,
            'customers'=>$customers,
            'tag'=>$tag,
            'rating'=>$rating,
            'blog'=>$blog,
            // 'ratings'=>$ratings,
        ]);

    }
    public function search_home(Request $req){
        $error = array(
            'message' => 'Không tìm thấy ',
            'alert-type' => 'error',
        );
        $priceMin=(int) substr(request()->min_value,1);
        $priceMax=(int) substr(request()->max_value,1);
        $CatID= request()->category;
         $category= Category::all();
         $tag =Tag::all();
        $TagID = request()->tag;
        $customers = Auth::user();
          $rating = Rating::paginate(3);
        $guide = Guide::where('Status',1)->get();
        $blog = Blog::paginate(6);
        $tour = Tour::where([
            ['CatID','=',$CatID],
            ['TagID','=',$TagID],
            ['Priceadult','>=',$priceMin],
            ['Priceadult','<=',$priceMax],

        ])->paginate(8);
        // dd($tour);bb
        if(count($tour) !== 0){
         return view('User.page.tour_packages',[
            'category'=>$category,
            'tag'=>$tag,
            'tour'=>$tour,
            'guide'=>$guide,
            'customers'=>$customers,
            'req'=>$req,
            'blog'=>$blog,
            'rating'=>$rating,
            'priceMin'=>$priceMin,
            'priceMax'=>$priceMax
         ]);
        }else {

            return redirect()->back()->with('oknha','không tìm thấy');
        }
    }
    public function tour_details($id)
    {

        $category = Category::all();
        $tour = Tour::find($id);

        $tourimage = TourImage::where('tour_id', $id)->get();
        $customers = Auth::user();
        // dd($customers);
        $tag =Tag::all();

        // dd($tour);
        // dd($customer);
        $ServiceDetail = ServiceDetail::where('TourID',$id)->get();
        // $ServiceDetail = ServiceDetail::with('service:id,Name')->get();

        $service = Service::all();
        $paymentMethods = PaymentMethods::all();
        $rating = Rating::where('TourID',$id)->avg('NumberRating');
        $ratings = round($rating);
        $countReview = Rating::where('TourID',$id)->count();

        $comment = Rating::where('TourID',$id)->with('customer:id,name,image')->orderBy('id','desc')->paginate(2);

        // dd($tour->rating->);
        // dd($comment);
        // if (Auth::user() != null) {

        $coment_me = Rating::where('TourID',$id)->orderBy('id','desc')->first();
        // dd($coment_me);

        return view('User.page.tour_details', [
            'category' => $category,
            'tour' => $tour,
            'tourimage' => $tourimage,
            'service' => $service,
            'paymentMethods' => $paymentMethods,
            'customers'=> $customers,
            'tag'=> $tag,
            'ServiceDetail'=> $ServiceDetail,
            'rating'=>$rating,
            'ratings'=>$ratings,
            'countReview'=>$countReview,
            'comment'=>$comment,
        ]);

    }
    // hiển thị tour đã đặt
     public function information(){
        $category =Category::all();
        $tag = Tag::all();
        if (Auth::user() != null) {
        $showorder = Order::where('UserID',Auth::user()->id)->paginate(5);
        }
          // $tour = Tour::find($id);

        return view('User.page.information',[
         'showorder'=>$showorder,
         'category'=>$category,
         'tag'=>$tag,
         // 'tour' => $tour,
        ]);
    }
    public function seach_information(){
         $error = array(
            'message' => 'Không tìm thấy ',
            'alert-type' => 'error',
        );
        $start_date = request()->start_date;
        $end_date = request()->end_date;
        $categorys= request()->category;
        $tags = request()->tag;
        // $customers = Auth::user();
        // $guide = Guide::where('Status',1)->get();
        $tour = Tour::where([
            ['CatID','=',$categorys],
            ['TagID','=',$tags],
        ])->get();
            // ['CatID','=',$category],
            // ['TagID','=',$tag],
            // ['Start','>=',$start_date],
            // ['Start','<=',$start_date],
            // ['End','<=',$end_date],
            // ['End','<=',$end_date],
        if(count($tour) !== 0){
         return view('User.page.information',[
            'categorys'=>$categorys,
            'tags'=>$tags,
            'tour'=>$tour,
            // 'guide'=>$guide,
            // 'customers'=>$customers,
         ]);
        }else {

            return redirect()->back()->with('ok','không tìm thấy');
        }
    }
    public function tour_details_post(BooktourDetailAddRequest $request,Order $order,OrderDetail $orderDetail)
    {
        // dd('ok');
          if (Auth::check()) {
            $order->Status = 1;
            $model = $order->add();
            $id_order = $model->id;
            $models = $orderDetail->add($id_order);
            if ($model) {
                $showorder = Order::where('UserID', Auth::user()->id)->orderBy('id', 'desc')->paginate(1);
                $email = Auth::user()->email;
                $data = [
                    'name' => Auth::user()->name,
                    'tour_name' => $models->tours->Name,
                    'start' => $models->Start,
                    'end' => $models->End,
                    'guestchild' => $models->guestchild,
                    'guestchildren' => $models->guestchildren,
                    'guestadult' => $models->guestadult,
                    'total' => $showorder[0]->Totalamount,
                ];
                Mail::send('User.page.mail', $data, function ($message) use ($email) {
                    $message->from('wendtravel68@gmail.com');
                    $message->to($email, 'Đặt tour thành công!')->subject('Đặt tour thành công!');

                });
                return redirect()->route('User.home')->with('yes', 'Đặt tour thành công ');
            } else {
                return redirect()->back()->with('no', 'toang ');
            }
        } else {
            return back()->with('no', 'bạn vui lòng đăng nhập trước khi đặt tour');
        }

        // dd($models);


    }
      public function rating_post(Rating $rating,Request $request){
        $success = array(
            'message' => 'Cảm ơn bạn đã đánh giá',
            'alert_type' => 'success',
        );
        $error = array(
            'message' => 'Cảm ơn bạn đã đánh giá',
            'alert_type' => 'error',
        );
        $tourid = $request->TourID;
        $delete = Rating::where('TourID',$tourid)->where('UserID',Auth::user()->id)->first();
        if($delete){
           $model= $delete->edit($delete);

             if ($model) {

                     return response($success);
               }else{
                     return response($error);
               }
        }else{

            $model =$rating->add();
             if ($model) {
                 return response($success);
            }else{
                 return response($error);
           }
        }

    }
    public function blog_single($id)
    {
        $countReview = Blog::where('id',$id)->count();
        // dd($countReview);
         $blog = Blog::where("id",$id)->first();
        $blogimage= BlogImage::where('BlogID',$id)->get();
         $tag = Tag::all();
         $blogmini = Blog::orderBy('id','desc')->paginate(10) ;

        return view('User.page.blog_single',[
            'blog'=> $blog,
            'blogimage'=>$blogimage,
            'tag'=>$tag,
            'blogmini'=>$blogmini,
            'countReview'=>$countReview,
        ]);
    }
    public function blog(Request $request)
    {
        $tag = Tag::all();

        $blog = Blog::paginate(6);
        if ($request->id !=null) {
            $blog = Blog::where('tagID',$request->id)->paginate(6);
        }
        return view('User.page.blog',[
                'blog'=>$blog,
                'tag'=>$tag,
        ]);
    }
    public function search_blogs(Request $request)
    {
        $error = array(
            'message' => 'Không tìm thấy kết quả nào',
            'alert-type' => 'error',
        );
        $search =$request->get('search');
            $blog = Blog::where('Content','like','%' .$search . '%')->paginate(10);
            // dd($search);
        if(count($blog) !== 0){
            return view('User.page.blog',compact('blog'));

        }elseif ($blog != $search ){
            return back()->with('ok','không tìm thấy');
        }
    }
    public function about()
    {
        $blog = Blog::paginate(5);
        $guide = Guide::where('Status',1)->paginate(4);
        return view('User.page.about',[
            'guide' => $guide,
            'blog'=>$blog,
        ]);
    }
    public function faq()
    {
        return view('User.page.faq');
    }
    public function service()
    {
        $blog = Blog::paginate(5);
         $guide = Guide::where('Status',1)->get();
        return view('User.page.service',[
            'guide'=>$guide,
             'blog'=>$blog,
        ]);
    }
    public function gallery()
    {
        $gallery = Gallery::all();
         $blog = Blog::paginate(6);
        return view('User.page.gallery', [
            'gallery' => $gallery,
            'blog' => $blog,
        ]);
    }
    public function contact()
    {

        $customers = Auth::user();
         $contact = contact::all();
        return view('User.page.contact',[
            'contact'=>$contact,
            'customers'=>$customers,
        ]);
    }
    public function contact_post(Request $request, contact $contact)
    {
        $success = array(
            'message' => 'Cảm ơn bạn đã góp ý.',
            'alert-type' => 'success',
        );
       $model =$contact->add();

         if ($model) {
             return redirect()->route('User.contact')->with($success);
       }else{
             return redirect()->back()->with('no','toang ');
       }
    }
    public function tour_packages($id)
    {

         $tag =Tag::all();
        $customers = Auth::user();
          $category= Category::all();
         $guide = Guide::where('Status',1)->get();
        $tour =Tour::where('CatID',$id)->orderBy('id','desc')->paginate(8);
        $rating = Rating::paginate(3);
         $blog = Blog::paginate(6);
        return view('User.page.tour_packages',[
            'category'=>$category,
            'tour'=>$tour,
            'guide'=>$guide,
            'customers'=>$customers,
            'tag'=>$tag,
            'rating'=>$rating,
            'blog'=>$blog,
        ]);
    }
    public function error()
    {
        return view('User.page.error');
    }
     public function guideview(Request $request){
        $id = $request->id;
        $guide = Guide::where('id',$id)->first();
        // dd($guide);

        return view('User.page.guideview',[
            'guide' => $guide,
        ]);
    }
    public function postpegister(Request $request, registeruser $registeruser)
    {
        // dd($request->all());
        $validator = Validator::make($request->all(),[
            'name' => 'required|string|max:255|unique:customers',
            'email2' => 'required|max:255|email|unique:customers,email',
            'address' => 'required|max:255',
            'password2' => 'required|min:5',
            'confirm_password2' => 'required|same:password2',
            'date_of_birth' => 'required',
            'phone_number' => 'required|min:10|numeric|unique:customers',

        ],[
           'name.required' => 'Tên Không Được Để Trống!',
           'name.unique' => 'Tên Không Được TRùng nhau!',
           'address.required' => 'Địa chỉ Không Được Để Trống!',
           'email2.required' => 'Email Không Được Để Trống!',
           'email2.unique' => 'Email Không Được trùng nhau!',
           'email2.email' => 'Email không đúng định dạng!',
           'password2.required' => 'Mật khẩu Không Được Để Trống!',
           'password2.min' => 'Mật khẩu quá ngắn!',
           'confirm_password2.required' => 'Nhập lại Không Được Để Trống!',
           'confirm_password2.same' => 'Bạn cần phải nhập đúng mật khẩu trên!',
           'date_of_birth.required' => 'Ngày sinh không được để trống!',
           'phone_number.required' => 'Số điện thoại không được để trống!',
           'phone_number.min' => 'Số điện thoại không đúng định dạng!',
           'phone_number.numeric' => 'Số điện thoại không đúng định dạng!',
           'phone_number.unique' => 'Số điện thoại không được trùng nhau !',

        ]);

        // $customer->level = 2;
        $success = array(
            'message' => 'Đăng kí tài khoản thành công!',
            'alert-type' => 'success',
        );
        $error = array(
            'message' => 'Đăng kí tài khoản không thành công!',
            'alert-type' => 'error',
        );

        if ($validator->passes()) {
              $model = $registeruser->add();
            return response($success);

        } else {
             return response()->json(['error'=>$validator->errors()->all()]);;
        }
    }
    public function postlogin(Request $request)
    {
        $ms = [];
        // dd($request);
        $success = array(
            'message' => 'Đăng nhập thành công',
            'alert_type' => 'success',
        );
        $error = array(
            'message' => 'Email hoặc Password không chính xác',
            'alert_type' => 'error',
        );
        $remember = ($request->remember == 1) ? true : false;
        $login = [
            'email' => $request->email,
            'password' => $request->password,
        ];
        if (Auth::attempt($login, $remember)) {
            if(request()->model = null){
                return response($success);
            }else {
                return response($success);
            }

        } else {
            return response($error);
        }
    }
    public function getlogout()
    {
        $success = array(
            'message' => 'Đăng xuất thành công!',
            'alert-type' => 'success',
        );
        Auth::logout();
        return redirect()->route('User.home')->with($success);
    }
    public function post_change_pasword(Request $request)
    {
        $error = array(
            'message' => 'Sai mật khẩu!',
            'alert-type' => 'error',
        );
        $success = array(
            'message' => 'Sai mật khẩu!',
            'alert-type' => 'success',
        );
        $email = Auth::user()->email;
        $password = request()->password;
        $newpassword = request()->newpassword;
        if (Auth::attempt(['email' => $email, 'password' => $password])) {
            customer::where('email', $email)->update([
                'password' => Hash::make($newpassword),
            ]);
            return redirect()->route('User.logout')->with($success);
        } else {
            return redirect()->back()->with($error);
        }
    }
    public function post_password(Request $request)
    {
        $error = array(
            'message' => 'Email chưa được đăng kí tài khoản!',
            'alert-type' => 'error',
        );
        $checkcustomer = customer::where('email', request()->email)->first();
        if (!$checkcustomer) {

            return redirect()->back()->with($error);
        }
        $date = Carbon::now('Asia/Ho_Chi_Minh')->subMinutes(1);
        customer::where('updated_at', '<=', $date)->update([
            'email_verified_at' => null,
        ]);
        $id = $checkcustomer->id;
        $code = mt_rand(100000, 999999);
        $checkcustomer->email_verified_at = $code;
        $checkcustomer->save();
        $url = route('admin.get_code', ['code' => bcrypt($checkcustomer->email_verified_at), 'email' => request()->email]);
        $email = request()->email;
        $data = [
            'code' => $checkcustomer->email_verified_at,
            'route' => $url,
        ];
        Mail::send('admin.page.mail', $data, function ($message) use ($email) {
            $message->from('wendtravel68@gmail.com');
            $message->to($email, 'Reset Password')->subject('lấy lại mật khẩu!');

        });

        return view('User.page.Forgotpass');
    }
    public function get_code()
    {
        return view('User.page.Forgotpass');
    }
    public function post_code()
    {

        $success = array(
            'message' => 'Đổi mật khẩu thành công',
            'alert-type' => 'success',
        );
        $error = array(
            'message' => 'Đổi mật khẩu thành công',
            'alert-type' => 'error',
        );
        $checkcustomer = customer::where([
            'email' => request()->email,
            'email_verified_at' => request()->email_verified_at,
        ])->first();
        if ($checkcustomer) {
            customer::where('email', request()->email)->update([
                'password' => bcrypt(request()->password),
            ]);
            return redirect()->route('User.home')->with($success);
        } else {
            return redirect()->back()->with($error);
        }
    }
    public function postavatar(customer $customer)
    {
        $success = array(
            'message' => 'Cập nhật ảnh thành công!',
            'alert-type' => 'success',
        );
        $customer->upload_avatar($customer->id);

        return redirect()->route('User.home')->with($success);
    }

}
