<?php

namespace App\Http\Requests\OrderDetail;

use Illuminate\Foundation\Http\FormRequest;

class OrderDetailAddRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'Start'=> 'required|unique:order_details',

        ];
    }
    public function messages()
    {
        return [
            'Start.required'=> 'Ngày bắt đầu không được để trống !',
            'Start.unique'=> 'Phương thức thanh toán không đước trùng nhau !',
        ];
    }
}
