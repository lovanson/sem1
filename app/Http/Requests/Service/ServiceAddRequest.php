<?php

namespace App\Http\Requests\Service;

use Illuminate\Foundation\Http\FormRequest;

class ServiceAddRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'Name'=> 'required|unique:services',
            'Price' => 'required|regex:/^\d+(\.\d{1,2})?$/',
            'Description'=>'required',
        ];
    }
    public function messages()
    {
        return [
            'Name.required'=> 'Tên không được để trống',
            'Name.unique'=> 'Tên không được trùng nhau',
            'Price.required' => 'Giá không được để trống',
            'Price.regex' => 'Giá dịch vụ chỉ được để số',
            'Description.required' => 'Mô tả không được để trống',
        ];
    }
}
