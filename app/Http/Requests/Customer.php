<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class Customer extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ($this->method()) {
            case 'POST':
                return [
                    'name' => 'required|string|max:255',
                    'email' => 'required|max:255',
                    'address' => 'required|max:255',
                    'upload' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
                    'password' => 'required|min:5',
                    'confirm_password' => 'required|same:password',
                    'date_of_birth' => 'required',
                    'phone_number' => 'required|min:10|numeric',
                    'sex' => 'required',
                    'status' => 'required',
                ];
                break;
            case 'PUT':
                return [
                    'name' => 'required|max:255',
                    'email' => 'required|max:255',
                    'date_of_birth' => 'required',
                    'phone_number' => 'required|min:10|numeric',
                    'date_of_birth' => 'required',
                    'sex' => 'required',
                    'status' => 'required',
                ];
        }

    }
}
