<?php

namespace App\Http\Requests\Booktour;

use Illuminate\Foundation\Http\FormRequest;

class BooktourDetailAddRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'Start'=> 'required',
            'PaymentID' => 'required',
        ];
    }
    public function messages()
    {
        return [
            'Start.required'=> 'Ngày không được để trống',
            'PaymentID.required' => 'Phương thức thanh toán không được để trống',
        ];
    }
}
