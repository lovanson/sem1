<?php

namespace App\Http\Requests\Guide;

use Illuminate\Foundation\Http\FormRequest;

class GuideAddRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'Name'=> 'required|unique:guides',
            'DoB'=> 'required|date|date_format:Y-m-d',
            'Email'=> 'required|email',
            'Phone'=> 'required|min:10|numeric',
            'Contactmethod'=> 'required',

        ];
    }
    public function messages()
    {
        return [
            'Name.required'=> 'Tên không được để trống !',
            'Name.unique'=> 'Tên không được trùng nhau !',
            'DoB.unique'=> 'lNgày sinh không được để trống !',
            'DoB.date'=> 'Sai định dang  !',
            'DoB.date_format'=> 'Sai định dang . !',
            'Email.email'=> 'Sai định dang . !',
            'Phone.min'=> 'Sai định dang . !',
            'Phone.numeric'=> 'Sai định dang . !',
            'Contactmethod.required'=> 'Không được để trống nha!',
            'Email.required'=> 'Không được để trống  !',
            'Phone.required'=> 'Không được để trống  !',

        ];
    }
}
