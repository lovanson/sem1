<?php

namespace App\Http\Requests\Tour;

use Illuminate\Foundation\Http\FormRequest;

class TourAddRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [

            'Name'=> 'required|unique:tours',
            'CatID'=> 'required',
            'TagID'=> 'required',
            'Duration'=> 'required',
            'Priceadult'=> 'required',
            'Pricechildren'=> 'required',
            'Content'=> 'required',
            'Piority'=> 'required',
            'Title'=> 'required',
            'MaxGuest'=> 'required',
            'Pricechild'=> 'required',


        ];
    }
    public function messages()
    {
        return [
            'Name.required'=> 'Tên không được để trống !',
            'CatID.required'=> 'không được để trống !',
            'TagID.required'=> 'Bạn phải chọn các trường  !',
            'Name.unique'=> 'Tên không được trùng nhau !',
            'Duration.required'=> 'không được để trống !',
            'Priceadult.required'=> 'không được để trống !',
            'Pricechildren.required'=> 'không được để trống !',
            'Content.required'=> 'không được để trống !',
            'Piority.required'=> 'không được để trống !',
            'Title.required'=> 'không được để trống !',
            'MaxGuest.required'=> 'không được để trống !',
            'Pricechild.required'=> 'không được để trống !',


        ];
    }
}
