<?php

namespace App\Http\Requests\Payment;

use Illuminate\Foundation\Http\FormRequest;

class PaymentAddRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'Payment_type'=> 'required|unique:payment_methods',

        ];
    }
    public function messages()
    {
        return [
            'Payment_type.required'=> 'Phương thức thanh toán không được để trống !',
            'Payment_type.unique'=> 'Phương thức thanh toán không đước trùng nhau !',
        ];
    }
}
