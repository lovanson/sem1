<?php

namespace App\Http\Requests\Blog;

use Illuminate\Foundation\Http\FormRequest;

class BlogAddRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'Content'=> 'required|unique:blogs',
            'Creator' => 'required',
            'TagID' => 'required',
            'BlogID' => 'required',
            'Image'=>'mimes:jpeg,jpg,png,gif'
        ];
    }
    public function messages()
    {
        return [
            'Content.required'=> 'Tên không được để trống',
            'Creator.required'=> 'Tên không được để trống',
            'TagID.required' => 'Từ khóa không được để trống',
            'BlogID.required' => 'Từ khóa không được để trống',
            'Image.mimes'=> 'không đúng định dạng !'
        ];
    }
}
