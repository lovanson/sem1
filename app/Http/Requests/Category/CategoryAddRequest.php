<?php

namespace App\Http\Requests\Category;

use Illuminate\Foundation\Http\FormRequest;

class CategoryAddRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'CatName'=> 'required|unique:categories',
            'Keyword_Sell' => 'required',
        ];
    }
    public function messages()
    {
        return [
            'CatName.required'=> 'Tên không được để trống',
            'CatName.unique'=> 'Tên không được trùng nhau',
            'Keyword_Sell.required' => 'Từ khóa không được để trống',
        ];
    }
}
