<?php

namespace App\Http\Requests\Tag;

use Illuminate\Foundation\Http\FormRequest;

class TagAddRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

           return [
            'Name'=> 'required|unique:tags',

        ];
    }
    public function messages()
    {
        return [
            'Name.required'=> 'Tên không được để trống',
            'Name.unique'=> 'Tên không được để trùng nhau',

        ];
    }
}
