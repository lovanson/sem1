<?php

namespace App\Http\Middleware;

use Auth;
use Closure;
use Illuminate\Auth\Middleware\Authenticate as Middleware;

class Authenticate extends Middleware
{
    /**
     * Get the path the user should be redirected to when they are not authenticated.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return string|null
     */
    protected function redirectTo($request)
    {
        if (!$request->expectsJson()) {
            return route('admin.getlogin');
        }
    }
    public function handle($request, Closure $next)
    {
        if (Auth::check()) {
            $error = array(
                'message' => 'Bạn không thể truy cập trang!',
                'alert-type' => 'error',
            );
            $customer = Auth::user();
            if ($customer->status == 1 && $customer->level == 1) {

                $route = $request->route()->getName();
                if ($customer->cant($route)) {

                    return redirect()->route('admin.error', ['code' => 403])->with($error);
                }
                return $next($request);
            } else {
                Auth::logout();
                return redirect()->route('admin.getlogin')->with($error);
            }
        }

        return redirect()->route('admin.getlogin');

    }
}
