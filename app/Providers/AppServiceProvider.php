<?php

namespace App\Providers;

use App\Models\Category;
use App\Models\Blog;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        view()->composer('User.layout.main', function ($view) {
            $address = Category::all();
            $blog = Blog::all();
            $view->with([
                'address'=>$address,
                'blog'=>$blog,
            ] );
        });
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
