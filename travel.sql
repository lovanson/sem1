-- phpMyAdmin SQL Dump
-- version 5.0.1
-- https://www.phpmyadmin.net/
--
-- Máy chủ: 127.0.0.1
-- Thời gian đã tạo: Th8 05, 2020 lúc 06:56 PM
-- Phiên bản máy phục vụ: 10.4.11-MariaDB
-- Phiên bản PHP: 7.4.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Cơ sở dữ liệu: `travel`
--

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `blogs`
--

CREATE TABLE `blogs` (
  `id` int(10) UNSIGNED NOT NULL,
  `TagID` int(10) UNSIGNED NOT NULL,
  `Image` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `Content` text COLLATE utf8_unicode_ci NOT NULL,
  `Creator` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `baner_top` text COLLATE utf8_unicode_ci NOT NULL,
  `Contents` text COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `blogs`
--

INSERT INTO `blogs` (`id`, `TagID`, `Image`, `Content`, `Creator`, `created_at`, `updated_at`, `baner_top`, `Contents`) VALUES
(2, 3, 'blog/baner-top/6.png', 'Châu á', 'Lv sơn', '2020-07-20 08:37:42', '2020-07-27 07:20:14', 'blog/baner-top/ds-1.png', '<h2>Prepare for a trip</h2>\r\n<p><img class=\"aligncenter size-full wp-image-22904\" src=\"https://tripandtravelblog.com/wp-content/uploads/2020/07/seoul-skyline-P32PQSP.jpg\" sizes=\"(max-width: 800px) 100vw, 800px\" srcset=\"https://tripandtravelblog.com/wp-content/uploads/2020/07/seoul-skyline-P32PQSP.jpg 800w, https://tripandtravelblog.com/wp-content/uploads/2020/07/seoul-skyline-P32PQSP-300x200.jpg 300w, https://tripandtravelblog.com/wp-content/uploads/2020/07/seoul-skyline-P32PQSP-768x512.jpg 768w, https://tripandtravelblog.com/wp-content/uploads/2020/07/seoul-skyline-P32PQSP-696x464.jpg 696w, https://tripandtravelblog.com/wp-content/uploads/2020/07/seoul-skyline-P32PQSP-630x420.jpg 630w\" alt=\"Seoul Skyline\" width=\"800\" height=\"533\" /></p>\r\n<p>Creating a list of top places you would like to visit is always a good idea when planning your trip. Search for the best sightseeings available online, explore the photos and the reviews of other tourists to choose the best places you would like to visit. It is important to follow this rule especially when you are not planning to stay in the city for a long time. A couple of days is surely not enough to understand the city atmosphere. However, you will surely have an opportunity to take a look at the most brilliant sightseeings throughout the city.</p>\r\n<p> </p>\r\n<h2>Make a list</h2>\r\n<p> </p>\r\n<h2>Plan vacation for spring or autumn</h2>\r\n<p><img class=\"aligncenter size-large wp-image-22905\" src=\"https://tripandtravelblog.com/wp-content/uploads/2020/07/yeouido-park-in-seoul-korea-RLVJMKF-1024x683.jpg\" sizes=\"(max-width: 696px) 100vw, 696px\" srcset=\"https://tripandtravelblog.com/wp-content/uploads/2020/07/yeouido-park-in-seoul-korea-RLVJMKF-1024x683.jpg 1024w, https://tripandtravelblog.com/wp-content/uploads/2020/07/yeouido-park-in-seoul-korea-RLVJMKF-300x200.jpg 300w, https://tripandtravelblog.com/wp-content/uploads/2020/07/yeouido-park-in-seoul-korea-RLVJMKF-768x512.jpg 768w, https://tripandtravelblog.com/wp-content/uploads/2020/07/yeouido-park-in-seoul-korea-RLVJMKF-1536x1024.jpg 1536w, https://tripandtravelblog.com/wp-content/uploads/2020/07/yeouido-park-in-seoul-korea-RLVJMKF-2048x1365.jpg 2048w, https://tripandtravelblog.com/wp-content/uploads/2020/07/yeouido-park-in-seoul-korea-RLVJMKF-696x464.jpg 696w, https://tripandtravelblog.com/wp-content/uploads/2020/07/yeouido-park-in-seoul-korea-RLVJMKF-1068x712.jpg 1068w, https://tripandtravelblog.com/wp-content/uploads/2020/07/yeouido-park-in-seoul-korea-RLVJMKF-630x420.jpg 630w\" alt=\"Yeouido Park in Seoul, Korea\" width=\"696\" height=\"464\" /></p>\r\n<p>Fall and spring are the best seasons to visit Seoul. The fact is that summers are very hot and wet, while winter days are often very nasty. In case you want to enjoy the city, it is better to plan a trip from March to May or from September to October. These periods are considered to be the best ones for traveling to Seoul.</p>\r\n<div id=\"quads-ad2\" class=\"quads-location quads-ad2\"> </div>\r\n<h2>Purchase or rent a portable Wi-Fi gadget</h2>\r\n<p>Unfortunately, Wi-Fi is not available in every location in Seoul. Therefore, having a personal Wi-Fi spot will be a great solution for your needs. You will be able to keep in touch with all the family and friends, make posts in social media, and share your location with all your mates anytime and everywhere. By the way, renting a portable gadget is much cheaper than purchasing a local sim card. Therefore, you can save money for something more interesting.</p>\r\n<h2>Purchase an integrated palace ticket</h2>\r\n<p><img class=\"aligncenter size-full wp-image-22903\" src=\"https://tripandtravelblog.com/wp-content/uploads/2020/07/exterior-view-of-gyeonghoeru-pavilion-and-lake-in-6E3THJF.jpg\" sizes=\"(max-width: 800px) 100vw, 800px\" srcset=\"https://tripandtravelblog.com/wp-content/uploads/2020/07/exterior-view-of-gyeonghoeru-pavilion-and-lake-in-6E3THJF.jpg 800w, https://tripandtravelblog.com/wp-content/uploads/2020/07/exterior-view-of-gyeonghoeru-pavilion-and-lake-in-6E3THJF-300x200.jpg 300w, https://tripandtravelblog.com/wp-content/uploads/2020/07/exterior-view-of-gyeonghoeru-pavilion-and-lake-in-6E3THJF-768x512.jpg 768w, https://tripandtravelblog.com/wp-content/uploads/2020/07/exterior-view-of-gyeonghoeru-pavilion-and-lake-in-6E3THJF-696x464.jpg 696w, https://tripandtravelblog.com/wp-content/uploads/2020/07/exterior-view-of-gyeonghoeru-pavilion-and-lake-in-6E3THJF-630x420.jpg 630w\" alt=\"Exterior view of Gyeonghoeru Pavilion and lake in Gyeongbok Palace, Seoul, South Korea.\" width=\"800\" height=\"533\" /></p>\r\n<p>This purchase will also help you save a good sum of money in case you are planning to visit numerous tourist spots in the city. This way, you will get a pleasant discount for the entrance tickets to various palaces. By the way, the integrated ticket is valid a whopping three-month period after the date of purchase.</p>\r\n<p><img src=\"http://localhost:88/travel-master/upload/blog/baner-top/ds-1.png\" alt=\"\" width=\"400\" height=\"326\" /> </p>\r\n<p> </p>'),
(3, 3, 'blog/6.png', 'châu mĩ', 'lv anh', '2020-07-20 08:45:08', '2020-07-27 07:20:25', 'blog/baner-top/5.png', '<p> </p>\r\n<h2>Search for more discounts</h2>\r\n<p>There are lots of travel deals offered by numerous official Korean websites. In case you spend just a couple of minutes before the travel, you might find some exclusive promotional codes and discount passes to various interesting places. This includes shows, parks, museums, zoos, and even an airport transfer. Moreover, you can subscribe to some services and monitor the prices to get the most attractive prices.</p>\r\n<h2>Live time for history</h2>\r\n<p><img class=\"aligncenter size-full wp-image-22902\" src=\"https://tripandtravelblog.com/wp-content/uploads/2020/07/seoul-south-korea-P8DLM3J.jpg\" sizes=\"(max-width: 800px) 100vw, 800px\" srcset=\"https://tripandtravelblog.com/wp-content/uploads/2020/07/seoul-south-korea-P8DLM3J.jpg 800w, https://tripandtravelblog.com/wp-content/uploads/2020/07/seoul-south-korea-P8DLM3J-300x200.jpg 300w, https://tripandtravelblog.com/wp-content/uploads/2020/07/seoul-south-korea-P8DLM3J-768x512.jpg 768w, https://tripandtravelblog.com/wp-content/uploads/2020/07/seoul-south-korea-P8DLM3J-696x464.jpg 696w, https://tripandtravelblog.com/wp-content/uploads/2020/07/seoul-south-korea-P8DLM3J-630x420.jpg 630w\" alt=\"Seoul, South Korea\" width=\"800\" height=\"533\" /></p>\r\n<p>Most students prefer visiting numerous clubs, cafes, and explore the nightlife of Seoul. However, it is important to leave some time for investigating various historical places and museums, too. This will help you better understand the culture of locals, explore the Korean traditions, and learn plenty of inspiring facts. Take a look at the country’s past and feel a special atmosphere of Seoul by visiting its top historical places and museums.</p>\r\n<h2>Try local cuisine</h2>\r\n<p>Sticking to traditional hamburgers and sandwiches is not the best idea when it comes to visiting other countries. Try to get more experience with tasting the local dishes. Although you might not be a fan of Korean cuisine, it is definitely worth trying. The number of dishes available for both tourists and locals is simply amazing. However, consider choosing the reliable restaurants and avoid purchasing meals from suspicious and low-quality culinary spots or cafes. By the way, you can use various services with the reviews on different restaurants in the area. Lots of tourists share their feedback on visiting local cafes – it might appear to be helpful for you, too.</p>\r\n<p> </p>'),
(4, 1, 'blog/baner-top/5.png', 'name1', 'lv anh 1', '2020-07-20 09:03:06', '2020-07-22 07:19:45', 'blog/baner-top/5.png', '<p><img style=\"display: block; margin-left: auto; margin-right: auto;\" src=\"http://localhost:88/travel-master/upload/blog/baner-top/5.png\" alt=\"\" width=\"350\" height=\"262\" /></p>\r\n<p style=\"text-align: center;\">ok nha</p>'),
(5, 1, 'blog/4.png', 'châu âu', 'lv anh', '2020-07-22 07:19:28', '2020-07-22 07:19:28', 'blog/baner-top/ds-1.png', '<p><img src=\"http://localhost:88/travel-master/upload/blog/baner-top/5.png\" alt=\"\" /></p>');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `blog_images`
--

CREATE TABLE `blog_images` (
  `id` int(10) UNSIGNED NOT NULL,
  `Images` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `BlogID` int(10) UNSIGNED NOT NULL,
  `Contents` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `blog_images`
--

INSERT INTO `blog_images` (`id`, `Images`, `BlogID`, `Contents`, `created_at`, `updated_at`) VALUES
(2, '[\"http://localhost:88/travel-master/upload/blog/blogimage/bg-1%20(1).jpg\",\"http://localhost:88/travel-master/upload/blog/blogimage/bg-2%20(1).jpg\"]', 2, '<p><img src=\"http://localhost:88/travel-master/upload/blog/blogimage/bg-1%20(1).jpg\" alt=\"\" width=\"400\" height=\"400\" /></p>\r\n<p>ok nha</p>\r\n<p> </p>', '2020-07-20 08:37:42', '2020-07-20 08:37:42');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `categories`
--

CREATE TABLE `categories` (
  `id` int(10) UNSIGNED NOT NULL,
  `Keyword_Sell` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `CatName` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `Status` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `categories`
--

INSERT INTO `categories` (`id`, `Keyword_Sell`, `CatName`, `created_at`, `updated_at`, `Status`) VALUES
(1, 'châu á', 'châu á', '2020-07-18 15:31:24', '2020-07-24 01:41:23', 1),
(3, 'châu âu', 'châu âu', '2020-07-22 18:39:04', '2020-07-24 01:44:42', 1),
(9, 'mĩ la tinh', 'mĩ la tinh', '2020-07-24 01:38:52', '2020-07-24 01:44:47', 1);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `configs`
--

CREATE TABLE `configs` (
  `id` int(10) UNSIGNED NOT NULL,
  `email` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `phone_number` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `address` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `link_fb` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `contacts`
--

CREATE TABLE `contacts` (
  `id` int(10) UNSIGNED NOT NULL,
  `content` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `email` varchar(225) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `contacts`
--

INSERT INTO `contacts` (`id`, `content`, `created_at`, `updated_at`, `email`) VALUES
(1, 'aa', '2020-07-23 15:55:08', '2020-07-23 15:55:08', ''),
(3, 'gửi tử tran home', '2020-07-23 16:03:51', '2020-07-23 16:03:51', 'son@gmail.com'),
(4, ';', '2020-07-27 08:54:50', '2020-07-27 08:54:50', 'admin@gmail.com'),
(5, 'aaaa', '2020-07-29 01:10:48', '2020-07-29 01:10:48', 'losonmndb@gmail.com'),
(6, 'ok nha ban', '2020-08-03 02:26:54', '2020-08-03 02:26:54', 'son@gmail.com'),
(7, 'cảm  thấy ok nha', '2020-08-05 13:42:21', '2020-08-05 13:42:21', 'losonmndb@gmail.com');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `customers`
--

CREATE TABLE `customers` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `image` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `address` text COLLATE utf8_unicode_ci NOT NULL,
  `sex` tinyint(4) NOT NULL,
  `email_verified_at` int(11) DEFAULT NULL,
  `phone_number` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `date_of_birth` date NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` tinyint(4) NOT NULL,
  `level` tinyint(4) NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `customers`
--

INSERT INTO `customers` (`id`, `name`, `image`, `address`, `sex`, `email_verified_at`, `phone_number`, `date_of_birth`, `email`, `password`, `status`, `level`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'admin', '107224328_797919237279497_6400430658662130750_n.jpg', '', 0, 0, '', '0000-00-00', 'admin@gmail.com', '$2y$10$wXT.Eq7IcwUM.SXRgtG.busuNM5Ulg9Q8Zm.DrPHjz8s2kyM1qfQG', 1, 1, NULL, NULL, '2020-07-27 08:55:10'),
(2, 'Văn Sơn', '107224328_797919237279497_6400430658662130750_n.jpg', 'hà nội', 1, NULL, '0396890722', '2020-07-01', 'son@gmail.com', '$2y$10$okvMdWeFY7cYH45RKZh/xOMT4i56WdWKuSpUyP.A.ouC.syDuDoS.', 2, 2, NULL, '2020-07-18 15:08:38', '2020-07-31 07:47:56'),
(3, 'sonn', NULL, 'hoang hoa', 1, NULL, '0369895121', '2020-07-02', 'losonmndb@gmail.com', '$2y$10$lc10lAPbWeckutFoP.YLJOfrJ/SmY3I/TqLr2DwndNfdx6Gjt9Pq2', 2, 2, NULL, '2020-07-29 00:58:10', '2020-07-29 00:58:10'),
(5, 'vansoná', NULL, 'w123', 1, NULL, '0369895122', '2020-07-27', 'vvvv@gmail.com', '$2y$10$YwH1iMGJ4sgwZEvB8skJK.t./h6h/7hYBMQaMc3kNFgREw9qSImNi', 2, 2, NULL, '2020-08-03 16:32:36', '2020-08-03 16:32:36'),
(6, 'aio;c akcasn,c', NULL, 'asdpiohans ,caks', 1, NULL, '0235602312', '2020-07-27', 'aask@gmail.com', '$2y$10$2.EWcrRzOMbROxA4.p5rz.T.ZCwnt4LQ4u.nQTG9oEmkq1I0MPkZ.', 2, 2, NULL, '2020-08-05 05:34:42', '2020-08-05 05:34:42');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `customer_services`
--

CREATE TABLE `customer_services` (
  `id` int(10) UNSIGNED NOT NULL,
  `contact_id` int(10) UNSIGNED NOT NULL,
  `content` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `customer_services`
--

INSERT INTO `customer_services` (`id`, `contact_id`, `content`, `created_at`, `updated_at`) VALUES
(1, 5, 'eo ok nha cu', '2020-07-29 01:11:16', '2020-07-29 01:11:16'),
(2, 7, 'cảm ơn bạn đã tương tác với chúng tôi', '2020-08-05 13:42:51', '2020-08-05 13:42:51');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `connection` text COLLATE utf8_unicode_ci NOT NULL,
  `queue` text COLLATE utf8_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `galleries`
--

CREATE TABLE `galleries` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `img` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `images` text COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `galleries`
--

INSERT INTO `galleries` (`id`, `title`, `img`, `created_at`, `updated_at`, `images`) VALUES
(1, 'ảnh đẹp', 'Kawasaki-Ninja-H2R-dynamic-rear-quarter.jpg', '2020-07-20 16:14:18', '2020-07-20 16:14:18', '');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `guides`
--

CREATE TABLE `guides` (
  `id` int(10) UNSIGNED NOT NULL,
  `Name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `DoB` date NOT NULL,
  `Email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `Phone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `Contactmethod` text COLLATE utf8_unicode_ci NOT NULL,
  `Status` tinyint(1) NOT NULL DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `avata` text COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `guides`
--

INSERT INTO `guides` (`id`, `Name`, `DoB`, `Email`, `Phone`, `Contactmethod`, `Status`, `created_at`, `updated_at`, `avata`) VALUES
(2, 'Nguyễn Lâm.', '2020-07-11', 'loson2k1dienbien@gmail.com', '0396890722', '<p>Rất năng động và nhiệt huyết</p>', 1, '2020-07-19 08:05:30', '2020-08-05 14:28:03', 'guide/t-3.png'),
(3, 'sơn', '2020-07-27', 'tangminhdat293@gmail.com', '0396890733', '<p>biết nhiều</p>', 1, '2020-08-05 14:32:50', '2020-08-05 14:32:50', 'guide/t-1.png'),
(4, 'Tuấn Anh', '2020-07-28', 'aaa@gmail.com', '0396890311', '<p>Rất năng  động</p>', 1, '2020-08-05 14:38:48', '2020-08-05 14:38:48', 'guide/t-4.png'),
(5, 'Nhung', '2020-07-29', 'nhung@gmail.com', '0396890744', '<p>Nhệt huyết với nghề</p>', 1, '2020-08-05 14:39:49', '2020-08-05 14:39:49', 'guide/t-2.png');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2019_08_19_000000_create_failed_jobs_table', 1),
(3, '2020_06_22_075430_create_categories_table', 1),
(4, '2020_06_23_064439_create_tags_table', 1),
(5, '2020_06_23_065044_create_tours_table', 1),
(6, '2020_06_26_014338_create_customers_table', 1),
(7, '2020_06_26_014429_create_roles_table', 1),
(8, '2020_06_26_014912_create_galleries_table', 1),
(9, '2020_06_26_014932_create_contacts_table', 1),
(10, '2020_06_26_015045_create_role_assignments_table', 1),
(11, '2020_06_28_102450_create_customer_services_table', 1),
(12, '2020_07_02_022657_create_tour_images_table', 1),
(13, '2020_07_02_030644_create_ratings_table', 1),
(14, '2020_07_02_043001_create_payment_methods_table', 1),
(15, '2020_07_02_045637_create_orders_table', 1),
(16, '2020_07_02_075932_create_guides_table', 1),
(17, '2020_07_03_022643_create_order_guides_table', 1),
(18, '2020_07_03_022948_create_services_table', 1),
(19, '2020_07_03_024533_create_service_details_table', 1),
(20, '2020_07_03_025335_create_order_details_table', 1),
(21, '2020_07_03_031528_create_blogs_table', 1),
(22, '2020_07_06_023036_create_blog_images_table', 1),
(23, '2020_07_18_094126_create_configs_table', 1),
(24, '2020_07_21_140051_update_service_detail', 2);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `orders`
--

CREATE TABLE `orders` (
  `id` int(10) UNSIGNED NOT NULL,
  `UserID` int(10) UNSIGNED NOT NULL,
  `PaymentID` int(10) UNSIGNED NOT NULL,
  `Totalamount` double(8,2) NOT NULL,
  `Status` tinyint(1) NOT NULL DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `orders`
--

INSERT INTO `orders` (`id`, `UserID`, `PaymentID`, `Totalamount`, `Status`, `created_at`, `updated_at`) VALUES
(1, 1, 2, 200.00, 1, '2020-07-20 14:24:53', '2020-07-20 14:24:53'),
(2, 1, 2, 200.00, 1, '2020-07-20 14:40:15', '2020-07-20 14:40:15'),
(3, 1, 2, 200.00, 1, '2020-07-20 14:42:11', '2020-07-20 14:42:11'),
(4, 2, 2, 200.00, 1, '2020-07-21 01:23:10', '2020-07-21 01:23:10'),
(5, 2, 2, 540.00, 1, '2020-07-21 01:24:10', '2020-07-21 01:24:10'),
(6, 1, 2, 200.00, 1, '2020-07-22 08:12:45', '2020-07-22 08:12:45'),
(7, 1, 2, 200.00, 1, '2020-07-22 08:14:37', '2020-07-22 08:14:37'),
(8, 1, 2, 888.00, 1, '2020-07-23 01:39:11', '2020-07-23 01:39:11'),
(9, 2, 2, 200.00, 1, '2020-07-23 07:27:06', '2020-07-23 07:27:06'),
(10, 2, 2, 888.00, 1, '2020-07-23 07:27:34', '2020-07-23 07:27:34'),
(11, 2, 2, 1065.60, 1, '2020-07-23 07:29:15', '2020-07-23 07:29:15'),
(12, 2, 2, 1065.60, 1, '2020-07-23 07:29:54', '2020-07-23 07:29:54'),
(13, 2, 2, 888.00, 1, '2020-07-23 07:31:38', '2020-07-23 07:31:38'),
(14, 2, 2, 200.00, 1, '2020-07-23 07:33:09', '2020-07-23 07:33:09'),
(15, 2, 2, 200.00, 1, '2020-07-23 07:33:56', '2020-07-23 07:33:56'),
(16, 2, 2, 200.00, 1, '2020-07-23 07:35:56', '2020-07-23 07:35:56'),
(17, 2, 2, 888.00, 1, '2020-07-23 07:38:13', '2020-07-23 07:38:13'),
(18, 2, 2, 200.00, 1, '2020-07-23 07:40:09', '2020-07-23 07:40:09'),
(19, 2, 2, 888.00, 1, '2020-07-23 07:44:30', '2020-07-23 07:44:30'),
(20, 2, 2, 888.00, 1, '2020-07-23 14:55:15', '2020-07-23 14:55:15'),
(21, 1, 2, 888.00, 1, '2020-07-27 03:07:25', '2020-07-27 03:07:25'),
(22, 3, 2, 888.00, 1, '2020-07-29 00:59:26', '2020-07-29 00:59:26'),
(23, 3, 2, 200.00, 1, '2020-07-29 03:14:44', '2020-07-29 03:14:44'),
(24, 3, 2, 888.00, 1, '2020-07-29 03:15:19', '2020-07-29 03:15:19'),
(25, 3, 2, 888.00, 1, '2020-07-29 03:16:14', '2020-07-29 03:16:14');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `order_details`
--

CREATE TABLE `order_details` (
  `id` int(10) UNSIGNED NOT NULL,
  `TourID` int(10) UNSIGNED NOT NULL,
  `OrderID` int(10) UNSIGNED NOT NULL,
  `Destination` text COLLATE utf8_unicode_ci NOT NULL,
  `Start` date NOT NULL,
  `End` date NOT NULL,
  `guestchild` int(11) NOT NULL,
  `guestchildren` int(11) NOT NULL,
  `guestadult` int(11) NOT NULL,
  `TotalPrice` double(8,2) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `order_details`
--

INSERT INTO `order_details` (`id`, `TourID`, `OrderID`, `Destination`, `Start`, `End`, `guestchild`, `guestchildren`, `guestadult`, `TotalPrice`, `created_at`, `updated_at`) VALUES
(1, 6, 3, 'name1', '2020-07-22', '2020-08-11', 0, 0, 1, 200.00, '2020-07-20 14:42:11', '2020-07-20 14:42:11'),
(2, 5, 4, 'TPHCM', '2020-07-23', '1970-01-01', 0, 0, 1, 200.00, '2020-07-21 01:23:10', '2020-07-21 01:23:10'),
(3, 3, 5, 'Đà Nẵng', '2020-07-31', '1970-01-01', 1, 1, 2, 540.00, '2020-07-21 01:24:10', '2020-07-21 01:24:10'),
(4, 22, 6, 'anh duy1234s5a', '2020-07-24', '1970-01-01', 0, 0, 1, 200.00, '2020-07-22 08:12:45', '2020-07-22 08:12:45'),
(5, 22, 7, 'anh duy1234s5a', '2020-07-24', '1970-01-01', 0, 0, 1, 200.00, '2020-07-22 08:14:37', '2020-07-22 08:14:37'),
(6, 23, 8, 'đà lạt', '2020-07-25', '1970-01-01', 0, 0, 1, 888.00, '2020-07-23 01:39:11', '2020-07-23 01:39:11'),
(7, 22, 9, 'anh duy1234s5a', '2020-07-25', '1970-01-01', 0, 0, 1, 200.00, '2020-07-23 07:27:06', '2020-07-23 07:27:06'),
(8, 23, 10, 'đà lạt', '2020-07-25', '1970-01-01', 0, 0, 1, 888.00, '2020-07-23 07:27:34', '2020-07-23 07:27:34'),
(9, 23, 11, 'đà lạt', '2020-07-26', '1970-01-01', 1, 0, 1, 1065.60, '2020-07-23 07:29:15', '2020-07-23 07:29:15'),
(10, 23, 12, 'đà lạt', '2020-07-26', '1970-01-01', 1, 0, 1, 1065.60, '2020-07-23 07:29:54', '2020-07-23 07:29:54'),
(11, 23, 13, 'đà lạt', '2020-07-26', '1970-01-01', 0, 0, 1, 888.00, '2020-07-23 07:31:38', '2020-07-23 07:31:38'),
(12, 22, 14, 'anh duy1234s5a', '2020-07-25', '1970-01-01', 0, 0, 1, 200.00, '2020-07-23 07:33:09', '2020-07-23 07:33:09'),
(13, 22, 15, 'anh duy1234s5a', '2020-07-26', '1970-01-01', 0, 0, 1, 200.00, '2020-07-23 07:33:56', '2020-07-23 07:33:56'),
(14, 21, 16, 'anh duy1234s5', '2020-07-25', '1970-01-01', 0, 0, 1, 200.00, '2020-07-23 07:35:56', '2020-07-23 07:35:56'),
(15, 23, 17, 'đà lạt', '2020-07-26', '1970-01-01', 0, 0, 1, 888.00, '2020-07-23 07:38:13', '2020-07-23 07:38:13'),
(16, 22, 18, 'anh duy1234s5a', '2020-07-30', '1970-01-01', 0, 0, 1, 200.00, '2020-07-23 07:40:09', '2020-07-23 07:40:09'),
(17, 23, 19, 'đà lạt', '2020-07-30', '1970-01-01', 0, 0, 1, 888.00, '2020-07-23 07:44:30', '2020-07-23 07:44:30'),
(18, 23, 20, 'đà lạt', '2020-07-25', '1970-01-01', 0, 0, 1, 888.00, '2020-07-23 14:55:15', '2020-07-23 14:55:15'),
(19, 26, 21, 'châu áaa', '2020-07-29', '1970-01-01', 0, 0, 1, 888.00, '2020-07-27 03:07:25', '2020-07-27 03:07:25'),
(20, 26, 22, 'châu áaa', '2020-08-01', '1970-01-01', 0, 0, 1, 888.00, '2020-07-29 00:59:26', '2020-07-29 00:59:26'),
(21, 12, 23, 'name125', '2020-08-01', '1970-01-01', 0, 0, 1, 200.00, '2020-07-29 03:14:44', '2020-07-29 03:14:44'),
(22, 26, 24, 'châu áaa', '2020-08-01', '1970-01-01', 0, 0, 1, 888.00, '2020-07-29 03:15:19', '2020-07-29 03:15:19'),
(23, 26, 25, 'châu áaa', '2020-08-01', '1970-01-01', 0, 0, 1, 888.00, '2020-07-29 03:16:14', '2020-07-29 03:16:14');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `order_guides`
--

CREATE TABLE `order_guides` (
  `id` int(10) UNSIGNED NOT NULL,
  `OrderID` int(10) UNSIGNED NOT NULL,
  `GuideID` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `payment_methods`
--

CREATE TABLE `payment_methods` (
  `id` int(10) UNSIGNED NOT NULL,
  `Payment_type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `Status` tinyint(1) NOT NULL DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `payment_methods`
--

INSERT INTO `payment_methods` (`id`, `Payment_type`, `Status`, `created_at`, `updated_at`) VALUES
(2, 'online', 1, '2020-07-20 01:34:07', '2020-07-24 02:07:20');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `ratings`
--

CREATE TABLE `ratings` (
  `id` int(10) UNSIGNED NOT NULL,
  `TourID` int(10) UNSIGNED NOT NULL,
  `UserID` int(10) UNSIGNED NOT NULL,
  `NumberRating` double(8,2) DEFAULT NULL,
  `Content` text COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `ratings`
--

INSERT INTO `ratings` (`id`, `TourID`, `UserID`, `NumberRating`, `Content`) VALUES
(58, 26, 2, 1.00, 'ok mmm'),
(59, 26, 1, 4.00, 'ádasdasd'),
(60, 22, 2, 5.00, 'ok'),
(61, 28, 2, 4.00, 'aaa');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `roles`
--

CREATE TABLE `roles` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `permissions` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `roles`
--

INSERT INTO `roles` (`id`, `name`, `permissions`, `created_at`, `updated_at`) VALUES
(1, 'Super admin', '[\"admin.changestatus\",\"admin.getlogin\",\"admin.postlogin\",\"admin.error\",\"admin.customer.avatar\",\"admin.get_password\",\"admin.post_password\",\"admin.get_code\",\"admin.post_code\",\"admin.get_register\",\"admin.post_register\",\"admin.post_change_pasword\",\"admin.index\",\"admin.category.index\",\"admin.category.create\",\"admin.category.store\",\"admin.category.show\",\"admin.category.edit\",\"admin.category.update\",\"admin.category.destroy\",\"admin.tag.index\",\"admin.tag.create\",\"admin.tag.store\",\"admin.tag.show\",\"admin.tag.edit\",\"admin.tag.update\",\"admin.tag.destroy\",\"admin.tour.index\",\"admin.tour.create\",\"admin.tour.store\",\"admin.tour.show\",\"admin.tour.edit\",\"admin.tour.update\",\"admin.tour.destroy\",\"admin.customer.index\",\"admin.customer.create\",\"admin.customer.store\",\"admin.customer.show\",\"admin.customer.edit\",\"admin.customer.update\",\"admin.customer.destroy\",\"admin.role.index\",\"admin.role.create\",\"admin.role.store\",\"admin.role.show\",\"admin.role.edit\",\"admin.role.update\",\"admin.role.destroy\",\"admin.gallery.index\",\"admin.gallery.create\",\"admin.gallery.store\",\"admin.gallery.show\",\"admin.gallery.edit\",\"admin.gallery.update\",\"admin.gallery.destroy\",\"admin.role_assignment.index\",\"admin.role_assignment.create\",\"admin.role_assignment.store\",\"admin.role_assignment.show\",\"admin.role_assignment.edit\",\"admin.role_assignment.update\",\"admin.role_assignment.destroy\",\"admin.contact.index\",\"admin.contact.create\",\"admin.contact.store\",\"admin.contact.show\",\"admin.contact.edit\",\"admin.contact.update\",\"admin.contact.destroy\",\"admin.customer_service.index\",\"admin.customer_service.create\",\"admin.customer_service.store\",\"admin.customer_service.show\",\"admin.customer_service.edit\",\"admin.customer_service.update\",\"admin.customer_service.destroy\",\"admin.permission.index\",\"admin.permission.create\",\"admin.permission.store\",\"admin.permission.show\",\"admin.permission.edit\",\"admin.permission.update\",\"admin.permission.destroy\",\"admin.payment.index\",\"admin.payment.create\",\"admin.payment.store\",\"admin.payment.show\",\"admin.payment.edit\",\"admin.payment.update\",\"admin.payment.destroy\",\"admin.order.index\",\"admin.order.create\",\"admin.order.store\",\"admin.order.show\",\"admin.order.edit\",\"admin.order.update\",\"admin.order.destroy\",\"admin.service.index\",\"admin.service.create\",\"admin.service.store\",\"admin.service.show\",\"admin.service.edit\",\"admin.service.update\",\"admin.service.destroy\",\"admin.guide.index\",\"admin.guide.create\",\"admin.guide.store\",\"admin.guide.show\",\"admin.guide.edit\",\"admin.guide.update\",\"admin.guide.destroy\",\"admin.blog.index\",\"admin.blog.create\",\"admin.blog.store\",\"admin.blog.show\",\"admin.blog.edit\",\"admin.blog.update\",\"admin.blog.destroy\",\"admin.rating.index\",\"admin.rating.create\",\"admin.rating.store\",\"admin.rating.show\",\"admin.rating.edit\",\"admin.rating.update\",\"admin.rating.destroy\",\"admin.orderview\",\"admin.search_category\",\"admin.search_tag\",\"admin.search_payment\",\"admin.search_guide\",\"admin.search_rating\",\"admin.search_contact\",\"admin.search_tour\",\"admin.search_blog\",\"admin.search_gallery\",\"admin.search_customer\",\"admin.search_customer_service\",\"admin.search_role_assignment\",\"admin.seach_role\",\"admin.search_service\",\"admin.seach_order\",\"admin.seach_permission\",\"admin.getlogout\",\"admin.index\",\"admin.getlogin\",\"admin.postlogin\",\"admin.get_password\",\"admin.post_password\",\"admin.get_code\",\"admin.post_code\",\"admin.get_register\",\"admin.post_change_pasword\",\"admin.getlogout\",\"admin.post_register\"]', NULL, '2020-07-22 16:57:25');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `role_assignments`
--

CREATE TABLE `role_assignments` (
  `customer_id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `role_assignments`
--

INSERT INTO `role_assignments` (`customer_id`, `role_id`) VALUES
(1, 1);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `services`
--

CREATE TABLE `services` (
  `id` int(10) UNSIGNED NOT NULL,
  `Name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `Description` text COLLATE utf8_unicode_ci NOT NULL,
  `Price` double(8,2) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `Status` tinyint(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `services`
--

INSERT INTO `services` (`id`, `Name`, `Description`, `Price`, `created_at`, `updated_at`, `Status`) VALUES
(2, 'nước suối', '<p>nước tinh khiết từ thi&ecirc;n nhi&ecirc;n</p>', 10.00, '2020-07-18 17:06:53', '2020-07-24 02:21:09', 0),
(3, 'máy bay', '<p>ok</p>', 11.00, '2020-07-21 04:15:36', '2020-07-21 04:15:36', 1),
(4, 'ô tô', '<p>okok</p>', 13.00, '2020-07-21 04:15:54', '2020-07-21 04:15:54', 1),
(5, 'xe đạp', '<p>ok</p>', 33.00, '2020-07-21 04:16:14', '2020-07-21 04:16:14', 1),
(6, 'nhà nghỉ', '<p>ok</p>', 22.00, '2020-07-21 04:16:43', '2020-07-21 04:16:43', 1);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `service_details`
--

CREATE TABLE `service_details` (
  `id` int(10) UNSIGNED NOT NULL,
  `ServiceID` int(10) UNSIGNED NOT NULL,
  `TourID` int(10) UNSIGNED NOT NULL,
  `Quantity` int(11) DEFAULT NULL,
  `Totalamount` double(8,2) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `service_details`
--

INSERT INTO `service_details` (`id`, `ServiceID`, `TourID`, `Quantity`, `Totalamount`, `created_at`, `updated_at`) VALUES
(4, 2, 22, 1, 1.00, '2020-07-21 07:11:40', '2020-07-21 07:11:40'),
(5, 3, 22, 1, 1.00, '2020-07-21 07:11:40', '2020-07-21 07:11:40'),
(6, 4, 22, 1, 1.00, '2020-07-21 07:11:40', '2020-07-21 07:11:40'),
(7, 2, 23, 1, 1.00, '2020-07-22 18:42:20', '2020-07-22 18:42:20'),
(8, 3, 23, 1, 1.00, '2020-07-22 18:42:20', '2020-07-22 18:42:20'),
(9, 4, 23, 1, 1.00, '2020-07-22 18:42:20', '2020-07-22 18:42:20'),
(10, 6, 23, 1, 1.00, '2020-07-22 18:42:20', '2020-07-22 18:42:20'),
(11, 2, 25, 1, 1.00, '2020-07-24 10:49:02', '2020-07-24 10:49:02'),
(12, 3, 25, 1, 1.00, '2020-07-24 10:49:02', '2020-07-24 10:49:02'),
(13, 6, 25, 1, 1.00, '2020-07-24 10:49:02', '2020-07-24 10:49:02'),
(14, 2, 26, 1, 1.00, '2020-07-24 11:00:36', '2020-07-24 11:00:36'),
(15, 5, 26, 1, 1.00, '2020-07-24 11:00:36', '2020-07-24 11:00:36'),
(16, 6, 26, 1, 1.00, '2020-07-24 11:00:36', '2020-07-24 11:00:36'),
(17, 2, 28, 1, 1.00, '2020-07-24 11:15:47', '2020-07-24 11:15:47'),
(18, 3, 28, 1, 1.00, '2020-07-24 11:15:47', '2020-07-24 11:15:47');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `tags`
--

CREATE TABLE `tags` (
  `id` int(10) UNSIGNED NOT NULL,
  `Name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `Status` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `tags`
--

INSERT INTO `tags` (`id`, `Name`, `created_at`, `updated_at`, `Status`) VALUES
(1, 'châu á', '2020-07-18 17:16:41', '2020-07-24 01:57:26', 1),
(3, 'châu âu', '2020-07-18 17:19:09', '2020-07-24 01:56:57', 1),
(4, 'Mạo hiểm', '2020-07-22 18:27:02', '2020-07-24 01:57:11', 1),
(5, 'Phiêu lưu', '2020-07-22 18:27:28', '2020-07-24 01:57:16', 1),
(6, 'Trải nghiệm', '2020-07-22 18:27:43', '2020-07-24 01:57:22', 1);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `tours`
--

CREATE TABLE `tours` (
  `id` int(10) UNSIGNED NOT NULL,
  `CatID` int(10) UNSIGNED NOT NULL,
  `Name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `Duration` int(11) NOT NULL,
  `Pricechild` int(11) NOT NULL,
  `Priceadult` double(8,2) NOT NULL,
  `Pricechildren` double(8,2) NOT NULL,
  `Content` text COLLATE utf8_unicode_ci NOT NULL,
  `Title` text COLLATE utf8_unicode_ci NOT NULL,
  `MaxGuest` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `Status` tinyint(1) NOT NULL DEFAULT 0,
  `Piority` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `Image` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `baner` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `TagID` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `tours`
--

INSERT INTO `tours` (`id`, `CatID`, `Name`, `Duration`, `Pricechild`, `Priceadult`, `Pricechildren`, `Content`, `Title`, `MaxGuest`, `created_at`, `updated_at`, `Status`, `Piority`, `Image`, `baner`, `TagID`) VALUES
(3, 1, 'Đà Nẵng', 5, 45, 200.00, 100.00, '<p>qwel;k.,m scsac</p>', 'việt nam đẹp lắm', 20, '2020-07-19 14:09:55', '2020-07-19 14:09:55', 1, '1', 'tour/d-3.png', 'tour/banner/tr.png', 1),
(5, 1, 'TPHCM', 5, 45, 200.00, 100.00, '<p>qwel;k.,m scsac</p>', 'việt nam đẹp lắm', 20, '2020-07-19 14:11:05', '2020-07-19 14:11:05', 1, '1', 'tour/d-3.png', 'tour/banner/tr.png', 1),
(6, 1, 'hà nội', 20, 45, 200.00, 666.00, '<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Repellendus, saepe porro totam praesentium numquam laboriosam hic quos impedit. Vero eius consequatur aliquam totam corrupti quos ipsum nulla, dolorum? Dolorem, ipsam.</p>\r\n<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Repellendus, saepe porro totam praesentium numquam laboriosam hic quos impedit. Vero eius consequatur aliquam totam corrupti quos ipsum nulla, dolorum? Dolorem, ipsam.</p>\r\n<p><img style=\"width: 219px;\" src=\"data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAkGBwgHBgkIBwgKCgkLDRYPDQwMDRsUFRAWIB0iIiAdHx8kKDQsJCYxJx8fLT0tMTU3Ojo6Iys/RD84QzQ5OjcBCgoKDQwNGg8PGjclHyU3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3N//AABEIAHsA2wMBIgACEQEDEQH/xAAbAAADAQEBAQEAAAAAAAAAAAADBAUCBgEAB//EADwQAAIBAwMCBAQDBwIFBQAAAAECAwAEEQUSITFBEyJRcQZhgZEUMrEVI0KhwdHwM1JygpLh8RY0Q1Ni/8QAGQEAAwEBAQAAAAAAAAAAAAAAAAECAwQF/8QAIBEAAgIDAAMBAQEAAAAAAAAAAAECERIhMQNBURMiYf/aAAwDAQACEQMRAD8A6eOVWGUIPtRQc1ycN7NEco5HqOxqjba0RxNGCPVP7V6b8bXDz4+RPpdFaWp8ep20hwGI9xTaTREZEq/eodmiaYxgVsCgqw6549a0JVHelZWgwrXvQfGWtCVaWwoLxivc9KF4q1l7tEHHJo2PSGNpzXqjnripkl85ORxXi3hNPFk5IssFUDzZNZzUn8QxPWiJeFeppYseSKJNfZpRbwHqfvRDejH8NFMLQfdXoal1uIz1YCteKvbmlQw+41ktQjMo/iFeGQEdRQBstWd9YL1neKANF/SsliayWFfZpiPjWa9JrJosVGHoNEc0IsKqyWciqZ6VvYBilGvQeF4rJuTnlvtWuZhiUAcD0raSADkgipguF9QT6GtC5RuM/SjJDplVLjb0Yj5g0dNRkTgyA+m7moyyoRkSLj5mtLeQKcGQf9JNS3Er+iyNWYHlQfmKZj1Vf4kbFc4b6zBBLyYz2A/TNeSaooY+D4mwflLkAn6CpyiVtHVfj0ZRhgD7UBrtN3JJ9q5wam3qK3+05Ovhrj6/3pZIZ0QmR/4gPevDKQeuR8hUEahK2MLGD2HBrQ1GUY3GMc/IUZoKLn4sKcH+YoyXcZHJxXPi+M0ojyjOxwMEU+lpejIkg2qBkljjA9eaWaCmUzdQgcsK8FzGf46Wj0m+mbZFEjHGSA3Kj519+zr5QqrEhLHbtDZOanOJWMhk3EHdj9jRI54iPJJ9DShtjAga7nhiJ6I4IJpe7JthGzSwxhlztEoOR6+opfquDwfSqbpF6uDQX1CJTgb6hSX5lZUj2AjqwbKn+lagvJbsTeEttGYIzIyk4LAHBxnqeelL9EFMtrqMZ6P9xWvx6/7h71Ahu5XGTaIf/wBZ2mnLdokjZry3lQjkMmMfWn+iFiyj+0YwcbxRBqMXrn2NfNbwbQstsoVQGYjr26/ei3R8HdDp8JErYdkSUIFU5x+nrS/VPiL/ACkZS7LglImIA6+vtQrjUPBXMkZQnsxANba7uJrJxPb3USRDLSgdBjk549f51Fvbm0Nym83TYJ8TO0vwB0HuQOaS8n1DnH4Fm1wZOwgewzSZ1wf/AGD7GsqbK4M8U9lNHMM+CjO4DnPGcLxxTUejLsG5NHB9GM5P3q/2ivRn+U/pw5mZT/qLn55rMl3ubaMk4yMHvUyV3LYZmJFGto5ArTMjblyAADjHrU3oSih+S5ETBuS3YUOa8fAbv3IoUUFzcDYsTmT820Dnb2PtRBaSxKpvFdEbkbFy20nGfvkc0r+lYv0aNxJJErAZBJyBWlK7BINjZ9W6VuyggKSxDxmbooZgm0569/t39RT1tooMq20iqjysGDMGdtuM9B/bPFQ5oqPjkyakbylnGxWB6+lbYlVzIykdzxVaewmsrl3t57UxjB3TJlc9uSOCeOOKu2d5E0sa6i8IHI2LD5R2AAwd3uDgc0ZMr8zlba3vnSN0gl2y8Rvjh/kPWrc/w1e2ltJJO6nw4g8hI4DnpH8z7Ue+uduokWlvKsar0dvIo4O47eX59q3YSzvma+uheW9soVoZVZQoPA3duvT2x0rKU5ejSHjXsjLa3ItjcbSY1IVtik/zpcRzsQscEzFidpSMknv0+h+1XY70zaovgT2kUcuV8KJfIgz0Y7QD35+fWq1jpEt20uJ4LFSGDyIyPvDZGPKvAOf06Us2uifjV6OTsrSW4kjMiYQEMTLGSCAfcV11lNdraRRW10dsabolRyq8ZAHIyehAXNLTWotLhYGliuWztkhihPAByMk8D64HHQ103w9JELLctvANzEbCfEVcZwOeB17YFPNlLxJHP3us6nIslukSiWMAyNFKFaM9t2MfPg8dMVOXVviW+t8Qfi2hJB8oK5Gcde1dN8WXMNo8KxLsluGaNY4HEQfzY8xA6cgdR3qHefEUtvNNHfPOLglTmOYNgEcFQDgjgZGec1OTH+aOdY315cFb2aZlRsSOwMhU846nOOKbg+F738JJfXc5t4BymRkyc44Hb6+tWLS8gvgLm7iitrfbhSiBZXcdiSM9eBg85+lXLKdvBlttR0x5Vk3IrIxLReUA+IzDjrxn0NVnRP5L2cROfAsRbQSOE8ZnbeMbuyjr1Az0FK2kNxLLHJbRuZI+Ulj8g+9N2X4XUr4NLGzgjcR1DD1J+3HyNdQixKP3YUe1XGaZGAnY29wgEl3KZZMcDrt+ppW7u7x7xMxlLQTbNwHEnIzkntzin70yfhZfA/1Nh21Bt9bhFslnew3TiEeVYSBuwc+bPXkUTkloEvp3dx4Rvpl7lRt6dM9j68dKU02+mMGZIWLFgXjMm3g8Enjrx9hUWD4mtGMZaWdARzG8e5g2P58/52pq2122lE6i4mWcMWcyRgZJ/hHr9T9MVmq9GuSZfeW6uC8DiOS3ChCqyYHHy79f8zUbVTZEo+lWcEt5L51eRMBT6jrkj6dRQrDUJp9RhtoHa4gkATyRnynOOeSPfv8AeonxVfyhLBpJ44FcuyCMuVaPftzx0ziiglJUN219dNrttZ3EPhXEUb7sOVVRtIOB0OThfSq1yH8dvBtN8fRWCg5/lXI6h8TSXN7FdeBbsyQ+CrxqwbbkkDrwOnuRT0fx/d26LDBp9kY0AVS0zAn3561aiZ5UiLG1pb73Y29qMDwT4WGcHqSeCw7ZNUtMbStSEgm1B7aUzbE8eLezrgYIbgDqwOT2qJCPxFzFGVM0ccYfykhtvZQzdvah3DB9XjgmtIxI3nMYl8QBfQjg+vf0pNsuKo6dIJ7SC4huBFibDxSWqiUsq8dVJ2jGTz/SvJNLmYQyWrM7SSIplbzopJ5Vj8hg4Hr7VnTZ47TFqyxYUgwTTL4ToR2TGMcA9+OTmg3mp3Ntq11PqN5JLBdqRvTBUKMgBsHDcE9e2Dk0rLocvbaK7vrqONIzPbbv3EUGFHPZQcHBHp35oeb/ADb20ZRo4G3MiEqcNjI3/m749OelSb+/htJVTTEJlZN7Oucj1HA7c/btVW71uQpFb2cFyGJ8ySrsAJGATnrwob796LoGlehNJbq1ut6qEjY+RVXcCvI+vvVRrC8sA7Ty+C8YDKMqhbd1Pfjnn2POKNBpt3Fcl7uUCxCjfFJID4q4G4rz5TgnGB2HJq/ouo/DOoWklukUs8MfEomjdnXuGyeQPrQpCkkiVD8Ja5eYKLaKqSkqWwfEx2yP86VKMz248LUCrYijikRnKu4XjcGHU9yP/Fe22ra7puoW9vZOYYw5G8xbYM7jjnhcbSMn3qjpUMN3fXI1a6tUmOGEvIWInOF3dPMT147daG0KKb2JXCRWGnx3E2LmUhcWsFtgq2eP4sY46HnnNPW1r+Lhhgitssyo8iwRspOTyGZumAD6dOPkpY21o1yzXBu5JMmVWRSUcLxlQCd3IGDjkVdsdK0+eETxSzGR1VCXPhGc9wQfy49Dmods1SS6StbTS7fXUskvJrSBArNbpGyorE8gjoCVIOTn3qj/AOoNEsFaLRopLu4mn80W4oC5yDyc9xjj1rn9R0CHT3N9FeXDxp1abyNsGdyjJy2Bxmt313ZTS6Xb6VO6zx3PhxPcBcjeRgseR1I5Hv8AOh6I2b+KRea8sTXkMdiLcOZUmlwjAkH7DGTT1ho+kRE30d4kp8IeAJJN4DDqQw+mP8FOfE+kaPYWBkmmuWSCN0jVmZgHbd5Vbv5hyOetc7Z/GQ0/4dtrCC2ijeCMI7B96Scck46Nk5wM+/rNsf8AKZ1kuk6To8KX2s3Mk8uVbxl3eQNjaMD9cfOua1fXYZ7qZdLgSOBsgySS7TKR02j5j271LW9v7tIUkKvDgKYpsspXBwQD04JFOFY4B4twVadhtjQ8kemamU0FMKYE0+3kuUj8OWYbUj3ZAOOnt39hTGkl47MiVw2WO3jBx0/pUS4Nz+JW35knfnfnIiX/ADFVYwIoljRicdSepPr+tPxJ3bJf+Dcko9ak3y2cSyyPGMYzgZGT9KaPfNTtWVHtJVI5xuB9ua6tNUZtC7rGkEXjMFYjDLFt6c4Az36DJPHpTFlLm98OKwWzS4iIZ/F3YODxz3zjik7eVZ7dckI6rv3HJx7/AFNbe5U6PI0z+HPGSjsXPfofl/2qY042KWmXtMOoWV/HdyThrC1tnnciUN5ljYjI6jzEda5/4wtTJqltakuYrOxgtztONj4LEjt1YikbDW5GmZpkUxiPwpwFI3Lg55A6EkcfL5VubWZ7+C6u7tkMkrYXd3Hy49TQui9BfgjS2vZp7oy2WyJ/C/fSgAYAJOO/BAzXRXX7Rt5vCWbTo1VV2ptWTAwMeYR88VL+DEnjsMx3F9GkzkrHakhI+SCeVIPbuMYpC5hP4iT97fudxyzWbZJ79GxVUmKMl9Frae0hc3aNIxCMzpE5Bxnr7cdOOopJ7hZ9XWezdiwQKrvkkADIxj3oUOoRta3UtnZqPDtdjN+RhuYYOM9K+tY45Vs0EsexJFMgA5jPcn5cnvUUa5XoqSRzy3RRplWHYfCkjwPFA68jAzz2Pb1pqy+HrjUri4jsPFnt5ERomUHbCxIwDk9B88Gp+owr+00mE2+OWLauZVOAc5OB0Oe3z5q1omu6jpGkLeWlwsU14gURyR4GxT5QN3VvN1HapfNFLtDWrJcfCo0w6rCkShX2yIxLsQMbG+R3Doe9V7VdLu9JMqwySCaLKKkapjcMLjdkn0Hr2rmNWvJdVgtdR1+SUMJCngSxnZvwNrdM9yD7fKhW12sqfs6zeQs2Azxuy5A7gEcDPI71LkkilG9WOaFM1jO+hain46+Ub7fc5IZQOU69RtPtTcigSvcbrixlDMhis+hAGPMSAAPXnnjnFTr+906CwuLadJo9YAiaG4yS6tuBbzH1Axn51W0T4is7rRrbTNQDh02x7yxBI3AA/Pqeenc+tNCkvR5+1ZY9OSyMixRheOdx8Q/xAdAeSOOtbEVndCZLqzaTxF/NnDdsebAI6fLr9zapaW+mq8it4+xwm5AWJVhuVuO2OvtQLVjemU2IV47Z97mVR+XnB29xz1POQelU8URFeSToMmq6tdXEq2EKJZwFUmKLtaNRkDzDJbODz3xiqNlfaaAGsr/8Q1s0jhGOdpGVJKtzyD0/U0tGbyUyJa+H4kqRo80h2ouzJX5dSe/eps9m9lbmRDCRJIN4OcEsQCzMDxjknOfask1ejtficU8nR58YahFqCyyabdObdoTFJDGchGPUcj0B6YOR86j6FaTTSxSlFS33BucsTg8ZHbOe+Kt31naXEkNt+KEtlvYzyRx+YS9VXI6gknPHpmtWrtaozTmONIgRv6CNM9f+I4H2qpNUcyjexy/iS4MxvBI4nbxH85IVwMA47dfT+lTYtJtoLVljtmmZm37du58Dp7fQVP1X4kfUJDbWSrHbluegL9skkcVm01XUYG808hRV2gcNjAwO3p61g50tg5JMaF2iSXAt42EmPPIy4x8gD6fOsuyNC+85bOAAASc8Dj60c6xIbVd8KzStMQEbgNGFznkEZzx2prT47C+mRI4PAuSCxBYgHAycdRnr2FLFT4wbbE7GD8Mm9zumI8x9PkKa8TFb/DpKpe3uFdP93Ufdc/zxS80E8ecxkj1XkD3x0roj8FVHslwAOtIzzbuDyDWJJM560pLLz1raJnMVswYrohpG2Djb6iq2q2MctoAzEiZcgBj+cdB/gqHk/juCDkdDXQANcacq4YmNwQF646f1rLjaQ1TQP4O1RLq1Omzq0T26eVonEaFR/uyeTXnxbHai0hkjtPDiZ/NsREC59So5P86HoRaHUb+SKdoY/IpDRDOD14NKfF9xFPpmRcRyHxQcCPa/5W59qtcsl/BnQImms3FpcXYgVv8AQWbgjPJxkcnr9BUT4g1C6s9Yube1QNCjAIQCmRgfwliR96V0m7ltLkSRnzBcMQeD9KDfTx3N3JNcPJ4rnLdP7UrsV0tFbSdKtNTunmWWEIT4Ss77cNxx0OTj0z/cWr2Udl8Ow3FrMDK0zLLscZPPlyDzgjHbFM6hf28OnQzaaLWJlwpWOPcCcDJw4I7duKjXF699dLOLaNZ28qrCuC3cEjoT15qbNnSWhrTow13bxXTb4pGBmYkKWBIB9u1d1dz/AA3pkEVtJY6jNCrBDMXAZc8AexyK4L4fvptN12Ge9thP4IkDQgjJJBGMjPQ88c/Ouw1f4kF/pkUN1YQKjky7CoY7h+UDI5wB9+xpU0wjzXS1e21pe/DF9NplnarFFGzYkYyOx2nnI43HB79q/KtFklsLm3vFVjdRsWQHqQeMY9s/er1xqV1ZXFvZQSNBHLGm9hGCrRkHpkdc7hn0qZJL4ME8e1Gbxh4YfG7aeT0PqoIPr6U60KTWVj+uSXeo2f7XlgERtZTaukaEFVwGBPqfMQPmD8qZtdMSaFhYyI1w6fu925mkU4PTHl70tYazFFrLyvJJHbyws0kLjdvbnA+R5xn0GO9EfXpruG0s7ZZkiKFQkLbFI7Buucc/ypquCbp2i1aalY3ul2tzbl7K6jTwJ44uQXHG454weDiugivNOsdMMei6fK1/OFC3LKqgkEHauOo4I4A45xXE6Qs1nHcWlwhginKPtBO7IBGRnB5Hf9adm1G1hfEpcu/JXfyPpnNR5IJmif06GS5vJnaAiKGRQqOoJkYY44yMdz2+vel5ntVDfi5y+DkmZjjrk9/Xtn6VzE2tSssgtgI1yAN6449gf1zSW2a7bbLNI285DyE7V9lFS2kDmdFffEMMYEOnx7gBgFTtTp1z3qQ7Xl/+8mfxFBBIA8o+n+GhLbmEB7idDjoGOCfpTVrrMykC3hErhtu5hkIPUD+tZNt8J6O6bpVpDie5dnX8xVeB9T6Vp78yTFbC2VSvBZjuH3P6Ut4Fxet4l4+N3JQetPxxrEgRAAo6AVnhe5FKAuIGAUyPlhW4bqS2n8WE7ZADhsA9Rg/yNbkNLSEVtFfB8BK/hkGMlSOhHBFMRapcoylmEqjpuHP/AFDn+dJSgH5H1oQJHOa1TZLOh1O5tFW1E6SZmt1nO8bgobtuGG7elTJrWGaMvby856Dzge+OR9qmyyknLEk4xyc0s7dwcP2PpWsZGUkfahY3Yk8SLzAdGjG7+Q5qvosjS6TIJiGO45PapWp6rMt8XtJEkgEaKI5BhshQGOeoyc1RsryS4tBOybFZipD+Ycdckc96lYttiWh7QrnUpfxUmkM7OrLvzsJK4OAC4POTUf4ikubqOVruMQS7gyo9sqM+TjAYff6U/Es2jMAiTWykgrxgE+vz9Km6jAt5cvcyXDtI2CcgnPI+3GenenTqkDXshWrGONlAHDd6RmLmVirnGeKvNYJKjyL1Xj3FCl0qHxG2g4z61DVMEmx3Tfhu61GO2unulK3UjBArE469gCexHvjOKV0y1lW9MbweKkcjkBsjfjGDjrjqc0aC9uLGCeGwEyK7lTDHIQijuO2enWhTvd6hciRbiOzRRkbpsEY+Yq2lwq7EZL5EX8KtvEI36spO7d1/7V4099O6WloWbnasY6EjmrC6dauTIrFlX/5FGAM9eTRluLSw/wBCJZS3LuoD4P6ZoXBbsQgTVrizjGJJoCw2k53Z/oBWY/h2TxxNeXUcKjldmXfPbIFUZ9ZYpiOMFsceK2QP+UY/WkJdQnuGEJmZz0KxkIoz7e9Z3Q9FB7KytLd9oDTMwDNI2DnOcADpmvW1GdWeJF8NBuwoGxe5PzNJWsarIIjIiE8EquQO3WnbmOyUYDiWZhySTgHsQe3ellXC6smtqlzcOd0zOQRuAHX37ntWooJQmeAp55PT6CqKWcgYrAqxxY6bcbj60a30oLHtmlaTJJOazlMFCxCGaKBh4jAgMo27MnP6DNMETzk+BC0cTH+LjjH+dKpx2kMRyFGfXHNbxUWaKBLi0lcgzuWPcDoapwQxwjEaBR8q0Bivc4FOx4hAcCvmcCgNJigvNQMPJIKUklock1LNJk1rFGbYZnzWSwoO+vi1WQfPg0vKBRHbApeUkhuQOOMmnwli1w0ch8oAA9fWr3wjzbXEsgyhO1QemRXNiGa6nS0jA3StgMei57nFdTdGPS9MEEfG3Ea4Pf1qY8sSR4+oACSAvuSN2Ybnz1Pz9z9qjW+rOLspEuEbIwR0+dLzp4cSBeS5yWJ60fTIHcsC0YwB5XOD9PWrjJhLZTjx+Hcno7BV+lClt1lkMhPLc8GmHSQwxssUphQ4Mu07Nx7bsYz8qG8S7ji4VQOME1En/TLitE8XNy0am5BQuCVCjAz0/wAFJwWyxyFy2055zgY/rTl9PLhofEbw42wig9Oal3HlmAXAGe1TnZPCgjrJJ55PECH+NsAcdgc/YViS+fcUiG9cYXjK0SzUFVGBhsg44zyKcPmlANF30rFkt4XLfuwC57AcoPn86oWlggiCSAjnLcYyef701FGgBwoo6AUmWooGlsm3aqqo9QOT7mjxWsKOGVMkcjPrW16UVahtlpBFGK2DWBXorNso0TWCRXxoZoQGi1CeTFZc0vITVJCbNvLQXlobmgsa1SM2zTyZoZastWBVkBd1eF8UOskmmI+kmGSucYGaVkaRyqQgyO3AA6n5Vi8dgRg96ufD8Ua6aJwo8Vur96XXRPWFsYV0y1eS42NO3Ltj8vyzSEtw2pSjIxFH3/zvWddlf9yu7hidw9aPKixacvhjbu647032hrogVaWYNIDtJ6HuKpW64Oeg69OlYt//AGee9MQgFVBGQXQEf8wql7B9NIxmbxHZtkfCgnOPb9agTzy3EzyxvsRjlVx27VW1Zilg6qcK2AQPQtg/pURmYMwBwASBXO21st6P/9k=\" alt=\"\" /></p>', 'việt nam đẹp lắm', 20, '2020-07-20 03:29:24', '2020-07-20 16:52:01', 1, '1', 'tour/d-4.png', 'tour/banner/tr.png', 1),
(23, 3, 'đà lạt', 5, 444, 888.00, 666.00, '<p><strong>Ngày 01: HÀ NỘI – NHA TRANG (Ăn trưa, chiều)</strong><br />05h45 Xe và HDV đón du khách tại văn phòng lữ hành <strong>Saigontourist Hoàn Kiếm - 55B Phan Chu Trinh</strong> tiễn sân bay Nội Bài đáp chuyến bay  đi Nha Trang <strong>VJ773 (Dự kiến cất cánh 07:55 – 09:45</strong>).Đến <strong>Sân bay Cam Ranh</strong>, xe đón Quý khách đưa đi thăm viếng <strong>chùa Long Sơn,</strong> tham quan &amp; chiêm bái <strong>tượng phật A Di Đà </strong>(cao nhất Việt Nam - <strong>44 m</strong>).<em> </em>Sau đó xe đưa Quý khách đi ăn trưa<em>. </em>Về khách sạn nhận phòng. Buổi chiều, xe đưa du khách tham quan<strong> trung tâm suối khoáng nóng I resort Nha Trang </strong>- thư giãn và tắm khoáng (tự túc chi phí tắm bùn các loại). Ăn tối tại Nhà hàng &amp; nghỉ đêm tại Thành phố biển Nha Trang.</p>\r\n<p> </p>\r\n<p><strong><img src=\"https://saigontourist.net/uploads/destination/TrongNuoc/Nhatrang/Long-Son-pagoda_319706471.jpg\" alt=\"\" /><br />Tượng Phật A-Di-Đà</strong></p>\r\n<p><strong>Ngày 02: NHA TRANG – KHU GIẢI TRÍ VINWONDERS (Ăn sáng, tối)</strong><br />Sau bữa sáng tại khách sạn, đoàn khởi hành đi thăm<strong> Viện Hải Dương </strong>học, sau đó xe đưa du khách đi tham quan <strong>Khu vui chơi giải trí Vinwonders xe và HDV đưa đi.</strong> (Chi phí vé cáp treo tự túc: 850.000đ) - Với quy mô và hiện đại, có thể sánh ngang với nhiều công viên giải trí hàng đầu trong khu vực, khám phá những công trình tiêu biểu như: Hệ thống cáp treo vượt biển dài nhất thế giới, Thủy cung lớn và hiện đại nhất Việt Nam, Khu trò chơi trong nhà,  Khu trò chơi ngoài trời và Công viên nước ngọt trên bãi biển đầu tiên &amp; duy nhất tại Việt Nam. Phòng chiếu phim 4D , Chương trình biểu diễn nhạc nước (Du khách tự túc ăn trưa )<strong>. </strong>Chiều, Quay về đất liền. Ăn tối và nghỉ đêm tại Nha Trang.</p>\r\n<p><strong><img src=\"https://saigontourist.net/uploads/destination/TrongNuoc/Nhatrang/Vinpearl-Land_781467691.jpg\" alt=\"\" /><br />Khu giải trí Vinwonders </strong></p>\r\n<p><strong>Ngày 03: NHA TRANG - ĐÀ LẠT (Ăn sáng, trưa, chiều)</strong><br />Sau bữa sáng tại khách sạn, tự do nghỉ ngơi, sau đó du khách trả phòng. Ăn trưa, đoàn trả phòng, khởi hành đi Đà Lạt theo cung đường mới <strong>Diên Khánh - Khánh Vĩnh - Khánh Linh</strong>, cung đường du lịch nối biển và thành phố Hoa, với phong cảnh đẹp tựa chốn bồng lai. Tới nơi, đoàn nhận phòng khách sạn. Ăn tối &amp; Nghỉ đêm tại Đà Lạt.</p>\r\n<p><strong><img src=\"https://saigontourist.net/uploads/destination/TrongNuoc/Dalat/Domain-de-Marie-church.jpg\" alt=\"\" /><br />Nhờ Thờ Domaine</strong></p>\r\n<p><strong>Ngày 04: THAM QUAN ĐÀ LẠT </strong><strong>(Ăn sáng, trưa)</strong><br />Sau bữa sáng tại khách sạn, xe đưa du khách tham quan <strong>Đường hầm điêu khắc đất đỏ (Đà Lạt Star) </strong>- tái hiện lịch sử Đà Lạt qua hơn 120 năm. Xe theo <strong>cung đường Mang - Ling</strong>, ngang qua khu <strong>Vườn Hồng Cam Ly</strong>, tham quan <strong>khu dã ngoại núi Langbian,</strong> tham quan <strong>đồi Mimosa, Thung lũng Trăm Năm, chinh phục núi Langbian</strong> (Không bao gồm phí xe Jeep), từ trên đỉnh núi ngắm toàn cảnh thành phố sương mù. Ăn trưa. Du khách về khách sạn nghỉ ngơi. Chiều tự do đi dạo thành phố Đà Lạt. Đoàn tự do ăn tối &amp; thưởng thức các món ăn đặc sản tại Chợ Đêm Đà Lạt. Nghỉ đêm tại Đà Lạt.</p>\r\n<p><strong><img src=\"https://saigontourist.net/uploads/destination/TrongNuoc/Dalat/DaLat-FestivalHoa_1343754392.jpg\" alt=\"\" /><br />Vườn hoa Đà Lạt</strong></p>', 'đà bẵng rất ok nha', 20, '2020-07-22 18:42:20', '2020-07-22 18:42:20', 1, '1', 'tour/d-2.png', 'tour/banner/tr.png', 5);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `tour_images`
--

CREATE TABLE `tour_images` (
  `id` int(10) UNSIGNED NOT NULL,
  `Images` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `tour_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `tour_images`
--

INSERT INTO `tour_images` (`id`, `Images`, `tour_id`, `created_at`, `updated_at`) VALUES
(2, '[\"http://localhost:88/travel-master/upload/tour/tourimage/usa.png\",\"http://localhost:88/travel-master/upload/tour/tourimage/messor.png\"]', 3, '2020-07-19 14:09:55', '2020-07-19 14:09:55'),
(3, '[\"http://localhost:88/travel-master/upload/tour/tourimage/usa.png\",\"http://localhost:88/travel-master/upload/tour/tourimage/messor.png\"]', 5, '2020-07-19 14:11:05', '2020-07-19 14:11:05'),
(4, '[\"http://localhost:88/travel-master/upload/tour/tourimage/usa.png\",\"http://localhost:88/travel-master/upload/tour/tourimage/messor.png\"]', 6, '2020-07-20 03:29:24', '2020-07-20 03:29:24'),
(5, '[\"http://localhost:88/travel-master/upload/tour/tourimage/italy.png\",\"http://localhost:88/travel-master/upload/tour/tourimage/messor.png\",\"http://localhost:88/travel-master/upload/tour/tourimage/usa.png\"]', 7, '2020-07-21 04:55:29', '2020-07-21 04:55:29'),
(6, '[\"http://localhost:88/travel-master/upload/tour/tourimage/blog-2.png\",\"http://localhost:88/travel-master/upload/tour/tourimage/blog-3.png\"]', 9, '2020-07-21 04:58:17', '2020-07-21 04:58:17'),
(7, '[\"http://localhost:88/travel-master/upload/tour/tourimage/blog-2.png\",\"http://localhost:88/travel-master/upload/tour/tourimage/blog-3.png\"]', 11, '2020-07-21 04:59:41', '2020-07-21 04:59:41'),
(8, '[\"http://localhost:88/travel-master/upload/tour/tourimage/banner-area.png\",\"http://localhost:88/travel-master/upload/tour/tourimage/blog-1.png\"]', 12, '2020-07-21 06:39:35', '2020-07-21 06:39:35'),
(9, '[\"http://localhost:88/travel-master/upload/tour/tourimage/banner-area.png\",\"http://localhost:88/travel-master/upload/tour/tourimage/blog-1.png\"]', 14, '2020-07-21 06:40:15', '2020-07-21 06:40:15'),
(10, '[\"http://localhost:88/travel-master/upload/tour/tourimage/banner-area.png\",\"http://localhost:88/travel-master/upload/tour/tourimage/blog-1.png\",\"http://localhost:88/travel-master/upload/tour/tourimage/blog-3.png\"]', 16, '2020-07-21 06:49:29', '2020-07-21 06:49:29'),
(12, '[\"http://localhost:88/travel-master/upload/tour/tourimage/banner-area.png\",\"http://localhost:88/travel-master/upload/tour/tourimage/blog-1.png\",\"http://localhost:88/travel-master/upload/tour/tourimage/blog-3.png\"]', 18, '2020-07-21 06:56:34', '2020-07-21 06:56:34'),
(13, '[\"http://localhost:88/travel-master/upload/tour/tourimage/banner-area.png\",\"http://localhost:88/travel-master/upload/tour/tourimage/blog-1.png\",\"http://localhost:88/travel-master/upload/tour/tourimage/blog-3.png\"]', 19, '2020-07-21 06:56:54', '2020-07-21 06:56:54'),
(14, '[\"http://localhost:88/travel-master/upload/tour/tourimage/banner-area.png\",\"http://localhost:88/travel-master/upload/tour/tourimage/blog-1.png\",\"http://localhost:88/travel-master/upload/tour/tourimage/blog-3.png\"]', 20, '2020-07-21 06:58:01', '2020-07-21 06:58:01'),
(15, '[\"http://localhost:88/travel-master/upload/tour/tourimage/banner-area.png\",\"http://localhost:88/travel-master/upload/tour/tourimage/blog-1.png\",\"http://localhost:88/travel-master/upload/tour/tourimage/blog-3.png\"]', 21, '2020-07-21 06:58:40', '2020-07-21 06:58:40'),
(16, '[\"http://localhost:88/travel-master/upload/tour/tourimage/banner-area.png\",\"http://localhost:88/travel-master/upload/tour/tourimage/blog-1.png\",\"http://localhost:88/travel-master/upload/tour/tourimage/blog-3.png\"]', 22, '2020-07-21 07:11:40', '2020-07-21 07:11:40'),
(17, '[\"http://localhost:88/travel-master/upload/tour/tourimage/messor.png\",\"http://localhost:88/travel-master/upload/tour/tourimage/italy.png\"]', 23, '2020-07-22 18:42:20', '2020-07-22 18:42:20'),
(18, '[\"http://localhost:88/travel-master/upload/tour/tourimage/blog-3.png\",\"http://localhost:88/travel-master/upload/tour/tourimage/france.png\"]', 25, '2020-07-24 10:49:02', '2020-07-24 10:49:02'),
(19, '[\"http://localhost:88/travel-master/upload/tour/tourimage/banner-area.png\",\"http://localhost:88/travel-master/upload/tour/tourimage/blog-1.png\"]', 26, '2020-07-24 11:00:36', '2020-07-24 11:00:36'),
(20, '[\"http://localhost:88/travel-master/upload/blog/6.png\",\"http://localhost:88/travel-master/upload/blog/5.png\"]', 28, '2020-07-24 11:15:47', '2020-07-24 11:15:47');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Chỉ mục cho các bảng đã đổ
--

--
-- Chỉ mục cho bảng `blogs`
--
ALTER TABLE `blogs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `blogs_tagid_foreign` (`TagID`);

--
-- Chỉ mục cho bảng `blog_images`
--
ALTER TABLE `blog_images`
  ADD PRIMARY KEY (`id`),
  ADD KEY `blog_images_blogid_foreign` (`BlogID`);

--
-- Chỉ mục cho bảng `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `configs`
--
ALTER TABLE `configs`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `contacts`
--
ALTER TABLE `contacts`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `customers`
--
ALTER TABLE `customers`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `customers_phone_number_unique` (`phone_number`),
  ADD UNIQUE KEY `customers_email_unique` (`email`);

--
-- Chỉ mục cho bảng `customer_services`
--
ALTER TABLE `customer_services`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `galleries`
--
ALTER TABLE `galleries`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `guides`
--
ALTER TABLE `guides`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`),
  ADD KEY `orders_userid_foreign` (`UserID`),
  ADD KEY `orders_paymentid_foreign` (`PaymentID`);

--
-- Chỉ mục cho bảng `order_details`
--
ALTER TABLE `order_details`
  ADD PRIMARY KEY (`id`),
  ADD KEY `order_details_tourid_foreign` (`TourID`),
  ADD KEY `order_details_orderid_foreign` (`OrderID`);

--
-- Chỉ mục cho bảng `order_guides`
--
ALTER TABLE `order_guides`
  ADD PRIMARY KEY (`id`),
  ADD KEY `order_guides_orderid_foreign` (`OrderID`),
  ADD KEY `order_guides_guideid_foreign` (`GuideID`);

--
-- Chỉ mục cho bảng `payment_methods`
--
ALTER TABLE `payment_methods`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `ratings`
--
ALTER TABLE `ratings`
  ADD PRIMARY KEY (`id`),
  ADD KEY `ratings_tourid_foreign` (`TourID`),
  ADD KEY `ratings_userid_foreign` (`UserID`);

--
-- Chỉ mục cho bảng `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `roles_name_unique` (`name`);

--
-- Chỉ mục cho bảng `role_assignments`
--
ALTER TABLE `role_assignments`
  ADD PRIMARY KEY (`customer_id`,`role_id`),
  ADD KEY `role_assignments_role_id_foreign` (`role_id`);

--
-- Chỉ mục cho bảng `services`
--
ALTER TABLE `services`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `services_name_unique` (`Name`);

--
-- Chỉ mục cho bảng `service_details`
--
ALTER TABLE `service_details`
  ADD PRIMARY KEY (`id`),
  ADD KEY `service_details_serviceid_foreign` (`ServiceID`),
  ADD KEY `service_details_tourid_foreign` (`TourID`);

--
-- Chỉ mục cho bảng `tags`
--
ALTER TABLE `tags`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `tags_name_unique` (`Name`);

--
-- Chỉ mục cho bảng `tours`
--
ALTER TABLE `tours`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `tours_name_unique` (`Name`),
  ADD KEY `tours_catid_foreign` (`CatID`),
  ADD KEY `tours_tagid_foreign` (`TagID`);

--
-- Chỉ mục cho bảng `tour_images`
--
ALTER TABLE `tour_images`
  ADD PRIMARY KEY (`id`),
  ADD KEY `tour_images_tour_id_foreign` (`tour_id`);

--
-- Chỉ mục cho bảng `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT cho các bảng đã đổ
--

--
-- AUTO_INCREMENT cho bảng `blogs`
--
ALTER TABLE `blogs`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT cho bảng `blog_images`
--
ALTER TABLE `blog_images`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT cho bảng `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT cho bảng `configs`
--
ALTER TABLE `configs`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT cho bảng `contacts`
--
ALTER TABLE `contacts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT cho bảng `customers`
--
ALTER TABLE `customers`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT cho bảng `customer_services`
--
ALTER TABLE `customer_services`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT cho bảng `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT cho bảng `galleries`
--
ALTER TABLE `galleries`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT cho bảng `guides`
--
ALTER TABLE `guides`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT cho bảng `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT cho bảng `orders`
--
ALTER TABLE `orders`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT cho bảng `order_details`
--
ALTER TABLE `order_details`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT cho bảng `order_guides`
--
ALTER TABLE `order_guides`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT cho bảng `payment_methods`
--
ALTER TABLE `payment_methods`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT cho bảng `ratings`
--
ALTER TABLE `ratings`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=62;

--
-- AUTO_INCREMENT cho bảng `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT cho bảng `services`
--
ALTER TABLE `services`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT cho bảng `service_details`
--
ALTER TABLE `service_details`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT cho bảng `tags`
--
ALTER TABLE `tags`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT cho bảng `tours`
--
ALTER TABLE `tours`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;

--
-- AUTO_INCREMENT cho bảng `tour_images`
--
ALTER TABLE `tour_images`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT cho bảng `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- Các ràng buộc cho các bảng đã đổ
--

--
-- Các ràng buộc cho bảng `blogs`
--
ALTER TABLE `blogs`
  ADD CONSTRAINT `blogs_tagid_foreign` FOREIGN KEY (`TagID`) REFERENCES `tags` (`id`) ON DELETE CASCADE;

--
-- Các ràng buộc cho bảng `blog_images`
--
ALTER TABLE `blog_images`
  ADD CONSTRAINT `blog_images_blogid_foreign` FOREIGN KEY (`BlogID`) REFERENCES `blogs` (`id`) ON DELETE CASCADE;

--
-- Các ràng buộc cho bảng `orders`
--
ALTER TABLE `orders`
  ADD CONSTRAINT `orders_paymentid_foreign` FOREIGN KEY (`PaymentID`) REFERENCES `payment_methods` (`id`),
  ADD CONSTRAINT `orders_userid_foreign` FOREIGN KEY (`UserID`) REFERENCES `customers` (`id`);

--
-- Các ràng buộc cho bảng `order_details`
--
ALTER TABLE `order_details`
  ADD CONSTRAINT `order_details_orderid_foreign` FOREIGN KEY (`OrderID`) REFERENCES `orders` (`id`),
  ADD CONSTRAINT `order_details_tourid_foreign` FOREIGN KEY (`TourID`) REFERENCES `tours` (`id`);

--
-- Các ràng buộc cho bảng `order_guides`
--
ALTER TABLE `order_guides`
  ADD CONSTRAINT `order_guides_guideid_foreign` FOREIGN KEY (`GuideID`) REFERENCES `guides` (`id`),
  ADD CONSTRAINT `order_guides_orderid_foreign` FOREIGN KEY (`OrderID`) REFERENCES `orders` (`id`);

--
-- Các ràng buộc cho bảng `ratings`
--
ALTER TABLE `ratings`
  ADD CONSTRAINT `ratings_tourid_foreign` FOREIGN KEY (`TourID`) REFERENCES `tours` (`id`),
  ADD CONSTRAINT `ratings_userid_foreign` FOREIGN KEY (`UserID`) REFERENCES `customers` (`id`);

--
-- Các ràng buộc cho bảng `role_assignments`
--
ALTER TABLE `role_assignments`
  ADD CONSTRAINT `role_assignments_customer_id_foreign` FOREIGN KEY (`customer_id`) REFERENCES `customers` (`id`),
  ADD CONSTRAINT `role_assignments_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`);

--
-- Các ràng buộc cho bảng `service_details`
--
ALTER TABLE `service_details`
  ADD CONSTRAINT `service_details_serviceid_foreign` FOREIGN KEY (`ServiceID`) REFERENCES `services` (`id`),
  ADD CONSTRAINT `service_details_tourid_foreign` FOREIGN KEY (`TourID`) REFERENCES `tours` (`id`);

--
-- Các ràng buộc cho bảng `tours`
--
ALTER TABLE `tours`
  ADD CONSTRAINT `tours_catid_foreign` FOREIGN KEY (`CatID`) REFERENCES `categories` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `tours_tagid_foreign` FOREIGN KEY (`TagID`) REFERENCES `tags` (`id`) ON DELETE CASCADE;

--
-- Các ràng buộc cho bảng `tour_images`
--
ALTER TABLE `tour_images`
  ADD CONSTRAINT `tour_images_tour_id_foreign` FOREIGN KEY (`tour_id`) REFERENCES `tours` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
