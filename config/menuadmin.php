<?php
return [
    [
        'label' => 'Customer',
        'route' => 'admin.customer.index',
        'items' => [    
            [
                'label' => 'Add Customer',
                'route' => 'admin.customer.create',
            ],
            [
                'label' => 'List Customer',
                'route' => 'admin.customer.index',
            ],
        ],
    ],
    [
        'label' => 'Category',
        'route' => 'admin.category.index',
        'items' => [
            [
                'label' => 'Add Category',
                'route' => 'admin.category.create',
            ],
            [
                'label' => 'List Category',
                'route' => 'admin.category.index',
            ],
        ],
    ],
    [
        'label' => 'Gallery',
        'route' => 'admin.gallery.index',
        'items' => [
            [
                'label' => 'Add Gallery',
                'route' => 'admin.gallery.create',
            ],
            [
                'label' => 'List Gallery',
                'route' => 'admin.gallery.index',
            ],
        ],
    ],
    [
        'label' => 'Tour',
        'route' => 'admin.tour.index',
        'items' => [
            [
                'label' => 'Add Tour',
                'route' => 'admin.tour.create',
            ],
            [
                'label' => 'List Tour',
                'route' => 'admin.tour.index',
            ],
        ],
    ],
    [
        'label' => 'Role',
        'route' => 'admin.role.index',
        'items' => [
            [
                'label' => 'Add Role',
                'route' => 'admin.role.create',
            ],
            [
                'label' => 'List Role',
                'route' => 'admin.role.index',
            ],
        ],
    ],
]

?>
