<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        DB::table('customers')->insert([
            'name' => 'Trần Tuấn Anh',
            'address' => 'Nghi Lộc',
            'sex' => '1',
            'phone_number' => '0376742040',
            'date_of_birth' => '2001-12-02',
            'email' => 'root@gmail.com',
            'password' => Hash::make('123456789'),
            'level' => '1',
            'status' => '1',
        ]);
        DB::table('roles')->insert([
            'name' => 'Super Admin',
            'permission' => '["admin.changestatus","admin.getlogin","admin.postlogin","admin.error","admin.customer.avatar","admin.get_password","admin.post_password","admin.get_code","admin.post_code","admin.get_register","admin.post_register","admin.post_change_pasword","admin.index","admin.category.index","admin.category.create","admin.category.store","admin.category.show","admin.category.edit","admin.category.update","admin.category.destroy","admin.tag.index","admin.tag.create","admin.tag.store","admin.tag.show","admin.tag.edit","admin.tag.update","admin.tag.destroy","admin.tour.index","admin.tour.create","admin.tour.store","admin.tour.show","admin.tour.edit","admin.tour.update","admin.tour.destroy","admin.customer.index","admin.customer.create","admin.customer.store","admin.customer.show","admin.customer.edit","admin.customer.update","admin.customer.destroy","admin.role.index","admin.role.create","admin.role.store","admin.role.show","admin.role.edit","admin.role.update","admin.role.destroy","admin.gallery.index","admin.gallery.create","admin.gallery.store","admin.gallery.show","admin.gallery.edit","admin.gallery.update","admin.gallery.destroy","admin.role_assignment.index","admin.role_assignment.create","admin.role_assignment.store","admin.role_assignment.show","admin.role_assignment.edit","admin.role_assignment.update","admin.role_assignment.destroy","admin.contact.index","admin.contact.create","admin.contact.store","admin.contact.show","admin.contact.edit","admin.contact.update","admin.contact.destroy","admin.customer_service.index","admin.customer_service.create","admin.customer_service.store","admin.customer_service.show","admin.customer_service.edit","admin.customer_service.update","admin.customer_service.destroy","admin.permission.index","admin.permission.create","admin.permission.store","admin.permission.show","admin.permission.edit","admin.permission.update","admin.permission.destroy","admin.payment.index","admin.payment.create","admin.payment.store","admin.payment.show","admin.payment.edit","admin.payment.update","admin.payment.destroy","admin.order.index","admin.order.create","admin.order.store","admin.order.show","admin.order.edit","admin.order.update","admin.order.destroy","admin.service.index","admin.service.create","admin.service.store","admin.service.show","admin.service.edit","admin.service.update","admin.service.destroy","admin.guide.index","admin.guide.create","admin.guide.store","admin.guide.show","admin.guide.edit","admin.guide.update","admin.guide.destroy","admin.blog.index","admin.blog.create","admin.blog.store","admin.blog.show","admin.blog.edit","admin.blog.update","admin.blog.destroy","admin.rating.index","admin.rating.create","admin.rating.store","admin.rating.show","admin.rating.edit","admin.rating.update","admin.rating.destroy","admin.content_support.index","admin.content_support.create","admin.content_support.store","admin.content_support.show","admin.content_support.edit","admin.content_support.update","admin.content_support.destroy","admin.support.index","admin.support.create","admin.support.store","admin.support.show","admin.support.edit","admin.support.update","admin.support.destroy","admin.orderview","admin.getlogout","admin.index","admin.getlogin","admin.postlogin","admin.get_password","admin.post_password","admin.get_code","admin.post_code","admin.get_register","admin.post_change_pasword","admin.getlogout","admin.post_register"]',
        ]);
        DB::table('role_assignments')->insert([
            'customer_id' => '1',
            'role_id' => '1',
        ]);
    }
}
