<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBlogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('blogs', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('TagID');
            $table->string('Image');
            $table->string('baner_top');
            $table->text('Content');
            $table->text('Contents');
            $table->text('Creator');
            $table->timestamps();


            /*board link*/
            $table->foreign('TagID')->references('id')->on('tags');
              });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('blogs');
    }
}
