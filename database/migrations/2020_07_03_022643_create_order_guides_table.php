<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrderGuidesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('order_guides', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('OrderID');
            $table->unsignedInteger('GuideID');
            $table->timestamps();
            /*board link*/
            $table->foreign('OrderID')->references('id')->on('orders');
            $table->foreign('GuideID')->references('id')->on('guides');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('order_guides');
    }
}
