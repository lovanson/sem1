<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRatingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ratings', function (Blueprint $table) {
             $table->increments('id');
            $table->string('Piority');
            $table->unsignedInteger('TourID');
            $table->unsignedInteger('UserID');
            $table->float('NumberRating');
            $table->text('Content');
              $table->timestamps();



            /*board link*/
            $table->foreign('TourID')->references('id')->on('tours');
            $table->foreign('UserID')->references('id')->on('customers');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ratings');
    }
}
