<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrderDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('order_details', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('TourID');
            $table->unsignedInteger('OrderID');
            $table->text('Destination');
            $table->date('Start');
            $table->date('End');
            $table->integer('guestchild');
            $table->integer('guestchildren');
            $table->integer('guestadult');
            $table->float('TotalPrice');
            $table->timestamps();
            /*board link*/
            $table->foreign('TourID')->references('id')->on('tours');
            $table->foreign('OrderID')->references('id')->on('orders');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('order_details');
    }
}
