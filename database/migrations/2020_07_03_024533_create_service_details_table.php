<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateServiceDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('service_details', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('ServiceID');
            $table->unsignedInteger('TourID');
            $table->integer('Quantity');
            $table->float('Totalamount');
            $table->timestamps();
            /*board link*/
            $table->foreign('ServiceID')->references('id')->on('services');
            $table->foreign('TourID')->references('id')->on('tours');
            // $table->foreign('TourID')->references('id')->on('tours');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('service_details');
    }
}
