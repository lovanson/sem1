<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */

Route::group(['prefix' => '', 'namespace' => 'User'], function () {
    Route::post('/l', 'UserController@postlogin')->name('User.postlogin');
    Route::get('login', 'UserController@login')->name('User.login');
    Route::post('/register', 'UserController@postpegister')->name('User.postpegister');
    Route::get('/logout', 'UserController@getlogout')->name('User.logout');
    Route::post('/change_pasword', 'UserController@post_change_pasword')->name('User.post_change_pasword');
    Route::post('/forgot_password', 'UserController@post_password')->name('User.post_password');
    Route::get('/forgotpass', 'UserController@forgotpass')->name('User.forgotpass');
    Route::post('/avatar/{customer}', 'UserController@postavatar')->name('customer.avatar');
    Route::get('/information', 'UserController@information')->name('User.information');

    Route::get('/code', 'UserController@get_code')->name('User.get_code');
    Route::post('/code', 'UserController@post_code')->name('User.post_code');

    Route::get('/', 'UserController@home')->name('User.home');
    Route::get('search_home', 'UserController@search_home')->name('User.search_home');
    Route::get('guideview','UserController@guideview')->name('guideview');


    Route::get('tour_details/{id}', 'UserController@tour_details')->name('User.tour_details');
    Route::post('tour_details_post', 'UserController@tour_details_post')->name('User.tour_details_post');
     Route::post('rating_post','UserController@rating_post')->name('User.rating_post');

    Route::get('blog_single/{id}', 'UserController@blog_single')->name('User.blog_single');
    Route::get('about', 'UserController@about')->name('User.about');
    Route::get('blog', 'UserController@blog')->name('User.blog');
    Route::get('search_blogs', 'UserController@search_blogs')->name('User.search_blogs');

    Route::get('contact', 'UserController@contact')->name('User.contact');
    Route::post('contact_post', 'UserController@contact_post')->name('User.contact_post');

    Route::get('faq', 'UserController@faq')->name('User.faq');
    Route::get('service', 'UserController@service')->name('User.service');
    Route::get('tour_packages/{id}', 'UserController@tour_packages')->name('User.tour_packages');
    Route::get('error', 'UserController@error')->name('User.error');
    Route::get('gallery', 'UserController@gallery')->name('User.gallery');
    Route::post('ajax-post', 'UserController@ajax_post')->name('ajax-post');
    Route::get('seach_information','UserController@seach_information')->name('User.seach_information');
});
// -----------------------admin-------------------------------
Route::group(['prefix'=>'admin','namespace' => 'Admin','as' =>'admin.'], function () {
    Route::get('changestatus/{id}', 'customerController@changestatus')->name('changestatus');
    Route::get('/login', 'AdminLoginController@getlogin')->name('getlogin');
    Route::post('/login', 'AdminLoginController@postlogin')->name('postlogin');
    Route::get('/error', 'customerController@error')->name('error');
    Route::post('/avatar/{customer}', 'customerController@postavatar')->name('customer.avatar');
    Route::get('/forgot_password', 'AdminLoginController@get_password')->name('get_password');
    Route::post('/forgot_password', 'AdminLoginController@post_password')->name('post_password');
    Route::get('/password', 'AdminLoginController@get_code')->name('get_code');
    Route::post('/password', 'AdminLoginController@post_code')->name('post_code');
    Route::get('/register', 'RegisterController@get_register')->name('get_register');
    Route::post('/register', 'RegisterController@post_register')->name('post_register');
    Route::post('/change_pasword', 'AdminLoginController@post_change_pasword')->name('post_change_pasword');
    Route::group(['middleware' => 'auth'], function () {
        Route::get('/', 'AdminLoginController@dashboard')->name('index');
        Route::resources([
            'category' => 'CategoryController',
            'tag' => 'TagController',
            'tour' => 'TourController',
            'customer' => 'customerController',
            'role' => 'RoleController',
            'gallery' => 'GalleryController',
            'role_assignment' => 'Role_AssignmentController',
            'contact' => 'contactController',
            'customer_service' => 'CustomerServiceController',
            'permission' => 'permissionController',

            // 'role_permission' => 'role_permissionController',
            'payment' => 'PaymentController',
            'order' => 'OrderController',
            'service' => 'ServiceController',
            'guide' => 'GuideController',
            'blog' => 'BlogController',
            'rating' => 'RatingController',
        ]);
         Route::get('orderview','OrderController@orderview')->name('orderview');
         Route::get('search_category','CategoryController@search_category')->name('search_category');
         Route::get('search_tag','TagController@search_tag')->name('search_tag');
         Route::get('search_payment','PaymentController@search_payment')->name('search_payment');
         Route::get('search_guide','GuideController@search_guide')->name('search_guide');
         Route::get('search_rating','RatingController@search_rating')->name('search_rating');
         Route::get('search_contact','contactController@search_contact')->name('search_contact');
         Route::get('search_tour','TourController@search_tour')->name('search_tour');
         Route::get('search_blog','BlogController@search_blog')->name('search_blog');
         Route::get('search_gallery','GalleryController@search_gallery')->name('search_gallery');
         Route::get('search_customer','customerController@search_customer')->name('search_customer');
         Route::get('search_customer_service','CustomerServiceController@search_customer_service')->name('search_customer_service');
         Route::get('search_role_assignment','Role_AssignmentController@search_role_assignment')->name('search_role_assignment');
         Route::get('seach_role','RoleController@seach_role')->name('seach_role');
         Route::get('search_service','ServiceController@search_service')->name('search_service');
         Route::get('seach_order','OrderController@seach_order')->name('seach_order');
         Route::get('seach_permission','OrderController@seach_permission')->name('seach_permission');

        Route::get('/logout', 'AdminLoginController@getLogout')->name('getlogout');
    });
});
